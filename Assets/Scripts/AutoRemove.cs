﻿using UnityEngine;
using System.Collections;

public class AutoRemove : MonoBehaviour
{
    public float LifeTime;

    void Start()
    {
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(LifeTime);
        Destroy(gameObject);
    }
}
