﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class HeroController : MonoBehaviour
{
    private const string HIGHLIGHT_TAG = "HighLight";
    private const int TARGET_RAY_LENGHT = 20;

    private const int MAXSKILLDISTANCE = 40;
    private const int DAMAGERMARGINPERCENT = 10;

    private const int CROSSHAIR_MULTIPLIAR = 700;

    private const float NEUTRAL = 0f;
    private const float MAXFORWARD = 0.5f;
    private const float MAXFORWARDRUN = 2.2f;
    private const float MINFORWARD = -1f;
    private const float MINFORWARDRUN = -2f;

    private const float RUNSPEED = 1.3f;
    private const float BACKRUNSPEED = -1.4f;

    private const float MAXRIGHT = 2f;
    private const float MINRIGHT = -2f;

    private const float MINTURN = -1;
    private const float MAXTURN = 1;

    private const float MINTURNDIF = 0.001f;

    private const float MAXDEGTOROTATION = 0.1f;
    private const float LAYERRISESPEED = 0.05f;

    private const float MAINATTACKDELAY = 1.4f;
    private const float SKILLATTACKDELAY = 2f;

    public delegate void HeroActivated();
    public event HeroActivated OnHeroActivated;

    public delegate void HeroDeActivated();
    public event HeroDeActivated OnHeroDeActivated;

    public float ForwardSpeed = 0.1f;
    public float SideWaySpeed = 0.3f;
    public float TurnSpeed = 0.1f;
    public float CameraSpeed = 0.2f;
    public float CameraTurnSpeed = 0.2f;

    public LayerMask TargetRayLayerMask;

    [Header("Skills")]
    public List<Skill> Skills = new List<Skill>();
    public GameObject SkillTargetProjectorPrefab;

    private Skill currentSkill;
    private StrategieCameraController controller;

    private Animator animator;
    private CharacterController characterController;

    [Header("Healing")]
    private Building buildingToHeal;

    [Header("SkillUI")]
    public Texture2D TargetCurser;

    [Header("Skill Fields")]
    public Transform SkillCamPos;
    public Transform SkillCamLookAtPos;
    public float CameraChangeDamping;

    [Header("Main Attack")]
    public Texture CrossHairTexture;
    public GameObject Projectile;
    public Transform ProjectileSpawnPos;
    public float Damage;
    public float AttackSpeed;
    public float MaxDistance;
    public float Range;

    public Transform TargetRayStartPos;
    public Transform MainAttackTargetTransform;

    private Coroutine attackAnimDelayCoroutine;
    private Coroutine attackTimerCoroutine;
    private Coroutine layerWeightCoroutine;

    [HideInInspector]
    public ObjectPool<Projectile> ProjectilesPool;

    [Header("UI")]
    public Image RadialImage;
    public Text SpawnDelayText;

    [Header("On Death")]
    public AudioClip DeathSound;
    public float RespawnDelay = 10f;
    private float deathTimer = 0;

    [Header("On Damaged")]
    public float TimeToShowEffectOnHit = 3f;
    public float HitEffectSteps;
    private float hitEffectTimer;
    private Coroutine hitEffectCoroutine;

    [Header("Projectile")]
    public Transform ProjectilePos;
    public Transform ProjectilePosToSpawn;
    public GameObject FireInHandPrefab;

    [HideInInspector]
    public Health health;

    private FogOfWarElement fogOfWarElement;
    private bool cameraReachedHero = false;
    private bool isHudHidden = false;
    private bool skillsJustChanged = false;

    private float forward = 0;
    private float right = 0;
    private float turn = 0;

    public Transform CamTargetPos;
    public Transform CamStartPos;
    private float x = 0;
    private float y = 0;
    private float xSpeed = 60f;
    private float ySpeed = 120f;
    private float distance = 5f;
    private float yMinLimit = -10f;
    private float yMaxLimit = 80f;
    private float distanceMin = 2f;
    private float distanceMax = 5f;

    private Quaternion lasRot;

    [HideInInspector]
    public bool IsInMotion = false;
    [HideInInspector]
    public bool IsIdleing = false;
    [HideInInspector]
    public bool IsTurning = false;
    [HideInInspector]
    public bool IsAttacking = false;
    [HideInInspector]
    public bool IsRunning = false;
    [HideInInspector]
    public bool IsUsingSkill = false;
    [HideInInspector]
    public bool IsShowingSkillBuyPanel = false;

    private bool canAttack = true;
    private bool hideCrosshair = false;

    void Awake()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        ProjectilesPool = new ObjectPool<Projectile>(Projectile, 2);
        health = GetComponent<Health>();
    }

    void Start()
    {
        controller = GameManager.Manager.GetGameController();
        SkillTargetProjectorPrefab = Instantiate(SkillTargetProjectorPrefab, Vector3.zero, SkillTargetProjectorPrefab.transform.rotation) as GameObject;
        fogOfWarElement = FogOfWarManager.AddElement(health, 1f);
        health.OnDeath += Health_OnDeath;
        health.OnDamaged += Health_OnDamaged;
        UnitManager.Manager.AddFriendlyUnit(health);

        FireInHandPrefab = Instantiate(FireInHandPrefab, ProjectilePos.position, Quaternion.identity) as GameObject;
        FireInHandPrefab.transform.SetParent(ProjectilePos);
    }

    void Update()
    {
        if (GameManager.Manager.IsGamePaused() && GameManager.Manager.GetGameController().CamMode == StrategieCameraController.CameraMode.HeroMode)
        {
            if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroPause))
                GameManager.Manager.ResumeGame();
            return;
        }

        if (GameManager.Manager.GetGameController().CamMode == StrategieCameraController.CameraMode.HeroMode && Time.timeScale != 0)
        {
            if (cameraReachedHero)
            {
                HighLightTarget();
                ResetStates();
                HandleStates();
                GetCameraInput();
                ControllSkillCamera();
                GetMoveInputForwardsBackwards();
                GetMoveInputRightLeft();
                GetTurnInput();
                GetAttackInput();
                ChooseSkill();
                GetSkillInput();
                KeyInput();
                ControllFire();
                GetHealTargetInput();
                HandleCurrentSkillPanel();
            }
            else
                PlaceCameraAtHero();
        }
    }

    void OnGUI()
    {
        if (controller.CamMode == StrategieCameraController.CameraMode.HeroMode && currentSkill == null && Time.timeScale != 0 && !hideCrosshair && GameManager.Manager.GameSettings.DrawCrossHair)
        {
            float crossHairWidth = ((CrossHairTexture.width * Screen.width) / CROSSHAIR_MULTIPLIAR) * GameManager.Manager.GameSettings.CrossHairScale;
            float crossHairHeight = ((CrossHairTexture.height * Screen.width) / CROSSHAIR_MULTIPLIAR) * GameManager.Manager.GameSettings.CrossHairScale;

            Rect CrosshairRect = new Rect((Screen.width - crossHairWidth) / 2, (Screen.height - crossHairHeight) / 2, crossHairWidth, crossHairHeight);
            GUI.DrawTexture(CrosshairRect, CrossHairTexture);
        }
    }

    private void Health_OnDamaged(float damage)
    {
        if (controller.CamMode != StrategieCameraController.CameraMode.HeroMode)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "You´r Hero is beeing Attacked!", "", 1.5f);
        else
        {
            if (hitEffectCoroutine != null)
                StopCoroutine(hitEffectCoroutine);

            Color tempColor = GameManager.Manager.GetHitEffectImage().color;
            tempColor.a = 0.7f;
            GameManager.Manager.GetHitEffectImage().color = tempColor;

            hitEffectCoroutine = StartCoroutine(HitEffectTimer());
        }
    }

    private void Health_OnDeath()
    {
        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

        if (controller.CamMode == StrategieCameraController.CameraMode.SelectMode)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "You´r Hero died!", "", 2f);

        if (DeathSound != null)
            AudioSource.PlayClipAtPoint(DeathSound, GameManager.Manager.GetGameController().transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

        if (controller.CamMode == StrategieCameraController.CameraMode.HeroMode)
            DeActivateHeroMode();

        UnitManager.Manager.RemoveFriendlyUnit(health);

        GameManager.Manager.StartCoroutine(SpawningTimer(gameObject));

        RadialImage.fillAmount = 1;
        GameManager.Manager.StartCoroutine(RadialImageUpdate());

        transform.position = GameManager.Manager.GetMainBuildingController().HeroSpawnPosition.position;

        gameObject.SetActive(false);
    }

    public void ActivateHeroMode()
    {
        Cursor.lockState = CursorLockMode.Locked;
        hideCrosshair = false;
        Cursor.visible = false;

        if (OnHeroActivated != null)
            OnHeroActivated();

        foreach (Skill skill in Skills)
            skill.canFireSkill = true;

        foreach (var item in GameManager.SelectedObjects)
            item.OnDeSelected(ref GameManager.SelectedObjects);

        GameManager.Manager.GetRessources().HeroHealth_OnChanged();

        GameManager.Manager.ChangeHud(true);
        GameManager.SelectedObjects.Clear();
    }

    public void DeActivateHeroMode()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        ChangeHudView(true);

        if (OnHeroDeActivated != null)
            OnHeroDeActivated();

        if (hitEffectCoroutine != null)
            StopCoroutine(hitEffectCoroutine);

        cameraReachedHero = false;

        ChangeCurser(false);

        turn = 0;
        forward = 0;
        right = 0;

        animator.SetFloat("Right", right);
        animator.SetFloat("Forward", forward);
        animator.SetFloat("Turn", turn);

        Color tempColor = GameManager.Manager.GetHitEffectImage().color;
        tempColor.a = 0;
        GameManager.Manager.GetHitEffectImage().color = tempColor;

        GameManager.Manager.DestroyAllRepairCanvases();

        GameManager.Manager.ActivateHeroSkillsPanel(false);
        GameManager.Manager.ChangeHud(false);
        GameManager.Manager.GetGameController().ChangeModeTo("SelectMode");
        Camera.main.transform.rotation = GameManager.Manager.GetGameController().CamRotation;
        Camera.main.transform.position = GameManager.Manager.GetGameController().CamPosition;
    }

    public void SkillsJustChanged()
    {
        skillsJustChanged = true;

        StartCoroutine(SkillsJustChangedTimer());
    }

    public void ChooseSkill(int index)
    {
        switch (index)
        {
            case 1:
                if (Skills.Count >= 1)
                    currentSkill = Skills[0];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 2:
                if (Skills.Count >= 2)
                    currentSkill = Skills[1];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 3:
                if (Skills.Count >= 3)
                    currentSkill = Skills[2];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 4:
                if (Skills.Count >= 4)
                    currentSkill = Skills[3];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 5:
                if (Skills.Count >= 5)
                    currentSkill = Skills[4];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 6:
                if (Skills.Count >= 6)
                    currentSkill = Skills[5];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            default:
                currentSkill = null;
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                break;
        }

        if (currentSkill != null)
            ChangeCurser(true);
        else
            ChangeCurser(false);
    }

    private void ChooseSkill()
    {
        if (!Input.anyKey)
            return;

        string keyPressedString = Input.inputString;
        int keyPressed;
        bool result = int.TryParse(keyPressedString, out keyPressed);

        if (currentSkill != null)
            ChangeCurser(true);
        else
            ChangeCurser(false);

        if (!result)
            return;

        switch (keyPressed)
        {
            case 1:
                if (Skills.Count >= 1)
                    currentSkill = Skills[0];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 2:
                if (Skills.Count >= 2)
                    currentSkill = Skills[1];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 3:
                if (Skills.Count >= 3)
                    currentSkill = Skills[2];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 4:
                if (Skills.Count >= 4)
                    currentSkill = Skills[3];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 5:
                if (Skills.Count >= 5)
                    currentSkill = Skills[4];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            case 6:
                if (Skills.Count >= 6)
                    currentSkill = Skills[5];
                else
                {
                    currentSkill = null;
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                }
                break;

            default:
                currentSkill = null;
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
                break;
        }

        GameManager.Manager.HideCurrentSkillPanel();
        HandleCurrentSkillPanel();
    }

    private void PlaceCameraAtHero()
    {
        var wantedPosition = CamStartPos.position;

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, wantedPosition, CameraSpeed * 10);
        Camera.main.transform.LookAt(transform.position);

        if (Vector3.Distance(Camera.main.transform.position, wantedPosition) <= 0.4f)
        {
            Camera.main.transform.position = wantedPosition;
            cameraReachedHero = true;
        }
    }

    private void GetSkillInput()
    {
        if (currentSkill != null)
        {
            if (Input.GetMouseButtonDown(1))
            {
                currentSkill = null;
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (IsUsingSkill)
                {
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Cant Use this now!", "", 1.5f);
                    return;
                }

                if (Vector3.Distance(transform.position, SkillTargetProjectorPrefab.transform.position) >= MAXSKILLDISTANCE)
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Skill Target too far Away!", "", 2f);
                else
                {
                    if (currentSkill.OnActivated())
                    {
                        GameManager.Manager.GameStats.SkillsUsed++;
                        StartCoroutine(SkillAttackTimer());
                        animator.SetLayerWeight(1, 1);
                        StartCoroutine(LayerWeightTimer(1f, 1, 0));
                        animator.SetTrigger("Skill");
                    }

                    currentSkill = null;
                }
            }

            Vector3 worldPos = Utilitys.GiveMousePositionInWorldOnFloor();

            if (SkillTargetProjectorPrefab.activeSelf)
                SkillTargetProjectorPrefab.transform.position = new Vector3(worldPos.x, worldPos.y + 3.5f, worldPos.z);
            else
            {
                SkillTargetProjectorPrefab.SetActive(true);
                SkillTargetProjectorPrefab.transform.position = new Vector3(worldPos.x, worldPos.y + 3.5f, worldPos.z);
            }
        }
        else
        {
            if (SkillTargetProjectorPrefab.activeSelf)
                SkillTargetProjectorPrefab.SetActive(false);
        }

        if (Vector3.Distance(transform.position, SkillTargetProjectorPrefab.transform.position) >= MAXSKILLDISTANCE)
            SkillTargetProjectorPrefab.SetActive(false);
    }

    private void KeyInput()
    {
        if (GameManager.Manager.TextInputMode) return;

        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroHideHud))
            ChangeHudView();

        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroPause))
        {
            if (skillsJustChanged)
                GameManager.Manager.GetHeroSkillsPanelController().OnBackButtonClicked();
            else
            {
                if (GameManager.Manager.IsGamePaused()) GameManager.Manager.ResumeGame();
                else GameManager.Manager.PauseGame();
            }
        }

        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeySwitchMode))
        {
            controller.HeroModeJustChanged = true;
            DeActivateHeroMode();
        }

        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroSkills))
        {
            GameManager.Manager.ActivateHeroSkillsPanel(GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf ? false : true);

            if (GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf)
            {
                Time.timeScale = 0;
                controller.ChangeModeTo("SkillMode");
            }
            else
            {
                Time.timeScale = 1;
                GameManager.Manager.GetHeroSkillsPanelController().OnBackButtonClicked();
            }
        }
    }

    private void ControllFire()
    {
        if (IsRunning)
            FireInHandPrefab.SetActive(false);
        else
            FireInHandPrefab.SetActive(true);
    }

    private void ChangeCurser(bool skillMode)
    {
        if (!skillMode)
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        else
            Cursor.SetCursor(TargetCurser, Vector2.zero, CursorMode.Auto);
    }

    private void Respawn()
    {
        fogOfWarElement = FogOfWarManager.AddElement(health, 1f);

        health.Reset();
        transform.position = GameManager.Manager.GetMainBuildingController().HeroSpawnPosition.position;
        UnitManager.Manager.AddFriendlyUnit(health);
    }

    private void ChangeHudView()
    {
        if (isHudHidden)
        {
            for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
                GameManager.Manager.GetHeroHudElements()[i].SetActive(true);
            isHudHidden = false;
        }
        else
        {
            for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
                GameManager.Manager.GetHeroHudElements()[i].SetActive(false);
            isHudHidden = true;
        }
    }

    private void ChangeHudView(bool value)
    {
        for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
            GameManager.Manager.GetHeroHudElements()[i].SetActive(value);

        isHudHidden = value;
    }

    private void HandleStates()
    {
        if (right != 0 || forward != 0)
            IsInMotion = true;

        if (forward >= RUNSPEED || forward <= BACKRUNSPEED)
            IsRunning = true;
    }

    private void ResetStates()
    {
        IsInMotion = false;
        IsIdleing = false;
        IsTurning = false;
        IsRunning = false;
        IsAttacking = false;
        IsUsingSkill = false;
    }

    private void GetAttackInput()
    {
        if (GameManager.Manager.TextInputMode || currentSkill != null) return;

        if (Input.GetMouseButtonDown(0) && canAttack && !IsRunning)
        {
            if (layerWeightCoroutine != null) StopCoroutine(layerWeightCoroutine);
            if (attackTimerCoroutine != null) StopCoroutine(attackTimerCoroutine);
            if (attackAnimDelayCoroutine != null) StopCoroutine(attackAnimDelayCoroutine);

            animator.SetLayerWeight(1, 1);
            animator.SetTrigger("Attack");
            layerWeightCoroutine = StartCoroutine(LayerWeightTimer(1f, 1, 0));
            attackTimerCoroutine = StartCoroutine(MainAttackTimer());
            attackAnimDelayCoroutine = StartCoroutine(MainAttackAnimDelay());
        }
    }

    private void GetMoveInputRightLeft()
    {
        if (GameManager.Manager.TextInputMode) return;

        if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroMoveLeft))
            right -= SideWaySpeed * Time.deltaTime;
        else if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroMoveRight))
            right += SideWaySpeed * Time.deltaTime;
        else
        {
            if (right < 0)
            {
                right += SideWaySpeed * Time.deltaTime * 2;
                if (right > 0)
                    right = 0;
            }
            else if (right > 0)
            {
                right -= SideWaySpeed * Time.deltaTime * 2;
                if (right < 0)
                    right = 0;
            }
        }

        right = Mathf.Clamp(right, MINRIGHT, MAXRIGHT);

        animator.SetFloat("Right", right);
    }

    private void GetMoveInputForwardsBackwards()
    {
        if (GameManager.Manager.TextInputMode) return;

        if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroMoveForward) && Input.GetKey(GameManager.Manager.GameSettings.KeyHeroSprint) && forward >= MAXFORWARD)
        {
            forward += ForwardSpeed * 2 * Time.deltaTime;
            forward = Mathf.Clamp(forward, NEUTRAL, MAXFORWARDRUN);
        }
        else if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroMoveForward))
        {
            if (forward > MAXFORWARD)
                forward -= ForwardSpeed * Time.deltaTime;
            else
            {
                forward += ForwardSpeed * Time.deltaTime;
                forward = Mathf.Clamp(forward, NEUTRAL, MAXFORWARD);
            }
        }
        else if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroMoveBackwards))
        {
            forward -= ForwardSpeed * Time.deltaTime;
            forward = Mathf.Clamp(forward, MINFORWARD, MAXFORWARD);

            if (Input.GetKey(GameManager.Manager.GameSettings.KeyHeroSprint))
                forward *= 1.5f;
        }
        else
        {
            if (forward < 0)
            {
                forward += ForwardSpeed * Time.deltaTime * 4;
                if (forward > 0)
                    forward = 0;
            }
            else if (forward > 0)
            {
                forward -= ForwardSpeed * Time.deltaTime * 4;
                if (forward < 0)
                    forward = 0;
            }
        }

        animator.SetFloat("Forward", forward);
    }

    private void GetJumpInput()
    {
        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroJump))
        {

        }
    }

    private void ControllSkillCamera()
    {
        if (currentSkill == null) return;

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, SkillCamPos.position, CameraChangeDamping);
        Camera.main.transform.LookAt(SkillCamLookAtPos.position);
    }

    private void GetCameraInput()
    {
        if (currentSkill != null) return;

        x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

        y = Utilitys.ClampAngel(y, yMinLimit, yMaxLimit);

        Quaternion rotation = Quaternion.Euler(y, x, 0);

        distance = Vector3.Distance(Camera.main.transform.position, transform.position);
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 wantedPosition = rotation * negDistance + CamTargetPos.position;

        Camera.main.transform.rotation = rotation;
        Camera.main.transform.position = Vector3.Lerp(transform.position, wantedPosition, CameraSpeed);
    }

    private void GetTurnInput()
    {
        lasRot = transform.rotation;

        transform.eulerAngles = new Vector3(
            0,
            Mathf.LerpAngle(transform.eulerAngles.y, Camera.main.transform.eulerAngles.y, TurnSpeed * Time.deltaTime),
            0);

        if (Mathf.Abs(lasRot.y - transform.rotation.y) >= MINTURNDIF)
            turn += TurnSpeed * Time.deltaTime;
        else
            turn -= TurnSpeed * Time.deltaTime;

        turn = Mathf.Clamp(turn, 0, MAXTURN);

        if (IsInMotion)
            turn = 0;

        animator.SetFloat("Turn", turn);
    }

    private void HighLightTarget()
    {
        int x = Screen.width / 2;
        int y = Screen.height / 2;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y));
        RaycastHit info;

        if (Physics.Raycast(ray, out info, TARGET_RAY_LENGHT, TargetRayLayerMask))
        {
            if (info.transform.gameObject.tag == HIGHLIGHT_TAG)
            {
                Component[] components = info.transform.gameObject.GetComponents(typeof(Component));

                for (int i = 0; i < components.Length; i++)
                {
                    if (components[i] is IHighlightable)
                    {
                        MainAttackTargetTransform = components[i].transform;
                        (components[i] as IHighlightable).HighLight();
                        break;
                    }
                }
                return;
            }

            Building tempBuilding = info.transform.gameObject.GetComponent<Building>();
            if (tempBuilding != null)
            {
                if (tempBuilding is IRepeirable)
                {
                    (tempBuilding as IRepeirable).ShowRepeirable(info.point);
                    buildingToHeal = tempBuilding;
                }

                MainAttackTargetTransform = null;
                hideCrosshair = true;
            }
            else
            {
                hideCrosshair = false;

                if (buildingToHeal != null)
                    Destroy((buildingToHeal as IRepeirable).GetRepairCanvas());

                GameManager.Manager.DestroyAllRepairCanvases();

                buildingToHeal = null;
            }
        }
        else
        {
            MainAttackTargetTransform = null;
            hideCrosshair = false;

            if (buildingToHeal != null)
                Destroy((buildingToHeal as IRepeirable).GetRepairCanvas());

            GameManager.Manager.DestroyAllRepairCanvases();

            buildingToHeal = null;
        }
    }

    private void GetHealTargetInput()
    {
        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroRepair) && buildingToHeal != null)
        {
            if (buildingToHeal.Health.CurrentHealth != buildingToHeal.Health.MaxHealth)
            {
                buildingToHeal.RepairBuilding();
            }
            else
            {
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Allready Max health", "The Building you are trying to repair is allready repaired.", 1.75f);
            }
        }
    }

    private void HandleCurrentSkillPanel()
    {
        if (currentSkill != null)
        {
            Cursor.lockState = CursorLockMode.None;
            GameManager.Manager.ShowCurrentSkillPanel(currentSkill.Image, currentSkill.Name, currentSkill.Description, currentSkill.GodPointsCost.ToString(), currentSkill.Effect.ToString());
        }
        else
        {
            if (IsShowingSkillBuyPanel)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;

            GameManager.Manager.HideCurrentSkillPanel();
        }
    }

    IEnumerator LayerWeightTimer(float delay, int layerIndex, float targetWeight)
    {
        yield return new WaitForSeconds(delay);

        StartCoroutine(LayerWeightLerper(layerIndex, targetWeight, 0.1f));
    }

    IEnumerator LayerWeightLerper(int layerIndex, float targetWeight, float speed)
    {
        yield return new WaitForEndOfFrame();

        float layerWeight = animator.GetLayerWeight(layerIndex);
        layerWeight = Mathf.Lerp(layerWeight, targetWeight, speed);

        animator.SetLayerWeight(layerIndex, layerWeight);

        if (Mathf.Abs(layerWeight - targetWeight) > 0.1f)
            StartCoroutine(LayerWeightLerper(layerIndex, targetWeight, speed));
        else
            animator.SetLayerWeight(layerIndex, 0);
    }

    IEnumerator MainAttackTimer()
    {
        FireInHandPrefab.SetActive(false);
        IsAttacking = true;
        canAttack = false;

        yield return new WaitForSeconds(MAINATTACKDELAY);

        FireInHandPrefab.SetActive(true);
        canAttack = true;
        IsAttacking = false;
    }

    IEnumerator MainAttackAnimDelay()
    {
        yield return new WaitForSeconds(0.35f);

        GameObject projectileGameObject = ProjectilesPool.GetObject(ProjectilePosToSpawn.position);
        Projectile projectile = projectileGameObject.GetComponent<Projectile>();

        float damageMargin = Damage / 100 * DAMAGERMARGINPERCENT;

        Vector3 direction = Camera.main.transform.forward;
        direction.y = 0;

        projectile.SetValues(direction, UnityEngine.Random.Range(Damage - damageMargin, Damage + damageMargin), AttackSpeed * 1.5f, MaxDistance, Range, ProjectilesPool);
        if (MainAttackTargetTransform != null)
            projectile.SetTarget(MainAttackTargetTransform);

        projectile.Fire();
    }

    IEnumerator SkillAttackTimer()
    {
        FireInHandPrefab.SetActive(false);
        IsUsingSkill = true;

        yield return new WaitForSeconds(SKILLATTACKDELAY);

        FireInHandPrefab.SetActive(true);
        IsUsingSkill = false;
    }

    IEnumerator SpawningTimer(GameObject heroObject)
    {
        yield return new WaitForSeconds(RespawnDelay);

        heroObject.SetActive(true);
        Respawn();
    }

    IEnumerator RadialImageUpdate()
    {
        yield return new WaitForFixedUpdate();

        deathTimer += Time.deltaTime;

        SpawnDelayText.text = string.Format("{0:0.0}", Mathf.Abs(deathTimer - RespawnDelay));
        RadialImage.fillAmount = Mathf.Abs((deathTimer / RespawnDelay) - 1);

        if (deathTimer <= RespawnDelay)
            GameManager.Manager.StartCoroutine(RadialImageUpdate());
        else
        {
            RadialImage.fillAmount = 0;
            deathTimer = 0;
            SpawnDelayText.text = string.Empty;
        }
    }

    IEnumerator HitEffectTimer()
    {
        yield return new WaitForSeconds(TimeToShowEffectOnHit / HitEffectSteps);

        hitEffectTimer += Time.deltaTime;

        Color tempColor = GameManager.Manager.GetHitEffectImage().color;
        tempColor.a -= (float)1 / HitEffectSteps;
        if (tempColor.a < 0) tempColor.a = 0;
        GameManager.Manager.GetHitEffectImage().color = tempColor;

        if (tempColor.a > 0)
            hitEffectCoroutine = StartCoroutine(HitEffectTimer());
    }

    IEnumerator SkillsJustChangedTimer()
    {
        yield return new WaitForSeconds(0.5f);
        skillsJustChanged = false;
    }
}
