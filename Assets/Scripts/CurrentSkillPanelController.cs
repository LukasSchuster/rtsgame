﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentSkillPanelController : MonoBehaviour
{
    public Image SkillImage;

    public Text SkillNameText;
    public Text SkillDescribtionText;
    public Text SkillCostText;
    public Text SkillEffectText;

    private bool isShowing = false;

    public void SetCurrentSkillPanel(Sprite image, string name, string describtion, string cost, string effect)
    {
        if (isShowing) return;

        this.SkillImage.sprite = image;
        this.SkillNameText.text = name;
        this.SkillDescribtionText.text = describtion;
        this.SkillCostText.text = string.Format("Mana Cost: {0}", cost);

        switch (effect)
        {
            case "Poison":
                this.SkillEffectText.text = string.Format("Effect: <color=#00FF21FF>{0}</color>", "Posion");
                break;

            case "SlowEffect":
                this.SkillEffectText.text = string.Format("Effect: <color=#00DFFFFF>{0}</color>", "Slow");
                break;

            case "StunEffect":
                this.SkillEffectText.text = string.Format("Effect: <color=#FFED00FF>{0}</color>", "Stun");
                break;

            default:
                this.SkillEffectText.text = string.Format("Effect: <color=#FFFFFFFF>{0}</color>", effect);
                break;
        }

        Show();
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
        isShowing = true;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        isShowing = false;
    }
}
