﻿using UnityEngine;
using UnityEngine.UI;

public class DamageIndicator : MonoBehaviour
{
    private const float MARGIN = 0.5f;
    private const float SPEEDMARGIN = 0.7f;

    public static void Create(Vector3 pos, float damage, float duration, float riseSpeed, Color color)
    {
        GameObject indicator = Instantiate(GameManager.Manager.GetDemageIndicator(), GetRandomizedPos(pos), Quaternion.identity) as GameObject;

        Text text = indicator.GetComponentInChildren<Text>();
        text.text = string.Format("{0:0.00}", damage);
        text.color = color;

        indicator.GetComponent<DamageIndicatorBehaviour>().RiseSpeed = GetRandomizedFloat(riseSpeed);
        indicator.GetComponent<AutoRemove>().LifeTime = duration;
    }

    public static void Create(Vector3 pos, string textvalue, float duration, float riseSpeed, Color color)
    {
        GameObject indicator = Instantiate(GameManager.Manager.GetDemageIndicator(), GetRandomizedPos(pos), Quaternion.identity) as GameObject;

        Text text = indicator.GetComponentInChildren<Text>();
        text.text = textvalue;
        text.color = color;

        indicator.GetComponent<DamageIndicatorBehaviour>().RiseSpeed = GetRandomizedFloat(riseSpeed);
        indicator.GetComponent<AutoRemove>().LifeTime = duration;
    }

    private static Vector3 GetRandomizedPos(Vector3 pos)
    {
        Vector3 temp = pos;
        temp.x += Random.Range(-MARGIN, MARGIN);
        temp.y += Random.Range(-MARGIN, MARGIN);
        temp.z += Random.Range(-MARGIN, MARGIN);

        return temp;
    }

    private static float GetRandomizedFloat(float f)
    {
        return (f += Random.Range(-SPEEDMARGIN, SPEEDMARGIN));
    }
}
