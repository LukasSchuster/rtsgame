﻿using UnityEngine;
using System.Collections;

public class Wave
{
    public delegate void AllDeadDelegate();
    public event AllDeadDelegate OnWaveEnd;

    [SerializeField]
    public GameObject toSpawn;
    [SerializeField]
    public int amount;
    [SerializeField]
    public float delay;
    [SerializeField]
    public float frontDelay;
    [SerializeField]
    public Vector3 positionToSpawn;

    public int spawnedCounter = 0;
    private int remaining;

    public Wave(GameObject toSpawn, Vector3 pos, int amount, float delay)
    {
        this.toSpawn = toSpawn;
        this.positionToSpawn = pos;
        this.amount = amount;
        this.delay = delay;
        this.remaining = amount;
    }

    public void SpawnedElement()
    {
        spawnedCounter++;
    }

    public void Decrement()
    {
        remaining--;

        if (remaining <= 0)
            if (OnWaveEnd != null) OnWaveEnd();
    }
}
