﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialPanelController : MonoBehaviour
{
    public Image Image;
    public Text Name;
    public Text Description;
    public Text Hint;
    public Text TutorialText;
}
