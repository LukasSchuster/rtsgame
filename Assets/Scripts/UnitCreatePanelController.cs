﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class UnitCreatePanelController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("UI")]
    public Button SubHero1;
    public Button SubHero2;

    public Button SubHero1_Upgrade1;
    public Button SubHero1_Upgrade2;

    public Button SubHero2_Upgrade1;
    public Button SubHero2_Upgrade2;

    [Header("UI Feedback")]
    public Image SubHero_1_ProgressImage;
    public Image SubHero_2_ProgressImage;

    public Text SubHeroCounter_1;
    public Text SubHeroCounter_2;

    private bool canCreateSubHero1 = true;
    private bool canCreateSubHero2 = true;

    private SubheroesCreatorController currentController;

    private float progress1Timer;
    private float progress2Timer;

    void Awake()
    {
        SubHero1.onClick.AddListener(delegate { OnSubHero1Clicked(); });
        SubHero2.onClick.AddListener(delegate { OnSubHero2Clicked(); });
    }

    private void OnSubHero2Clicked()
    {
        if (canCreateSubHero2)
        {
            if (GetSelectedBuilding() && currentController != null  )
            {
                StartCoroutine(SpawnTimer(currentController.Subhero_2, currentController.SpawnPosition.position, currentController.Delay, 2));
                canCreateSubHero2 = false;
            }
        }
        else
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "There is allready a Unit in Creation!", "", 1.5f);
    }

    private void OnSubHero1Clicked()
    {
        if (canCreateSubHero1)
        {
            if(GetSelectedBuilding() && currentController != null)
            {
                StartCoroutine(SpawnTimer(currentController.SubHero_1, currentController.SpawnPosition.position, currentController.Delay, 1));
                canCreateSubHero1 = false;
            }
        }
        else
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "There is allready a Unit in Creation!", "", 1.5f);
    }

    private bool GetSelectedBuilding()
    {
        if (GameManager.SelectedObjects[0] is SubheroesCreatorController)
        {
            currentController = GameManager.SelectedObjects[0] as SubheroesCreatorController;
            return true;
        }
        else
            return false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.UIPressed();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.UIDePressed();
    }

    IEnumerator SpawnTimer(GameObject toSpawn, Vector3 pos, float duration, int indexBoolToReset)
    {
        Image temp = indexBoolToReset == 1 ? SubHero_1_ProgressImage : SubHero_2_ProgressImage;
        if (indexBoolToReset == 1) SubHero_1_ProgressImage.fillAmount = 1;
        else if (indexBoolToReset == 2) SubHero_2_ProgressImage.fillAmount = 1;
        StartCoroutine(ProgressUpdate(temp, duration));

        yield return new WaitForSeconds(duration);

        Instantiate(toSpawn, pos, Quaternion.identity);

        if (indexBoolToReset == 1) canCreateSubHero1 = true;
        else if (indexBoolToReset == 2) canCreateSubHero2 = true; 
    }

    IEnumerator ProgressUpdate(Image toControll, float duration)
    {
        yield return new WaitForFixedUpdate();

        if (toControll == SubHero_1_ProgressImage)
        {
            progress1Timer += Time.deltaTime;

            SubHeroCounter_1.text = string.Format("{0:0.0}", Mathf.Abs(progress1Timer - duration));
            toControll.fillAmount = Mathf.Abs((progress1Timer / duration) - 1);

            if (progress1Timer < duration) StartCoroutine(ProgressUpdate(toControll, duration));
            else
            {
                progress1Timer = 0;
                toControll.fillAmount = 0;
                SubHeroCounter_1.text = string.Empty;
            }
        }
        else
        {
            progress2Timer += Time.deltaTime;

            SubHeroCounter_2.text = string.Format("{0:0.0}", Mathf.Abs(progress2Timer - duration));
            toControll.fillAmount = Mathf.Abs((progress1Timer / duration) - 1);

            if (progress2Timer < duration) StartCoroutine(ProgressUpdate(toControll, duration));
            else
            {
                progress2Timer = 0;
                toControll.fillAmount = 0;
                SubHeroCounter_2.text = string.Empty;
            }
        }
    }
}
