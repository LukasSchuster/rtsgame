﻿using UnityEngine;
using System.Collections.Generic;

public class WayPointManager
{
    public static List<WayPoint> WayPoints = new List<WayPoint>();

    public static WayPoint GetClosestWayPoint(Vector3 currentPos)
    {
        float distance = 10000;

        WayPoint toReturn = null;

        for (int i = 0; i < WayPoints.Count; i++)
        {
            if (Vector3.Distance(currentPos, WayPoints[i].Position) <= distance)
            {
                toReturn = WayPoints[i];
                distance = Vector3.Distance(currentPos, WayPoints[i].Position);
            }
        }

        if (toReturn != null)
            return toReturn;
        else
            throw new System.Exception("ToReturn in WayPointManager is Null");
    }
}
