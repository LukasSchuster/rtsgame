﻿//using System;
//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;
//using System.Collections.Generic;


//public class NewController : MonoBehaviour
//{
//    private const int MAXSKILLDISTANCE = 40;
//    private const int DAMAGERMARGINPERCENT = 10;

//    private const float NEUTRAL = 0f;
//    private const float MAXFORWARD = 0.5f;
//    private const float MAXFORWARDRUN = 2.2f;
//    private const float MINFORWARD = -1f;
//    private const float MINFORWARDRUN = -2f;
//    private const float RUNSPEED = 1.3f;

//    private const float MAXRIGHT = 2f;
//    private const float MINRIGHT = -2f;

//    private const float MINTURN = -1;
//    private const float MAXTURN = 1;

//    private const float MINTURNDIF = 0.001f;

//    private const float MAXDEGTOROTATION = 0.1f;
//    private const float LAYERRISESPEED = 0.05f;

//    private const float MAINATTACKDELAY = 1.2f;

//    public delegate void HeroActivated();
//    public event HeroActivated OnHeroActivated;

//    public delegate void HeroDeActivated();
//    public event HeroDeActivated OnHeroDeActivated;

//    public float ForwardSpeed = 0.1f;
//    public float SideWaySpeed = 0.3f;
//    public float TurnSpeed = 0.1f;
//    public float CameraSpeed = 0.2f;
//    public float CameraTurnSpeed = 0.2f;

//    [Header("Skills")]
//    public List<Skill> Skills = new List<Skill>();
//    public GameObject SkillTargetProjectorPrefab;

//    private Skill currentSkill;
//    private StrategieCameraController controller;

//    private Animator animator;
//    private CharacterController characterController;

//    [Header("SkillUI")]
//    public Texture2D TargetCurser;

//    [Header("Main Attack")]
//    public GameObject Projectile;
//    public Transform ProjectileSpawnPos;
//    public float Damage;
//    public float Speed;
//    public float MaxDistance;
//    public float Range;

//    [HideInInspector]
//    public ObjectPool<Projectile> ProjectilesPool;

//    [Header("UI")]
//    public Image RadialImage;
//    public Text SpawnDelayText;

//    [Header("On Death")]
//    public AudioClip DeathSound;
//    public float RespawnDelay = 10f;
//    private float deathTimer = 0;

//    [Header("On Damaged")]
//    public float TimeToShowEffectOnHit = 3f;
//    public float HitEffectSteps;
//    private float hitEffectTimer;
//    private Coroutine hitEffectCoroutine;

//    [Header("Projectile")]
//    public Transform ProjectilePos;

//    [HideInInspector]
//    public Health health;

//    private FogOfWarElement fogOfWarElement;
//    private bool cameraReachedHero = false;
//    private bool isHudHidden = false;
//    private bool skillsJustChanged = false;

//    private float forward = 0;
//    private float right = 0;
//    private float turn = 0;

//    public Transform CamTargetPos;
//    public Transform CamStartPos;
//    private float x = 0;
//    private float y = 0;
//    private float xSpeed = 60f;
//    private float ySpeed = 120f;
//    private float distance = 5f;
//    private float yMinLimit = -10f;
//    private float yMaxLimit = 80f;
//    private float distanceMin = 2f;
//    private float distanceMax = 5f;
//    private float maxCamheight = 30f;
//    private float minCamheight = 2f;

//    private Quaternion lasRot;

//    [HideInInspector]
//    public bool IsInMotion = false;
//    [HideInInspector]
//    public bool IsIdleing = false;
//    [HideInInspector]
//    public bool IsTurning = false;
//    [HideInInspector]
//    public bool IsAttacking = false;
//    [HideInInspector]
//    public bool IsRunning = false;

//    private bool canAttack = true;

//    void Awake()
//    {
//        animator = GetComponent<Animator>();
//        characterController = GetComponent<CharacterController>();
//        ProjectilesPool = new ObjectPool<Projectile>(Projectile, 2);
//        health = GetComponent<Health>();
//    }

//    void Start()
//    {
//        controller = GameManager.Manager.GetGameController();
//        SkillTargetProjectorPrefab = Instantiate(SkillTargetProjectorPrefab, Vector3.zero, SkillTargetProjectorPrefab.transform.rotation) as GameObject;
//        fogOfWarElement = FogOfWarManager.AddElement(health, 1f);
//        health.OnDeath += Health_OnDeath;
//        health.OnDamaged += Health_OnDamaged;
//        UnitManager.Manager.AddFriendlyUnit(health);
//    }

//    void Update()
//    {
//        if (GameManager.Manager.IsGamePaused())
//        {
//            if (Input.GetKeyDown(GameSettings.KeyHeroPause))
//                GameManager.Manager.ResumeGame();
//            return;
//        }

//        if (controller.CamMode == StrategieCameraController.CameraMode.HeroMode)
//        {
//            if (cameraReachedHero)
//            {
//                ResetStates();
//                HandleStates();
//                GetCameraInput();
//                GetMoveInputForwardsBackwards();
//                GetMoveInputRightLeft();
//                GetTurnInput();
//                GetAttackInput();
//                ChooseSkill();
//                SkillInput();
//                KeyInput();
//            }
//            else
//                PlaceCameraAtHero();
//        }
//    }

//    private void Health_OnDamaged(float damage)
//    {
//        if (controller.CamMode != StrategieCameraController.CameraMode.HeroMode)
//            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "You´r Hero is beeing Attacked!", "", 1.5f);
//        else
//        {
//            if (hitEffectCoroutine != null)
//                StopCoroutine(hitEffectCoroutine);

//            Color tempColor = GameManager.Manager.GetHitEffectImage().color;
//            tempColor.a = 0.7f;
//            GameManager.Manager.GetHitEffectImage().color = tempColor;

//            hitEffectCoroutine = StartCoroutine(HitEffectTimer());
//        }
//    }

//    private void Health_OnDeath()
//    {
//        if (fogOfWarElement != null)
//            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

//        if (controller.CamMode == StrategieCameraController.CameraMode.SelectMode)
//            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "You´r Hero died!", "", 2f);

//        if (DeathSound != null)
//            AudioSource.PlayClipAtPoint(DeathSound, GameManager.Manager.GetGameController().transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

//        if (controller.CamMode == StrategieCameraController.CameraMode.HeroMode)
//            DeActivateHeroMode();

//        UnitManager.Manager.RemoveFriendlyUnit(health);

//        GameManager.Manager.StartCoroutine(SpawningTimer(gameObject));

//        RadialImage.fillAmount = 1;
//        GameManager.Manager.StartCoroutine(RadialImageUpdate());

//        transform.position = GameManager.Manager.GetMainBuildingController().HeroSpawnPosition.position;

//        gameObject.SetActive(false);
//    }

//    public void ActivateHeroMode()
//    {
//        if (OnHeroActivated != null)
//            OnHeroActivated();

//        foreach (Skill skill in Skills)
//            skill.canFireSkill = true;

//        foreach (var item in GameManager.SelectedObjects)
//            item.OnDeSelected(ref GameManager.SelectedObjects);

//        GameManager.Manager.GetRessources().HeroHealth_OnChanged();

//        GameManager.Manager.ChangeHud(true);
//        GameManager.SelectedObjects.Clear();
//    }

//    public void DeActivateHeroMode()
//    {

//        ChangeHudView(true);

//        if (OnHeroDeActivated != null)
//            OnHeroDeActivated();

//        if (hitEffectCoroutine != null)
//            StopCoroutine(hitEffectCoroutine);

//        cameraReachedHero = false;

//        ChangeCurser(false);

//        turn = 0;
//        forward = 0;
//        right = 0;

//        animator.SetFloat("Right", right);
//        animator.SetFloat("Forward", forward);
//        animator.SetFloat("Turn", turn);

//        Color tempColor = GameManager.Manager.GetHitEffectImage().color;
//        tempColor.a = 0;
//        GameManager.Manager.GetHitEffectImage().color = tempColor;

//        GameManager.Manager.ActivateHeroSkillsPanel(false);
//        GameManager.Manager.ChangeHud(false);
//        GameManager.Manager.GetGameController().ChangeModeTo("SelectMode");
//        Camera.main.transform.rotation = GameManager.Manager.GetGameController().CamRotation;
//        Camera.main.transform.position = GameManager.Manager.GetGameController().CamPosition;
//    }

//    public void SkillsJustChanged()
//    {
//        skillsJustChanged = true;

//        StartCoroutine(SkillsJustChangedTimer());
//    }

//    public void ChooseSkill(int index)
//    {
//        switch (index)
//        {
//            case 1:
//                if (Skills.Count >= 1)
//                    currentSkill = Skills[0];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 2:
//                if (Skills.Count >= 2)
//                    currentSkill = Skills[1];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 3:
//                if (Skills.Count >= 3)
//                    currentSkill = Skills[2];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 4:
//                if (Skills.Count >= 4)
//                    currentSkill = Skills[3];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 5:
//                if (Skills.Count >= 5)
//                    currentSkill = Skills[4];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 6:
//                if (Skills.Count >= 6)
//                    currentSkill = Skills[5];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            default:
//                currentSkill = null;
//                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                break;
//        }

//        if (currentSkill != null)
//            ChangeCurser(true);
//        else
//            ChangeCurser(false);
//    }

//    private void ChooseSkill()
//    {
//        if (!Input.anyKey)
//            return;

//        string keyPressedString = Input.inputString;
//        int keyPressed;
//        bool result = int.TryParse(keyPressedString, out keyPressed);

//        if (currentSkill != null)
//            ChangeCurser(true);
//        else
//            ChangeCurser(false);

//        if (!result)
//            return;

//        switch (keyPressed)
//        {
//            case 1:
//                if (Skills.Count >= 1)
//                    currentSkill = Skills[0];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 2:
//                if (Skills.Count >= 2)
//                    currentSkill = Skills[1];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 3:
//                if (Skills.Count >= 3)
//                    currentSkill = Skills[2];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 4:
//                if (Skills.Count >= 4)
//                    currentSkill = Skills[3];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 5:
//                if (Skills.Count >= 5)
//                    currentSkill = Skills[4];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            case 6:
//                if (Skills.Count >= 6)
//                    currentSkill = Skills[5];
//                else
//                {
//                    currentSkill = null;
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                }
//                break;

//            default:
//                currentSkill = null;
//                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Skill", "", 1.2f);
//                break;
//        }
//    }

//    private void PlaceCameraAtHero()
//    {
//        var wantedPosition = CamStartPos.position;

//        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, wantedPosition, CameraSpeed * 10);
//        Camera.main.transform.LookAt(transform.position);

//        if (Vector3.Distance(Camera.main.transform.position, wantedPosition) <= 0.4f)
//        {
//            Camera.main.transform.position = wantedPosition;
//            cameraReachedHero = true;
//        }
//    }

//    private void SkillInput()
//    {
//        if (currentSkill != null)
//        {
//            if (Input.GetMouseButtonDown(0))
//            {
//                if (Vector3.Distance(transform.position, SkillTargetProjectorPrefab.transform.position) >= MAXSKILLDISTANCE)
//                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Skill Target too far Away!", "", 2f);
//                else
//                {
//                    currentSkill.OnActivated();
//                    currentSkill = null;
//                }
//            }

//            Vector3 worldPos = Utilitys.GiveMousePositionInWorldOnFloor();

//            if (SkillTargetProjectorPrefab.activeSelf)
//                SkillTargetProjectorPrefab.transform.position = new Vector3(worldPos.x, worldPos.y + 3.5f, worldPos.z);
//            else
//            {
//                SkillTargetProjectorPrefab.SetActive(true);
//                SkillTargetProjectorPrefab.transform.position = new Vector3(worldPos.x, worldPos.y + 3.5f, worldPos.z);
//            }
//        }
//        else
//        {
//            if (SkillTargetProjectorPrefab.activeSelf)
//                SkillTargetProjectorPrefab.SetActive(false);
//        }

//        if (Vector3.Distance(transform.position, SkillTargetProjectorPrefab.transform.position) >= MAXSKILLDISTANCE)
//            SkillTargetProjectorPrefab.SetActive(false);
//    }

//    private void KeyInput()
//    {
//        if (GameManager.Manager.TextInputMode) return;

//        if (Input.GetKeyDown(GameSettings.KeyHeroHideHud))
//            ChangeHudView();

//        if (Input.GetKeyDown(GameSettings.KeyHeroPause))
//        {
//            if (skillsJustChanged)
//                GameManager.Manager.GetHeroSkillsPanelController().OnBackButtonClicked();
//            else
//            {
//                if (GameManager.Manager.IsGamePaused()) GameManager.Manager.ResumeGame();
//                else GameManager.Manager.PauseGame();
//            }
//        }

//        if (Input.GetKeyDown(GameSettings.KeySwitchMode))
//        {
//            controller.HeroModeJustChanged = true;
//            DeActivateHeroMode();
//        }

//        if (Input.GetKeyDown(GameSettings.KeyHeroSkills))
//        {
//            GameManager.Manager.ActivateHeroSkillsPanel(GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf ? false : true);

//            if (GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf)
//            {
//                Time.timeScale = 0;
//                controller.ChangeModeTo("SkillMode");
//            }
//            else
//            {
//                Time.timeScale = 1;
//                GameManager.Manager.GetHeroSkillsPanelController().OnBackButtonClicked();
//            }
//        }
//    }

//    private void ChangeCurser(bool skillMode)
//    {
//        if (!skillMode)
//            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
//        else
//            Cursor.SetCursor(TargetCurser, Vector2.zero, CursorMode.Auto);
//    }

//    private void Respawn()
//    {
//        fogOfWarElement = FogOfWarManager.AddElement(health, 1f);

//        health.Reset();
//        transform.position = GameManager.Manager.GetMainBuildingController().HeroSpawnPosition.position;
//        UnitManager.Manager.AddFriendlyUnit(health);
//    }

//    private void ChangeHudView()
//    {
//        if (isHudHidden)
//        {
//            for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
//                GameManager.Manager.GetHeroHudElements()[i].SetActive(true);
//            isHudHidden = false;
//        }
//        else
//        {
//            for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
//                GameManager.Manager.GetHeroHudElements()[i].SetActive(false);
//            isHudHidden = true;
//        }
//    }

//    private void ChangeHudView(bool value)
//    {
//        for (int i = 0; i < GameManager.Manager.GetHeroHudElements().Length; i++)
//            GameManager.Manager.GetHeroHudElements()[i].SetActive(value);

//        isHudHidden = value;
//    }

//    private void HandleStates()
//    {
//        if (right != 0 || forward != 0) 
//            IsInMotion = true;

//        if (forward >= RUNSPEED)
//            IsRunning = true;
//    }

//    private void ResetStates()
//    {
//        IsInMotion = false;
//        IsIdleing = false;
//        IsTurning = false;
//        IsRunning = false;
//        IsAttacking = false;
//    }

//    private void GetAttackInput()
//    {
//        if (Input.GetMouseButtonDown(0) && canAttack && !IsRunning)
//        {
//            animator.SetLayerWeight(1, 1);
//            animator.SetTrigger("Attack");
//            StartCoroutine(LayerWeightTimer(1f, 1, 0));
//            StartCoroutine(MainAttackTimer());
//        }
//    }

//    private void GetMoveInputRightLeft()
//    {
//        if (Input.GetKey(GameSettings.KeyHeroMoveLeft))
//            right -= SideWaySpeed;
//        else if (Input.GetKey(GameSettings.KeyHeroMoveRight))
//            right += SideWaySpeed;
//        else
//        {
//            if (right < 0)
//            {
//                right += SideWaySpeed;
//                if (right > 0)
//                    right = 0;
//            }
//            else if (right > 0)
//            {
//                right -= SideWaySpeed;
//                if (right < 0)
//                    right = 0;
//            }
//        }

//        right = Mathf.Clamp(right, MINRIGHT, MAXRIGHT);

//        animator.SetFloat("Right", right);
//    }

//    private void GetMoveInputForwardsBackwards()
//    {
//        if (Input.GetKey(GameSettings.KeyHeroMoveForward) && Input.GetKey(GameSettings.KeyHeroSprint))
//        {
//            forward += ForwardSpeed * 2;
//            forward = Mathf.Clamp(forward, NEUTRAL, MAXFORWARDRUN);
//        }
//        else if (Input.GetKey(GameSettings.KeyHeroMoveForward))
//        {
//            if (forward > MAXFORWARD)
//                forward -= ForwardSpeed;
//            else
//            {
//                forward += ForwardSpeed;
//                forward = Mathf.Clamp(forward, NEUTRAL, MAXFORWARD);
//            }
//        }
//        else if(Input.GetKey(GameSettings.KeyHeroMoveBackwards))
//        {
//            forward -= ForwardSpeed;
//            forward = Mathf.Clamp(forward, MINFORWARD, MAXFORWARD);
//        }
//        else
//        {
//            if (forward < 0)
//            {
//                forward += ForwardSpeed;
//                if (forward > 0)
//                    forward = 0;
//            }
//            else if (forward > 0)
//            {
//                forward -= ForwardSpeed;
//                if (forward < 0)
//                    forward = 0;
//            }
//        }

//        animator.SetFloat("Forward", forward);
//    }

//    private void GetCameraInput()
//    {
//        x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
//        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

//        y = Utilitys.ClampAngel(y, yMinLimit, yMaxLimit);

//        Quaternion rotation = Quaternion.Euler(y, x, 0);

//        distance = Vector3.Distance(Camera.main.transform.position, transform.position);
//        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

//        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
//        Vector3 wantedPosition = rotation * negDistance + CamTargetPos.position;

//        Camera.main.transform.rotation = rotation;
//        //Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, wantedPosition, CameraSpeed);
//        Camera.main.transform.position = Vector3.Lerp(transform.position, wantedPosition, CameraSpeed);
//    }

//    private void GetTurnInput()
//    {
//        lasRot = transform.rotation;

//        transform.eulerAngles = new Vector3(
//            0,
//            Mathf.LerpAngle(transform.eulerAngles.y, Camera.main.transform.eulerAngles.y, TurnSpeed),
//            0);

//        if (Mathf.Abs(lasRot.y - transform.rotation.y) >= MINTURNDIF)
//            turn += TurnSpeed;
//        else
//            turn -= TurnSpeed;

//        turn = Mathf.Clamp(turn, 0, MAXTURN);

//        if (IsInMotion)
//            turn = 0;

//        animator.SetFloat("Turn", turn);
//    }

//    IEnumerator LayerWeightTimer(float delay, int layerIndex, float targetWeight)
//    {
//        yield return new WaitForSeconds(delay);

//        StartCoroutine(LayerWeightLerper(layerIndex, targetWeight, 0.1f));
//    }

//    IEnumerator LayerWeightLerper(int layerIndex, float targetWeight, float speed)
//    {
//        yield return new WaitForEndOfFrame();

//        float layerWeight = animator.GetLayerWeight(layerIndex);
//        layerWeight = Mathf.Lerp(layerWeight, targetWeight, speed);

//        animator.SetLayerWeight(layerIndex, layerWeight);

//        if (Mathf.Abs(layerWeight - targetWeight) > 0.1f)
//            StartCoroutine(LayerWeightLerper(layerIndex, targetWeight, speed));
//        else
//            animator.SetLayerWeight(layerIndex, 0);
//    }

//    IEnumerator MainAttackTimer()
//    {
//        IsAttacking = true;
//        canAttack = false;

//        yield return new WaitForSeconds(MAINATTACKDELAY);

//        canAttack = true;
//        IsAttacking = false;
//    }

//    IEnumerator SpawningTimer(GameObject heroObject)
//    {
//        yield return new WaitForSeconds(RespawnDelay);

//        heroObject.SetActive(true);
//        Respawn();
//    }

//    IEnumerator RadialImageUpdate()
//    {
//        yield return new WaitForFixedUpdate();

//        deathTimer += Time.deltaTime;

//        SpawnDelayText.text = string.Format("{0:0.0}", Mathf.Abs(deathTimer - RespawnDelay));
//        RadialImage.fillAmount = Mathf.Abs((deathTimer / RespawnDelay) - 1);

//        if (deathTimer <= RespawnDelay)
//            GameManager.Manager.StartCoroutine(RadialImageUpdate());
//        else
//        {
//            RadialImage.fillAmount = 0;
//            deathTimer = 0;
//            SpawnDelayText.text = string.Empty;
//        }
//    }

//    IEnumerator HitEffectTimer()
//    {
//        yield return new WaitForSeconds(TimeToShowEffectOnHit / HitEffectSteps);

//        hitEffectTimer += Time.deltaTime;

//        Color tempColor = GameManager.Manager.GetHitEffectImage().color;
//        tempColor.a -= (float)1 / HitEffectSteps;
//        if (tempColor.a < 0) tempColor.a = 0;
//        GameManager.Manager.GetHitEffectImage().color = tempColor;

//        if (tempColor.a > 0)
//            hitEffectCoroutine = StartCoroutine(HitEffectTimer());
//    }

//    IEnumerator SkillsJustChangedTimer()
//    {
//        yield return new WaitForSeconds(0.5f);
//        skillsJustChanged = false;
//    }
//}
