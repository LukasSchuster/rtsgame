﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillEffect : MonoBehaviour
{
    public List<Health> Enemys = new List<Health>();
    public float Duration;
    public float Radius;
    public float Delay;
    public float Damage;

    private float timer = 0;
    private float delayTimer = 0;

    public SkillEffect(float damage ,float duration, float radius, float delay, List<Health> enemys)
    {
        Damage = damage;
        Duration = duration;
        Radius = radius;
        Delay = delay;
        Enemys = enemys;

        OnStartFrame();

        GameManager.Manager.StartCoroutine(EveryFrame());
    }

    protected virtual void OnStartFrame() { }

    protected virtual void OnEveryFrame() { }

    protected virtual void OnEveryDelaydFrame() { }

    protected virtual void OnEndFrame() { }

    IEnumerator EveryFrame()
    {
        yield return new WaitForFixedUpdate();
        OnEveryFrame();

        timer += Time.deltaTime;
        delayTimer += Time.deltaTime;

        if (timer < Duration)
            GameManager.Manager.StartCoroutine(EveryFrame());
        else
            OnEndFrame();

        if (delayTimer >= Delay)
        {
            OnEveryDelaydFrame();
            delayTimer = 0;
        }
    }
}
