﻿using UnityEngine;
using System.Collections;

public class DamageIndicatorBehaviour : MonoBehaviour
{
    public float RiseSpeed;

    void Update()
    {
        transform.LookAt(Camera.main.transform.position);
        transform.Translate(Vector3.up * RiseSpeed * Time.deltaTime);
    }
}
