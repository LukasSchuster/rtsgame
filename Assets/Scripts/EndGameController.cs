﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class EndGameController : MonoBehaviour
{
    public Sprite VictorySprite;
    public Sprite DefeatSprite;

    public Button ExitButton;
    public GameObject ControllPanel;

    public Image StatusImage;

    void Start()
    {
        GetComponent<Image>().enabled = false;
        ControllPanel.SetActive(false);

        ExitButton.onClick.AddListener(delegate { OnExitClicked(); });
    }

    private void OnExitClicked()
    {
        Application.Quit();
    }

    public void Victory()
    {
        Time.timeScale = 0;
        StatusImage.sprite = VictorySprite;

        EnablePanel();
    }

    public void Defeat()
    {
        Time.timeScale = 0;
        StatusImage.sprite = DefeatSprite;

        EnablePanel();
    }

    private void EnablePanel()
    {
        Cursor.visible = true;
        GameManager.Manager.GameStats.PrintGameStatsToPanel();

        GetComponent<Image>().enabled = true;
        ControllPanel.SetActive(true);
    }
}
