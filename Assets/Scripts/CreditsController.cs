﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreditsController : MonoBehaviour
{
    public float Speed;
    public RectTransform ParentTransform;


    private Text creditsText;
    private RectTransform myTransform;
    private float startPosY;

    private Coroutine coroutine;

    void Start()
    {
        myTransform = GetComponent<RectTransform>();
        startPosY = myTransform.localPosition.y;
    }

    public void StartCredits()
    {
        if (coroutine != null)
            StopCoroutine(coroutine);

        StartCoroutine(Updater());
    }

    public void StopCredits()
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
    }

    public void ResetCredits()
    {
        Vector3 tempPos = myTransform.localPosition;
        tempPos.y = startPosY;
        myTransform.localPosition = tempPos;
    }

    IEnumerator Updater()
    {
        yield return new WaitForFixedUpdate();

        myTransform.Translate(Vector3.up * Speed);

        if (myTransform.localPosition.y <= Mathf.Abs(startPosY) + ParentTransform.rect.height)
        {
            coroutine = StartCoroutine(Updater());
        }
        else
        {
            ResetCredits();
            coroutine = StartCoroutine(Updater());
        }
    }
}
