﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MainBuildingController : Building, IRepeirable
{
    private const int GP_COST_PER_HEALTHPOINT = 1;

    public Transform HeroSpawnPosition;

    [Header("Outline")]
    public Material OutlineMaterial;
    public Renderer[] MeshRenderers;
    private Coroutine highlightedCoroutine;
    private bool highlighted = false;
    private GameObject repairCanvas;

    private Health myHealth;
    private FogOfWarElement fogOfWarElement;

    void Awake()
    {
        myHealth = GetComponent<Health>();
        AddToBuildingsList();
        GetMaterials();
    }

    void Start()
    {
        OnCreated();
        myHealth.OnDeath += MyHealth_OnDeath;
        myHealth.OnDamaged += MyHealth_OnDamaged;

        fogOfWarElement = FogOfWarManager.AddElement(myHealth, 2);
    }

    private void MyHealth_OnDamaged(float damage)
    {
        HitEffect(0.2f);
        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "The Head Quator is under Attack!", "", 1.5f);
    }

    private void MyHealth_OnDeath()
    {
        GameManager.Manager.GetEndGameController().Defeat();

        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, Description, null);
        base.OnSelected(ref selectedObjects);
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void AddToBuildingsList()
    {
        GameManager.AddBuilding(this);
    }

    public void Destroy()
    {
       
    }

    public GameObject GetRepairCanvas()
    {
        if (repairCanvas != null) return repairCanvas;
        else return null;
    }

    public void ShowRepeirable(Vector3 hitPoint)
    {
        if (!highlighted)
        {
            if (repairCanvas == null)
            {
                repairCanvas = RepairCanvas.Create(
                    hitPoint,
                    Image,
                    Health,
                    Name,
                    Description,
                    string.Format("Repair this by Pressing <color=#ffa500ff>{0}</color>", GameManager.Manager.GameSettings.KeyHeroRepair.ToString()),
                    (GP_COST_PER_HEALTHPOINT * Health.CurrentHealth).ToString(),
                    GP_COST_PER_HEALTHPOINT
                    );
            }

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                tempMats.Add(OutlineMaterial);
                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = true;
        }

        if (highlightedCoroutine != null) StopCoroutine(highlightedCoroutine);
        highlightedCoroutine = GameManager.Manager.StartCoroutine(ShowRepeirableTimer(gameObject));
    }

    public void DeShowRepeirable()
    {
        if (highlighted)
        {
            if (repairCanvas != null)
                Destroy(repairCanvas, 0.2f);

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length - 1; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = false;
        }
    }

    public IEnumerator ShowRepeirableTimer(GameObject gameObjectRef)
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObjectRef != null)
            DeShowRepeirable();
    }
}
