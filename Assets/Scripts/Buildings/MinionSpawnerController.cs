﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MinionSpawnerController : Building, IDestroyable, IRepeirable
{
    private const int GP_COST_PER_HEALTHPOINT = 1;

    [Header("Building Specific Fields")]
    public GameObject MinionPrefab;
    public GameObject SpawnParticlePrefab;
    public Transform MinionSpawnPosition;
    public int MinionFoodCost;
    public int StartDelay = 15;

    [Header("Outline")]
    public Material OutlineMaterial;
    public Renderer[] MeshRenderers;
    private Coroutine highlightedCoroutine;
    private bool highlighted = false;
    private GameObject repairCanvas;

    [HideInInspector]
    public bool Spawning = true;
    [HideInInspector]
    public MinionController MinionController;
    [HideInInspector]
    public ObjectPool<MinionController> MinionPool;
    [HideInInspector]
    public Wave MyWave;

    private Health myHealth;
    private FogOfWarElement fogOfWarElement;

    void Awake()
    {
        MinionPool = new ObjectPool<MinionController>(MinionPrefab, 20);

        for (int i = 0; i < MinionPool.Objetcs.Length; i++)
            MinionPool.Objetcs[i].GetComponent<MinionController>().SetPool(MinionPool);

        myHealth = GetComponent<Health>();
        MinionController = MinionPrefab.GetComponent<MinionController>();
        AddToBuildingsList();
        OnCreated();
        GetMaterials();

        MyWave = new Wave(MinionPrefab, MinionSpawnPosition.position, (int)Amount, 0.8f);
        MyWave.OnWaveEnd += MyWave_OnWaveEnd;
    }

    private void MyWave_OnWaveEnd()
    {
        MyWave = new Wave(MinionPrefab, MinionSpawnPosition.position, (int)Amount, 0.8f);
        MyWave.OnWaveEnd += MyWave_OnWaveEnd;
        StartCoroutine(AfterWaveDelay());
    }

    void Start()
    {
        myHealth.OnDeath += MyHealth_OnDeath;
        myHealth.OnDamaged += MyHealth_OnDamaged;

        StartCoroutine(TimeChecker());
    }

    private void MyHealth_OnDamaged(float damage)
    {
        HitEffect(0.2f);
    }

    private void MyHealth_OnDeath()
    {
        Destroy();
    }

    public override void OnPlaced()
    {
        base.OnPlaced();
        fogOfWarElement = FogOfWarManager.AddElement(myHealth, 1);
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowMinionUpgradePanel();
        GameManager.Manager.ShowPanel(Image, Name, Description, null);
        base.OnSelected(ref selectedObjects);
    }

    public override void AddToBuildingsList()
    {
        GameManager.AddBuilding(this);
    }

    public void Destroy()
    {
        DeShowRepeirable();

        if (PlacedSound != null)
        {
            if (!PlacedSound.isPlaying)
                AudioSource.PlayClipAtPoint(PlacedSound.clip, Camera.main.transform.position);
        }

        if (GetComponent<MapProp>() != null)
            GetComponent<MapProp>().DestroyIcon();

        if (Villagar != null)
            Destroy(Villagar.gameObject);

        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

        GameManager.Manager.HideMinionUpgradePanel();
        GameManager.Manager.HidePanel();
        GameManager.Manager.BuildingMap.FreePosition(transform.position, this);
        GameManager.RemoveSelectableObject(this);
        GameManager.Buildings.Remove(this);
    }

    public void ShowRepeirable(Vector3 hitPoint)
    {
        if (!highlighted)
        {
            if (repairCanvas == null)
            {
                repairCanvas = RepairCanvas.Create(
                    hitPoint,
                    Image,
                    Health,
                    Name,
                    Description,
                    string.Format("Repair this by Pressing <color=#ffa500ff>{0}</color>", GameManager.Manager.GameSettings.KeyHeroRepair.ToString()),
                    (GP_COST_PER_HEALTHPOINT * Health.CurrentHealth).ToString(),
                    GP_COST_PER_HEALTHPOINT
                    );
            }

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                tempMats.Add(OutlineMaterial);
                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = true;
        }

        if (highlightedCoroutine != null) StopCoroutine(highlightedCoroutine);
        highlightedCoroutine = GameManager.Manager.StartCoroutine(ShowRepeirableTimer(gameObject));
    }

    public void DeShowRepeirable()
    {
        if (highlighted)
        {
            if (repairCanvas != null)
                Destroy(repairCanvas, 0.2f);

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length - 1; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = false;
        }
    }

    public GameObject GetRepairCanvas()
    {
        if (repairCanvas != null) return repairCanvas;
        else return null;
    }

    public IEnumerator ShowRepeirableTimer(GameObject gameObjectRef)
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObjectRef != null)
            DeShowRepeirable();
    }

    IEnumerator AfterWaveDelay()
    {
        yield return new WaitForSeconds(Delay);
        StartCoroutine(SpawnWaveElement());
    }

    IEnumerator TimeChecker()
    {
        yield return new WaitForEndOfFrame();
        if (GameManager.Manager.GetGameTime() >= StartDelay) StartCoroutine(SpawnWaveElement());
        else StartCoroutine(TimeChecker());
    }

    IEnumerator SpawnWaveElement()
    {
        yield return new WaitForSeconds(MyWave.delay);

        GameObject spawned = MinionPool.GetObject(MinionSpawnPosition.position);
        spawned.GetComponent<MinionController>().SetWave(MyWave);
        MyWave.SpawnedElement();

        Instantiate(SpawnParticlePrefab, MinionSpawnPosition.position, Quaternion.identity);

        if (MyWave.amount > MyWave.spawnedCounter)
            StartCoroutine(SpawnWaveElement());
    }
}
