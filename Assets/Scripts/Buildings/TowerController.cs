﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TowerController : Building, IDestroyable, IRepeirable
{
    private const int GP_COST_PER_HEALTHPOINT = 1;

    [Header("Building Specific Fields")]
    public Transform TowerHead;
    public float Range;
    public ParticleSystem ShootParticle;
    public AudioClip ShootSound;

    [Header("Outline")]
    public Material OutlineMaterial;
    public Renderer[] MeshRenderers;
    private Coroutine highlightedCoroutine;
    private bool highlighted = false;
    private GameObject repairCanvas;

    private Health enemyHealth;
    private Health myHealth;
    private bool placed = false;
    private bool canAttack = true;
    private float attackTimer = 0;
    private FogOfWarElement fogOfWarElement;

    void Awake()
    {
        myHealth = GetComponent<Health>();
        AddToBuildingsList();
        OnCreated();
        GetMaterials();
    }

    void Start()
    {
        myHealth.OnDeath += MyHealth_OnDeath;
        myHealth.OnDamaged += MyHealth_OnDamaged;
    }

    private void MyHealth_OnDamaged(float damage)
    {
        HitEffect(0.2f);
    }

    private void MyHealth_OnDeath()
    {
        Destroy();
    }

    public override void OnPlaced()
    {
        base.OnPlaced();
        GameManager.Manager.TowerBuild = true;
        placed = true;
        fogOfWarElement = FogOfWarManager.AddElement(myHealth, 1f);
    }

    void Update()
    {
        if (enemyHealth == null || !enemyHealth.Allive)
            LookForEnemy();
        else
            AttackEnemy();

        UpdateTimer();
    }

    private void UpdateTimer()
    {
        if (!canAttack)
        {
            attackTimer += Time.deltaTime;

            if (attackTimer >= Delay)
            {
                canAttack = true;
                attackTimer = 0;
            }
        }
    }

    private void LookForEnemy()
    {
        Rotate();

        foreach (Health enemyHealth in UnitManager.Manager.EnemyUnits)
        {
            if (Vector3.Distance(TowerHead.transform.position, enemyHealth.transform.position) <= Range)
            {
                this.enemyHealth = enemyHealth;
                return;
            }
        }
    }

    private void AttackEnemy()
    {
        if (!placed || Vector3.Distance(transform.position, enemyHealth.transform.position) >= Range)
            return;

        RotateToEnemy();

        if (canAttack)
        {
            if (ShootSound != null)
                AudioSource.PlayClipAtPoint(ShootSound, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

            if (ShootParticle != null)
                ShootParticle.Play();

            float damage = UnityEngine.Random.Range(AttackDamage - 2, AttackDamage + 2);
            enemyHealth.DecreaseHealth(damage);

            canAttack = false;
        }
    }

    private void RotateToEnemy()
    {
        TowerHead.LookAt(enemyHealth.transform);

        Quaternion tempRotation = TowerHead.rotation;
        tempRotation.x = 0;
        tempRotation.z = 0;

        TowerHead.rotation = tempRotation;
    }

    private void Rotate() 
    {
        TowerHead.Rotate(0f, 1f, 0f, Space.Self);
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, Description, Upgrades);
        base.OnSelected(ref selectedObjects);
    }

    public override void AddToBuildingsList()
    {
        GameManager.AddBuilding(this);
    }

    public void Destroy()
    {
        if (PlacedSound != null)
        {
            if (!PlacedSound.isPlaying)
                AudioSource.PlayClipAtPoint(PlacedSound.clip, Camera.main.transform.position);
        }

        if (GetComponent<MapProp>() != null)
            GetComponent<MapProp>().DestroyIcon();

        if (Villagar != null)
            Destroy(Villagar.gameObject);

        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

        GameManager.Manager.HidePanel();
        GameManager.Manager.BuildingMap.FreePosition(transform.position, this);
        GameManager.RemoveSelectableObject(this);
        GameManager.Buildings.Remove(this);
    }

    public GameObject GetRepairCanvas()
    {
        if (repairCanvas != null) return repairCanvas;
        else return null;
    }

    public void ShowRepeirable(Vector3 hitPoint)
    {
        if (!highlighted)
        {
            if (repairCanvas == null)
            {
                repairCanvas = RepairCanvas.Create(
                    hitPoint,
                    Image,
                    Health,
                    Name,
                    Description,
                    string.Format("Repair this by Pressing <color=#ffa500ff>{0}</color>", GameManager.Manager.GameSettings.KeyHeroRepair.ToString()),
                    (GP_COST_PER_HEALTHPOINT * Health.CurrentHealth).ToString(),
                    GP_COST_PER_HEALTHPOINT
                    );
            }

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                tempMats.Add(OutlineMaterial);
                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = true;
        }

        if (highlightedCoroutine != null) StopCoroutine(highlightedCoroutine);
        highlightedCoroutine = GameManager.Manager.StartCoroutine(ShowRepeirableTimer(gameObject));
    }

    public void DeShowRepeirable()
    {
        if (highlighted)
        {
            if (repairCanvas != null)
                Destroy(repairCanvas, 0.2f);

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length - 1; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = false;
        }
    }

    public IEnumerator ShowRepeirableTimer(GameObject gameObjectRef)
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObjectRef != null)
            DeShowRepeirable();
    }
}
