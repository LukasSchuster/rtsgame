﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WallController : Building, IDestroyable
{
    private GameObject startPillar;
    private GameObject endPillar;

    void Awake()
    {
        AddToBuildingsList();
        OnCreated();
    }

    void Start()
    {
        GameManager.AddWall(this);
        Health.OnDeath += Health_OnDeath;
    }

    private void Health_OnDeath()
    {
        Destroy();
    }

    public void SetPillars(GameObject startPillar, GameObject endPillar)
    {
        this.startPillar = startPillar;
        this.endPillar = endPillar;
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, Description, null);
        base.OnSelected(ref selectedObjects);
    }

    public override void AddToBuildingsList()
    {
        GameManager.AddBuilding(this);
    }

    public void Destroy()
    {
        if (PlacedSound != null)
        {
            if (!PlacedSound.isPlaying)
                AudioSource.PlayClipAtPoint(PlacedSound.clip, Camera.main.transform.position);
        }

        if (endPillar != null)
            Destroy(endPillar);
        if (startPillar != null)
            Destroy(startPillar);

        GameManager.RemoveWall(this);

        GameManager.Manager.HidePanel();
        GameManager.Manager.BuildingMap.FreePosition(transform.position, this);
        GameManager.RemoveSelectableObject(this);
        GameManager.Buildings.Remove(this);

        Destroy(gameObject);
    }
}
