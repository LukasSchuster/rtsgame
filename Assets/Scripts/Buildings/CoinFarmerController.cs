﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class CoinFarmerController : Building, IDestroyable, IRepeirable
{
    private const int GP_COST_PER_HEALTHPOINT = 1;

    [Header("Building Specific Fields")]
    public int Money;

    [Header("Outline")]
    public Material OutlineMaterial;
    public Renderer[] MeshRenderers;
    private Coroutine highlightedCoroutine;
    private bool highlighted = false;
    private GameObject repairCanvas;

    private Ressources ressources;
    private List<GameObject> SelectedObjetcsRef;
    private Health myHealth;
    private FogOfWarElement fogOfWarElement;

    void Awake()
    {
        myHealth = GetComponent<Health>();
        ressources = Camera.main.GetComponent<Ressources>();
        AddToBuildingsList();

        GetMaterials();
    }

    void Start()
    {
        OnCreated();
        StartCoroutine(Counter());

        Health.OnDeath += Health_OnDeath;
        Health.OnDamaged += Health_OnDamaged;
    }

    private void Health_OnDamaged(float damage)
    {
        HitEffect(0.2f);
    }

    private void Health_OnDeath()
    {
        Destroy();
    }

    IEnumerator Counter()
    {
        yield return new WaitForSeconds(Delay);
        ressources.IncreaseMoney(Money);
        StartCoroutine(Counter());
    }

    public override void OnPlaced()
    {
        base.OnPlaced();
        fogOfWarElement = FogOfWarManager.AddElement(myHealth, 1);

        SpawnVillagar(transform.position);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, Description, Upgrades);
        base.OnSelected(ref selectedObjects);
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public void Destroy()
    {
        DeShowRepeirable();

        if (PlacedSound != null)
        {
            if (!PlacedSound.isPlaying)
                AudioSource.PlayClipAtPoint(PlacedSound.clip, Camera.main.transform.position);
        }

        if (Villagar != null)
            Destroy(Villagar.gameObject);

        if (GetComponent<MapProp>() != null)
            GetComponent<MapProp>().DestroyIcon();

        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

        GameManager.Manager.HidePanel();
        GameManager.Manager.BuildingMap.FreePosition(transform.position, this);
        GameManager.RemoveSelectableObject(this);
        GameManager.Buildings.Remove(this);
    }

    public override void AddToBuildingsList()
    {
        GameManager.AddBuilding(this);
    }

    public void ShowRepeirable(Vector3 hitPoint)
    {
        if (!highlighted)
        {
            if (repairCanvas == null)
            {
                repairCanvas = RepairCanvas.Create(
                    hitPoint,
                    Image,
                    Health,
                    Name,
                    Description,
                    string.Format("Repair this by Pressing <color=#ffa500ff>{0}</color>", GameManager.Manager.GameSettings.KeyHeroRepair.ToString()),
                    (GP_COST_PER_HEALTHPOINT * Health.CurrentHealth).ToString(),
                    GP_COST_PER_HEALTHPOINT
                    );
            }

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                tempMats.Add(OutlineMaterial);
                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = true;
        }

        if (highlightedCoroutine != null) StopCoroutine(highlightedCoroutine);
        highlightedCoroutine = GameManager.Manager.StartCoroutine(ShowRepeirableTimer(gameObject));
    }

    public void DeShowRepeirable()
    {
        if (highlighted)
        {
            if (repairCanvas != null)
                Destroy(repairCanvas, 0.2f);

            for (int i = 0; i < MeshRenderers.Length; i++)
            {
                List<Material> tempMats = new List<Material>();

                for (int x = 0; x < MeshRenderers[i].materials.Length - 1; x++)
                    tempMats.Add(MeshRenderers[i].materials[x]);

                MeshRenderers[i].materials = tempMats.ToArray();
            }

            highlighted = false;
        }
    }

    public IEnumerator ShowRepeirableTimer(GameObject gameObjectRef)
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObjectRef != null)
            DeShowRepeirable();
    }

    public GameObject GetRepairCanvas()
    {
        if (repairCanvas != null) return repairCanvas;
        else return null;
    }
}
