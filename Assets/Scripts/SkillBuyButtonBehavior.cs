﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class SkillBuyButtonBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private const float LERPSPEED = 0.1f;
    private const float MAXDISTANCETOTARGET = 0.02f;

    private GameObject currentSkillsPanel;
    private GameObject skillsPanel;

    private bool bought = false;
    private bool inSkillsPanel = false;

    private Skill skill;
    private Button button;
    private Sprite image;
    private string myName, description;
    private float coolDown, godPointsCost;

    private Color deHighlightedColor;
    private bool highlighted = false;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(new UnityEngine.Events.UnityAction(SkillClicked));
    }

    public void SetValues(Skill skill, Sprite image, string name, string description, float coolDown, float godPointsCost, Color notboughtColor, GameObject skillsPanel, GameObject currentSkillsPanel)
    {
        button.image.sprite = image;

        button.image.color = notboughtColor;

        this.skill = skill;
        this.image = image;
        this.myName = name;
        this.description = description;
        this.coolDown = coolDown;
        this.godPointsCost = godPointsCost;
        this.skillsPanel = skillsPanel;
        this.currentSkillsPanel = currentSkillsPanel;
    }

    private void SkillClicked()
    {
        if (inSkillsPanel)
        {
            LerpSkillToDestination(false);
            inSkillsPanel = false;
            GameManager.Manager.GetHeroController().Skills.Remove(skill);
            return;
        }

        if (bought)
        {
            if (GameManager.Manager.GetHeroController().Skills.Count < 6)
            {
                if (!GameManager.Manager.GetHeroController().Skills.Contains(skill))
                {
                    LerpSkillToDestination(true);
                    inSkillsPanel = true;
                    GameManager.Manager.GetHeroController().Skills.Add(skill);
                }
                else
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "You Allready know this Skill", "", 1.5f);
            }
            else
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No more Space for Skills", "", 1.5f);
        }
        else
        {
            if (skill.GoldCost <= GameManager.Manager.GetRessources().Money)
            {
                GameManager.Manager.GetRessources().DecreaseMoney(skill.GoldCost);
                bought = true;
                button.image.color = Color.white;

                GameManager.Manager.GameStats.SkillsBought++;
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Bought: {0}", myName), "", 1.5f);
            }
            else
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "Not Enough Gold!", "", 1.5f);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.ShowSkillToolTip(image, myName, description, skill.Effect.ToString(), skill.GoldCost, (int)godPointsCost, coolDown);
        HighlightSkill();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.HideSkillToolTip();
        DeHightlightSkill();
    }

    private void HighlightSkill()
    {
        if (!bought && !highlighted)
        {
            deHighlightedColor = button.gameObject.GetComponent<Image>().color;

            button.gameObject.GetComponent<Image>().color = GameManager.Manager.VisibleTextColor;
            highlighted = true;
        }
    }

    private void DeHightlightSkill()
    {
        if (highlighted && !bought)
        {
            button.gameObject.GetComponent<Image>().color = deHighlightedColor;
            highlighted = false;
        }
    }

    private void LerpSkillToDestination(bool toCurrentSkills)
    {
        GameObject fakeObject = Instantiate(GameManager.Manager.GetHeroSkillsPanelController().FakeSkillImage, Vector3.zero, Quaternion.identity) as GameObject;

        if (toCurrentSkills)
            fakeObject.transform.SetParent(currentSkillsPanel.transform, false);
        else
            fakeObject.transform.SetParent(skillsPanel.transform, false);

        GameManager.Manager.StartCoroutine(LerpUpdater(fakeObject.transform, toCurrentSkills));
    }

    IEnumerator LerpUpdater(Transform fakeImage, bool toCurrentSkills)
    {
        yield return new WaitForEndOfFrame();

        transform.position = Vector3.Lerp(transform.position, fakeImage.position, LERPSPEED);

        if (Vector3.Distance(transform.position, fakeImage.position) <= MAXDISTANCETOTARGET)
        {
            Destroy(fakeImage.gameObject);

            if (toCurrentSkills)
                transform.SetParent(currentSkillsPanel.transform);
            else
                transform.SetParent(skillsPanel.transform);
        }
        else
            GameManager.Manager.StartCoroutine(LerpUpdater(fakeImage, toCurrentSkills));
    }
}
