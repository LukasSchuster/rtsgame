﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public float MaxHealth;
    public float ObjectRadius = 0.5f;

    [HideInInspector]
    public int CurrentHealth { get { return currentHealth; } }
    [HideInInspector]
    public bool Allive { get { return allive; } }

    public delegate void OnDeathDelegate();
    public event OnDeathDelegate OnDeath;

    public delegate void OnDamagedDelegate(float damage);
    public event OnDamagedDelegate OnDamaged;

    public delegate void OnHealthChangedDelegate();
    public event OnHealthChangedDelegate OnChanged;

    private int currentHealth;
    private bool allive;

    void Start()
    {
        allive = true;
        currentHealth = (int)MaxHealth;
    }

    public HealthHitInfo DecreaseHealth(float amount)
    {
        if (!allive)
            return null;

        currentHealth -= (int)amount;

        HealthHitInfo healthHitInfo = new HealthHitInfo();

        if (currentHealth <= 0)
        {
            healthHitInfo.SetIsDead(true);
            currentHealth = 0;
            allive = false;
            if (OnDeath != null)
            {
                if (gameObject.GetComponent<MapProp>() != null)
                {
                    if (GetComponent<MapProp>().MapPropGameObject != null)
                        Destroy(GetComponent<MapProp>().MapPropGameObject);
                }

                OnDeath();
            }
        }
        else
            healthHitInfo.SetIsDead(false);

        // Set HealthHitInfo Type
        Component[] components = gameObject.GetComponents(typeof(Component));

        for (int i = 0; i < components.Length; i++)
        {
            if (components[i] is Building)
            {
                healthHitInfo.SetType(HealthHitInfo.HitType.Building);
                break;
            }

            if (components[i] is EnemyMininonController)
            {
                healthHitInfo.SetType(HealthHitInfo.HitType.Minion);
                break;
            }

            // IF comppnent is other network player
        }

        if (OnDamaged != null && allive)
            OnDamaged(amount);

        if (OnChanged != null)
            OnChanged();

        return healthHitInfo;
    }

    public void IncreaseHealth(float amount)
    {
        if (!allive)
            return;

        currentHealth += (int)amount;

        if (currentHealth < MaxHealth)
            currentHealth = (int)MaxHealth;

        if (OnChanged != null)
            OnChanged();
    }

    public void Reset()
    {
        currentHealth = (int)MaxHealth;
        allive = true;
    }
}
