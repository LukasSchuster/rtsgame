﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public float Speed;
    public float Distance;

    public float MinDistanceToTarget = 0.1f;
    public float ChangedTime = 0.3f;

    private bool changed = false;
    private Vector3 target;
    private Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;

        target = ChooseTarget();
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target, Speed);

        if (Vector3.Distance(transform.position, target) <= MinDistanceToTarget && !changed)
        {
            target =  ChooseTarget();
            changed = true;
            StartCoroutine(Timer());
        }
    }

    private Vector3 ChooseTarget()
    {
        Vector3 tempVector = startPosition;

        tempVector.x += Random.Range(-Distance, Distance);
        tempVector.y += Random.Range(-Distance, Distance);
        tempVector.z += Random.Range(-Distance, Distance);

        return tempVector;
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(ChangedTime);
        changed = false;
    }
}
