﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectableObjectPanel : MonoBehaviour
{
    public Image Image;
    public Text Name;
    public Text Description;
    public Text Count;
    public GameObject UpgardesPanel;
}
