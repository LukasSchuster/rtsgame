﻿using UnityEngine;
using System.Collections;

public class Interfaces : MonoBehaviour { }

public interface IMoveable<T>
{
    void MoveTo(T position);
}

public interface IDestroyable
{
    void Destroy();
}

public interface IMapProp
{
    void AddToMapController();
    void RemoveMapProp();
}

public interface IPoolable
{
    void ResetObject();
}

public interface IHighlightable
{
    void HighLight();
    void DeHighLight();
    IEnumerator HighlightedTimer(GameObject gameObject);
}

public interface IRepeirable
{
    GameObject GetRepairCanvas();
    void ShowRepeirable(Vector3 hitPoint);
    void DeShowRepeirable();
    IEnumerator ShowRepeirableTimer(GameObject gameObject);
}