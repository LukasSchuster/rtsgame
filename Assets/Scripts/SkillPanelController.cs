﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class SkillPanelController : MonoBehaviour
{
    public int MaxSkills = 6;

    [HideInInspector]
    public Button[] skillButtons;
    [HideInInspector]
    public List<Slider> skillButtonSliders = new List<Slider>();
    private HeroController controller;

    void Awake()
    {
        skillButtons = GetComponentsInChildren<Button>();

        foreach (Transform child in transform)
        {
            if (child.GetComponentInChildren<Slider>() != null)
                skillButtonSliders.Add(child.GetComponentInChildren<Slider>());
        }
    }

    void Start()
    {
        controller = GameManager.Manager.GetHeroController();

        controller.OnHeroActivated += Controller_OnHeroActivated;
        skillButtons[0].transform.parent.parent.gameObject.SetActive(false);
    }

    public void Controller_OnHeroActivated()
    {
        for (int i = 0; i < MaxSkills; i++)
        {
            if (GameManager.Manager.GetHeroController().Skills.Count > i)
            {
                skillButtons[i].gameObject.SetActive(true);
                skillButtons[i].image.sprite = GameManager.Manager.GetHeroController().Skills[i].Image;

                if (skillButtonSliders.Count > i)
                {
                    skillButtonSliders[i].gameObject.SetActive(true);
                    skillButtonSliders[i].maxValue = GameManager.Manager.GetHeroController().Skills[i].CoolDownTime;

                }
            }
            else
            {
                if (skillButtonSliders.Count > i)
                    skillButtonSliders[i].gameObject.SetActive(true);

                skillButtons[i].gameObject.SetActive(false);
            }
        }

        foreach (Slider slider in skillButtonSliders)
            slider.gameObject.SetActive(false);
    }

    public void ResetSkills()
    {
        GameManager.Manager.GetHeroController().Skills.Clear();
    }
}
