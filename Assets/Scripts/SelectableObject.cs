﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public abstract class SelectableObject : MonoBehaviour
{
    [Header("Upgrades")]
    public List<Upgrade> Upgrades = new List<Upgrade>();
    
    [Header("Upgradeable")]
    public float Speed;
    public float AttackDamage;
    public float Delay;
    public float Amount;

    [Header("Prices")]
    public int GoldCost;
    public int WoodCost;
    public int FoodCost;

    [Header("UI")]
    public Sprite Image;
    public string Name;
    public string Description;

    [HideInInspector]
    public StrategieCameraController controller;
    [HideInInspector]
    public Projector Projector;
    [HideInInspector]
    public NavMeshAgent Agent;
    [HideInInspector]
    public Health Health;
    [HideInInspector]
    public HealthBar HealthBar;
    [HideInInspector]
    public bool selected = false;

    private List<GameObject> selectedObjectsReference;

    public virtual void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.HidePanel();
        GameManager.Manager.HideMinionUpgradePanel();

        if (HealthBar != null)
            HealthBar.Hide();

        if (selected)
        {
            if (Projector != null)
                Projector.enabled = false;
            selected = false;
        }
    }

    public virtual void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        if (HealthBar != null)
            HealthBar.Show();
        if (Projector != null)
            Projector.enabled = true;
        selected = true;
        selectedObjects.Add(this);
    }

    public void OnCreated()
    {
        Health = GetComponent<Health>();
        HealthBar = GetComponentInChildren<HealthBar>();

        if (controller == null)
            controller = Camera.main.GetComponent<StrategieCameraController>();

        if (GetComponent<Projector>() != null)
            Projector = GetComponent<Projector>();
        else
        {
            Component[] componentsInChilds = GetComponentsInChildren(typeof(Component));

            for (int i = 0; i < componentsInChilds.Length; i++)
            {
                if (componentsInChilds[i] is Projector)
                {
                    Projector = componentsInChilds[i] as Projector;
                    break;
                }
            }
        }

        GameManager.AddSelectableObject(this);

        if (Projector != null)
            Projector.enabled = false;

        if (GetComponent(typeof(NavMeshAgent)) != null)
            Agent = GetComponent<NavMeshAgent>();

        if (Agent != null)
            Agent.stoppingDistance = 2;
    }

    public virtual void CallOnSelectedRightClick() { }
}
