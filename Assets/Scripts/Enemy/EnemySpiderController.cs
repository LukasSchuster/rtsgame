﻿using UnityEngine;
using System.Collections;
using System;

public class EnemySpiderController : MonoBehaviour
{
    public const string ENEMY_BUILDING = "FriendlyUnit";

    public float ViewDistance;
    public float AttackDistance;
    public float AttackDmg;
    public float AttackDelay;
    public float MinDistanceToWayPoint = 2f;
    public float TimeToShowHealthOnDamaged = 2f;
    public WayPoint CurrentWayPoint;
    public AudioClip AttackSoundClip;

    [Header("On Death")]
    public Material BloodOverlayMaterial;
    public AudioClip DeathSound;

    [Header("On Hit")]
    public AudioPlayer HitAudioPlayer;

    private HealthBar healthbar;
    private Animation myAnimation;
    private NavMeshAgent agent;
    private Health health;
    private Health TargetHealth;
    private Wave currentWave;
    private bool attacking = false;
    private bool active = true;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
        myAnimation = GetComponent<Animation>();
        healthbar = GetComponentInChildren<HealthBar>();
    }

    void Start()
    {
        CurrentWayPoint = WayPointManager.GetClosestWayPoint(transform.position);

        agent.SetDestination(CurrentWayPoint.Position);

        health.OnDeath += Health_OnDeath;
        health.OnDamaged += Health_OnDamaged;

        UnitManager.Manager.AddEnemyUnit(health);
    }

    void Update()
    {
        if (!active)
            return;

        ControllAnimations();
        CheckforWaypointReached();

        if (TargetHealth == null || !TargetHealth.Allive)
        {
            CheckForEnemysInSight();

            if (TargetHealth == null || !TargetHealth.Allive)
                agent.SetDestination(CurrentWayPoint.Position);
        }
        else
            AttackTarget();
    }

    private void Health_OnDamaged(float damage)
    {
        DamageIndicator.Create(new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.z), damage, 1.8f, 2f, Color.red);
    }

    private void Health_OnDeath()
    {
        UnitManager.Manager.RemoveEnemyUnit(health);

        if (DeathSound != null)
            AudioSource.PlayClipAtPoint(DeathSound, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

        //if (currentWave != null)
        //    currentWave.DecrementAmount();

        myAnimation.Play("Death", PlayMode.StopAll);

        Overlay overlay = new Overlay(BloodOverlayMaterial, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), 6f, true, true);
        overlay.CreateMoving(Vector3.up, 0.005f, 3f);

        active = false;
        agent.Stop();

        StartCoroutine(DeathTimer());
    }

    private void AttackTarget()
    {
        if (Vector3.Distance(transform.position, TargetHealth.transform.position) <= AttackDistance + TargetHealth.ObjectRadius)
        {
            if (!attacking)
            {
                transform.LookAt(TargetHealth.transform.position);
                StartCoroutine(AttacKTimer());
                attacking = true;
            }
            agent.velocity = Vector3.zero;
        }
        else
        {
            attacking = false;
            agent.SetDestination(TargetHealth.transform.position);
        }
    }

    private void CheckForEnemysInSight()
    {
        float shortestDistance = 0;
        Health enemy = null;

        foreach (Health item in UnitManager.Manager.FriendlyUnits)
        {
            if (item == null || !item.Allive)
                continue;

            float currentDistance = Vector3.Distance(item.transform.position, transform.position);

            if (currentDistance >= ViewDistance)
                continue;
            else
            {
                if (shortestDistance == 0 || shortestDistance > currentDistance)
                {
                    shortestDistance = currentDistance;
                    enemy = item;
                }
            }
        }

        if (enemy != null)
            TargetHealth = enemy;
        else
        {
            for (int i = 0; i < GameManager.Buildings.Count; i++)
            {
                if (GameManager.Buildings[i] == null)
                    continue;

                if (Vector3.Distance(GameManager.Buildings[i].transform.position, transform.position) <= ViewDistance)
                {
                    if (GameManager.Buildings[i].gameObject.tag == ENEMY_BUILDING)
                    {
                        TargetHealth = GameManager.Buildings[i].GetComponent<Health>();
                        break;
                    }
                }
            }
        }
    }

    private void CheckforWaypointReached()
    {
        if (Vector3.Distance(transform.position, CurrentWayPoint.Position) <= MinDistanceToWayPoint)
        {
            if (CurrentWayPoint.PreviousWayPoint != null)
            {
                CurrentWayPoint = CurrentWayPoint.PreviousWayPoint;
                agent.SetDestination(CurrentWayPoint.Position);
            }
            else
            {
                // EDIT THIS
                ViewDistance = 70;
            }
        }
    }

    private void ControllAnimations()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
        {
            myAnimation.Play("Run", PlayMode.StopAll);
        }
        else
        {
            myAnimation.Play("Idle", PlayMode.StopAll);
        }
    }

    IEnumerator AttacKTimer()
    {
        yield return new WaitForSeconds(AttackDelay);

        if (gameObject != null)
        {
            if (attacking && health.Allive)
            {
                if (AttackSoundClip != null && TargetHealth != null)
                    AudioSource.PlayClipAtPoint(AttackSoundClip, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

                if (TargetHealth != null)
                    TargetHealth.DecreaseHealth(AttackDmg);

                myAnimation.Play("Attack", PlayMode.StopAll);

                StartCoroutine(AttacKTimer());
            }
        }
    }

    IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(3);

        if (gameObject != null)
            Destroy(gameObject);
    }
}
