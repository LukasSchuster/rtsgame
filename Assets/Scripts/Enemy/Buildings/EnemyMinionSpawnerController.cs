﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMinionSpawnerController : MonoBehaviour
{
    [Header("Fields")]
    public GameObject EnemyMinion;
    public float StartDelay;
    public float SpawnDelay;
    public float AfterWaveDelay;
    public int Amount;

    [Header("Position")]
    public Transform SpawnPoint;

    private Wave MyWave;
    private Health health;

    void Awake()
    {
        health = GetComponent<Health>();
        health.OnDeath += Health_OnDeath;
        MyWave = new Wave(EnemyMinion, SpawnPoint.position, Amount, SpawnDelay);
        MyWave.OnWaveEnd += MyWave_OnWaveEnd;
    }

    void Start()
    {
        GameManager.Manager.AddEnemyMinionController(this);
        StartCoroutine(TimeChecker());
    }

    private void Health_OnDeath()
    {
        GameManager.Manager.RemoveEnemyMinionController(this);

        if (GameManager.Manager.GetEnemyMinionControllerList().Count == 0)
            GameManager.Manager.GetEndGameController().Victory();

        Destroy(gameObject, 0.1f);
    }

    private void MyWave_OnWaveEnd()
    {
        if (gameObject == null) return;

        MyWave = new Wave(EnemyMinion, SpawnPoint.position, Amount, SpawnDelay);
        MyWave.OnWaveEnd += MyWave_OnWaveEnd;
        StartCoroutine(AfterWaveDelayTimer());
    }

    IEnumerator AfterWaveDelayTimer()
    {
        yield return new WaitForSeconds(AfterWaveDelay);
        StartCoroutine(SpawnWaveElement());
    }

    IEnumerator TimeChecker()
    {
        yield return new WaitForEndOfFrame();
        if (GameManager.Manager.GetGameTime() >= StartDelay) StartCoroutine(SpawnWaveElement());
        else StartCoroutine(TimeChecker());
    }

    IEnumerator SpawnWaveElement()
    {
        yield return new WaitForSeconds(MyWave.delay);

        GameObject spawned = Instantiate(EnemyMinion, SpawnPoint.position, Quaternion.identity) as GameObject;
        spawned.GetComponent<EnemyMininonController>().SetWave(MyWave);
        MyWave.SpawnedElement();

        if (MyWave.amount > MyWave.spawnedCounter)
            StartCoroutine(SpawnWaveElement());
    }
}
