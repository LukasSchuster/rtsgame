﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Health))]
public class EnemyMininonController : MonoBehaviour, IHighlightable
{
    public const string ENEMY_BUILDING = "FriendlyUnit";
    private const string OUTLINE_SHADER_NAME = "Outlined/Silhouette Only (UnityEngine.Shader)";

    public float ViewDistance;
    public float AttackDistance;
    public float AttackDmg;
    public float AttackDelay;
    public float MinDistanceToWayPoint = 2f;
    public float TimeToShowHealthOnDamaged = 2f;
    public WayPoint CurrentWayPoint;
    public AudioClip AttackSoundClip;

    [Header("On Death")]
    public Material BloodOverlayMaterial;
    public AudioClip DeathSound;

    [Header("On Hit")]
    public AudioPlayer hitAudioPlayer;

    private HealthBar healthbar;
    private Animator myAnimator;
    private Animation myAnimation;
    private NavMeshAgent agent;
    private Health health;
    private Health TargetHealth;
    private bool attacking = false;
    private Wave currentWave;
    private bool active = true;
    private bool highLighted = false;

    [Header("Outline")]
    public Renderer MeshRenderer;
    private Coroutine highlightedCoroutine;

    void Awake()
    {
        myAnimator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
        myAnimation = GetComponent<Animation>();
        healthbar = GetComponentInChildren<HealthBar>();
    }

    void Start()
    {
        CurrentWayPoint = WayPointManager.GetClosestWayPoint(transform.position);

        agent.SetDestination(CurrentWayPoint.Position);

        health.OnDeath += Health_OnDeath;
        health.OnDamaged += Health_OnDamaged;

        UnitManager.Manager.AddEnemyUnit(health);
    }

    private void Health_OnDamaged(float damage)
    {
        int rndIndex = UnityEngine.Random.Range(0, 1);

        hitAudioPlayer.PlayAudio();

        DamageIndicator.Create(new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.z), damage, 1.8f, 2f, Color.red);

        if (rndIndex == 1)
            myAnimator.SetTrigger("hit_right");
        else
            myAnimator.SetTrigger("hit_left");
    }

    private void Health_OnDeath()
    {
        UnitManager.Manager.RemoveEnemyUnit(health);

        DamageIndicator.Create(new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.z), "Dead", 1f, 2f, Color.red);

        if (DeathSound != null)
            AudioSource.PlayClipAtPoint(DeathSound, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

        if (currentWave != null)
            currentWave.Decrement();

        myAnimator.SetBool("die", true);

        Overlay overlay = new Overlay(BloodOverlayMaterial, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), 6f, true, true);
        overlay.CreateMoving(Vector3.up, 0.005f, 3f);

        active = false;
        agent.Stop();

        health = null;

        DeHighLight();

        StartCoroutine(DeathTimer());
    }

    void Update()
    {
        if (!active)
            return;

        ControllAnimations();
        CheckforWaypointReached();

        if (TargetHealth == null || !TargetHealth.Allive)
        {
            CheckForEnemysInSight();

            if (TargetHealth == null || !TargetHealth.Allive)
                agent.SetDestination(CurrentWayPoint.Position);
        }
        else
            AttackTarget();
    }

    public void SetWave(Wave wave)
    {
        currentWave = wave;
    }

    private void AttackTarget()
    {
        try
        {
            CheckPath();
        }
        catch (System.Exception)
        {
            print("CheckPath Failed");
        }

        if (Vector3.Distance(transform.position, TargetHealth.transform.position) <= AttackDistance + TargetHealth.ObjectRadius)
        {
            if (!attacking)
            {
                transform.LookAt(TargetHealth.transform.position);
                StartCoroutine(AttacKTimer());
                attacking = true;
            }
            agent.velocity = Vector3.zero;
        }
        else if(Vector3.Distance(transform.position, TargetHealth.transform.position) <= ViewDistance)
        {
            attacking = false;
            agent.SetDestination(TargetHealth.transform.position);
        }
        else
            TargetHealth = null;
    }

    private void CheckForEnemysInSight()
    {
        float shortestDistance = 0;
        Health enemy = null;

        foreach (Health item in UnitManager.Manager.FriendlyUnits)
        {
            if (item == null || !item.Allive)
                continue;

            float currentDistance = Vector3.Distance(item.transform.position, transform.position);

            if (currentDistance >= ViewDistance)
                continue;
            else
            {
                if (shortestDistance == 0 || shortestDistance > currentDistance)
                {
                    shortestDistance = currentDistance;
                    enemy = item;
                }
            }
        }

        if (enemy != null)
            TargetHealth = enemy;
        else
        {
            for (int i = 0; i < GameManager.Buildings.Count; i++)
            {
                if (GameManager.Buildings[i] == null || GameManager.Buildings[i] is WallController)
                    continue;

                if (Vector3.Distance(GameManager.Buildings[i].transform.position, transform.position) <= ViewDistance)
                {
                    if (GameManager.Buildings[i].gameObject.tag == ENEMY_BUILDING)
                    {
                        TargetHealth = GameManager.Buildings[i].GetComponent<Health>();
                        break;
                    }
                }
            }
        }
    }

    private void CheckPath()
    {
        NavMeshPath tempPath = new NavMeshPath();

        if (!NavMesh.CalculatePath(transform.position, TargetHealth.transform.position, NavMesh.AllAreas, tempPath))
            TargetHealth = Utilitys.GetNearestWall(transform.position).Health;
    }

    private void CheckforWaypointReached()
    {
        if (Vector3.Distance(transform.position, CurrentWayPoint.Position) <= MinDistanceToWayPoint)
        {
            if (CurrentWayPoint.PreviousWayPoint != null)
            {
                CurrentWayPoint = CurrentWayPoint.PreviousWayPoint;
                agent.SetDestination(CurrentWayPoint.Position);
            }
            else
            {
                // EDIT THIS
                ViewDistance = 70;
            }
        }
    }

    private void ControllAnimations()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
        {
            myAnimator.SetBool("walk", true);
            myAnimator.SetBool("idle", false);
        }
        else
        {
            myAnimator.SetBool("idle", true);
            myAnimator.SetBool("walk", false);
        }
    }

    IEnumerator AttacKTimer()
    {
        yield return new WaitForSeconds(AttackDelay);

        if (gameObject != null && health != null)
        {
            if (attacking && health.Allive)
            {
                if (AttackSoundClip != null && TargetHealth != null)
                    AudioSource.PlayClipAtPoint(AttackSoundClip, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

                if (TargetHealth != null)
                    TargetHealth.DecreaseHealth(AttackDmg);

                int animIndex = UnityEngine.Random.Range(0, 2);

                switch (animIndex)
                {
                    case 0:
                        myAnimator.SetTrigger("attack_1");
                        break;
                    case 1:
                        myAnimator.SetTrigger("attack_2");
                        break;
                    case 2:
                        myAnimator.SetTrigger("attack_3");
                        break;
                }

                StartCoroutine(AttacKTimer());
            }
        }
    }

    IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(3);

        if (gameObject != null)
            Destroy(gameObject);
    }

    public void HighLight()
    {
        if (!highLighted && active)
        {
            highLighted = true;

            List<Material> tempMatList = new List<Material>();

            for (int i = 0; i < MeshRenderer.materials.Length; i++)
                tempMatList.Add(MeshRenderer.materials[i]);

            tempMatList.Add(GameManager.Manager.OutlineMaterial);

            MeshRenderer.materials = tempMatList.ToArray();
        }

        if (highlightedCoroutine != null) StopCoroutine(highlightedCoroutine);

        highlightedCoroutine = GameManager.Manager.StartCoroutine(HighlightedTimer(gameObject));
    }

    public void DeHighLight()
    {
        if (highLighted)
        {
            highLighted = false;

            List<Material> tempMatList = new List<Material>();

            for (int i = 0; i < MeshRenderer.materials.Length; i++)
            {
                if (MeshRenderer.materials[i].shader.ToString() != OUTLINE_SHADER_NAME)
                    tempMatList.Add(MeshRenderer.materials[i]);
            }

            MeshRenderer.materials = tempMatList.ToArray();
        }
    }

    public IEnumerator HighlightedTimer(GameObject gameObjectRef)
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObjectRef != null)
            DeHighLight();
    }
}
