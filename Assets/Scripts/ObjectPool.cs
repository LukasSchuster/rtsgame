﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectPool <T> where T : IPoolable
{
    public GameObject[] Objetcs;

    private LinkedList<GameObject> activeObjects = new LinkedList<GameObject>();
    private LinkedList<GameObject> inavticeObjects = new LinkedList<GameObject>();

    private int amount;

    public ObjectPool(GameObject instance, int amount)
    {
        this.amount = amount;
        this.Objetcs = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            GameObject tempObject = Object.Instantiate(instance, new Vector3(10, 0, 10), Quaternion.identity) as GameObject;

            Objetcs[i] = tempObject;
            inavticeObjects.AddFirst(tempObject);
            (inavticeObjects.First.Value as GameObject).SetActive(false);
        }
    }

    public GameObject GetObject()
    {
        if (amount == -1) return null;

        GameObject toReturn = inavticeObjects.First.Value;
        inavticeObjects.RemoveFirst();

        if (toReturn.GetComponent<T>() != null)
            (toReturn.GetComponent<T>() as IPoolable).ResetObject();

        if (toReturn is GameObject) (toReturn as GameObject).SetActive(true);
        else Debug.Log(toReturn + ": is not a GameObject!");

        return toReturn;
    }

    public GameObject GetObject(Vector3 position)
    {
        if (amount == -1) return null;

        GameObject toReturn = inavticeObjects.First.Value;
        inavticeObjects.RemoveFirst();

        if (toReturn.GetComponent<T>() != null)
            (toReturn.GetComponent<T>() as IPoolable).ResetObject();

        toReturn.transform.position = position;

        if (toReturn is GameObject) (toReturn as GameObject).SetActive(true);
        else Debug.Log(toReturn + ": is not a GameObject!");

        return toReturn;
    }

    public void StoreObject(GameObject objectToStore)
    {
        if (amount == -1) return;

        inavticeObjects.AddLast(objectToStore);
        activeObjects.Remove(objectToStore);

        if (objectToStore is GameObject) (objectToStore as GameObject).SetActive(false);
        else Debug.Log(objectToStore + ": is not a GameObject!");
    }

    public void RemovePool()
    {
        amount = -1;
        activeObjects.Clear();
        inavticeObjects.Clear();
    }
}
