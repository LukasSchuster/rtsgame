﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    private const float LERP_SPEED = 1.5f;
    private const float MIN_HEALTH_VALUE_DIST = 0.05f;

    private const string OUTLINE_NAME = "HealthBarImageOutline";
    private const string INNER_NAME = "HealthBarImage";
    private const string TEXT_NAME = "HealthText";

    public float TimeToShowHealthOnDamaged = 3f;

    private Image healthBarOutline;
    private Image healthBarInner;
    private Text healthBarText;
    private Health health;

    private float healthBarX;
    private float healthbarY;
    private float healthBarZ;

    private bool destroyed;

    private Coroutine currentCoroutine;
    private Coroutine healthLerperCoroutine;

    void Awake()
    {
        health = GetComponentInParent<Health>();
        healthBarOutline = transform.Find(OUTLINE_NAME).GetComponent<Image>();
        healthBarInner = transform.Find(INNER_NAME).GetComponent<Image>();
        healthBarText = transform.Find(TEXT_NAME).GetComponent<Text>();
    }

    void Start()
    {
        destroyed = false;

        healthBarX = transform.localScale.x;
        healthbarY = transform.localScale.y;
        healthBarZ = transform.localScale.z;

        health.OnDamaged += Health_OnDamaged;
        health.OnChanged += Health_OnChanged;
        health.OnDeath += Health_OnDeath;

        healthBarOutline.enabled = false;
        healthBarInner.enabled = false;
        healthBarText.enabled = false;
    }

    private void Health_OnChanged()
    {
        healthBarOutline.enabled = true;
        healthBarInner.enabled = true;
        healthBarText.enabled = true;
        UpdateHealthBar();

        healthBarText.text = string.Format("{0} / {1}", health.CurrentHealth, health.MaxHealth);

        if (currentCoroutine != null)
            StopCoroutine(currentCoroutine);

        currentCoroutine = GameManager.Manager.StartCoroutine(HealthBarTimer(healthBarOutline, healthBarInner, healthBarText));
    }

    private void Health_OnDeath()
    {
        healthBarInner.fillAmount = 0;
        UpdateHealthBar();
        destroyed = true;
        healthBarOutline.enabled = false;
        healthBarInner.enabled = false;
        healthBarText.enabled = false;
    }

    private void Health_OnDamaged(float damage)
    {
        healthBarOutline.enabled = true;
        healthBarInner.enabled = true;
        healthBarText.enabled = true;
        UpdateHealthBar();

        healthBarText.text = string.Format("{0} / {1}", health.MaxHealth, health.CurrentHealth);

        if (currentCoroutine != null)
            StopCoroutine(currentCoroutine);

        currentCoroutine = GameManager.Manager.StartCoroutine(HealthBarTimer(healthBarOutline, healthBarInner, healthBarText));
    }

    public void Show()
    {
        if (!destroyed)
        {
            healthBarOutline.enabled = true;
            healthBarInner.enabled = true;
            healthBarText.enabled = true;
        }
        else
        {
            healthBarOutline.enabled = false;
            healthBarInner.enabled = false;
            healthBarText.enabled = false;
        }
    }

    public void Hide()
    {
        healthBarOutline.enabled = false;
        healthBarInner.enabled = false;
        healthBarText.enabled = false;
    }

    void Update()
    {
        transform.LookAt(Camera.main.transform);

        if ((health.CurrentHealth / health.MaxHealth) != healthBarInner.fillAmount)
        {

        }
    }

    private void UpdateHealthBar()
    {
        if (health != null && !destroyed)
        {
            if (healthLerperCoroutine != null)
                StopCoroutine(healthLerperCoroutine);

            healthLerperCoroutine = GameManager.Manager.StartCoroutine(HealthLerper(healthBarInner.transform.parent.gameObject));
        }
    }

    IEnumerator HealthLerper(GameObject healthbarCanvas)
    {
        yield return new WaitForFixedUpdate();

        if (healthbarCanvas != null)
        {
            float wantedHealhValue = (health.CurrentHealth / health.MaxHealth);
            healthBarInner.fillAmount = Mathf.Lerp(healthBarInner.fillAmount, wantedHealhValue, LERP_SPEED * Time.deltaTime);

            if (Mathf.Abs(wantedHealhValue - healthBarInner.fillAmount) <= MIN_HEALTH_VALUE_DIST)
                healthBarInner.fillAmount = wantedHealhValue;
            else
                healthLerperCoroutine = GameManager.Manager.StartCoroutine(HealthLerper(healthBarInner.transform.parent.gameObject));
        }
    }

    IEnumerator HealthBarTimer(Image healthBarOutline, Image healthBarInner, Text healthBarText)
    {
        yield return new WaitForSeconds(TimeToShowHealthOnDamaged);

        if (healthBarOutline != null) healthBarOutline.enabled = false;
        if (healthBarInner != null) healthBarInner.enabled = false;
        if (healthBarText != null) healthBarText.enabled = false;
    }
}
