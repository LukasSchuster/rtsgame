﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Upgrade : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [Header("Owner")]
    public SelectableObject Owner;

    [Header("UI")]
    public Sprite Sprite;
    public string Name;
    public string Description;

    [Header("Prices")]
    public int GoldCost;
    public int WoodCost;
    public int FoodCost;

    [Header("Effect")]
    public float AttackDmgMultiplyer;
    public float SpeedMultiplyer;
    public float CostMultiplyer;
    public float DelayMultiplier;
    public float AmountMultiplier;

    private Ressources ressources;
    private SelectableObject ObjectToEffect;

    void Awake()
    {
        ressources = Camera.main.GetComponent<Ressources>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.ShowToolTip(Sprite, Name, Description, GoldCost, WoodCost, FoodCost);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.HideToolTip();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameManager.Manager.UIPressedOnce();
        GameManager.Manager.PlaySound(GameManager.SoundFile.Upgrade);

        if (ressources.Money >= GoldCost && ressources.Wood >= WoodCost && ressources.Food >= FoodCost)
        {
            ressources.DecreaseWood(WoodCost);
            ressources.DecreaseMoney(GoldCost);
            ressources.DecreaseFood(FoodCost);

            GameManager.Manager.UpgradeBought();

            ObjectToEffect = GameManager.SelectedObjects[0];

            if (AttackDmgMultiplyer != 0)
                ObjectToEffect.AttackDamage *= AttackDmgMultiplyer;

            if (SpeedMultiplyer != 0)
                ObjectToEffect.Speed *= SpeedMultiplyer;

            if (DelayMultiplier != 0)
                ObjectToEffect.Delay *= DelayMultiplier;

            if (AmountMultiplier != 0)
                ObjectToEffect.Amount *= AmountMultiplier;

            Upgrade upgradeToRemove = null;

            foreach (Upgrade upgrade in ObjectToEffect.Upgrades)
            {
                if (upgrade.Name == Name)
                    upgradeToRemove = upgrade;
            }

            if (upgradeToRemove != null)
                ObjectToEffect.Upgrades.Remove(upgradeToRemove);
            else
                throw new SystemException("ObjectToRemove is Null");

            Destroy(gameObject);
            GameManager.Manager.HideToolTip();
        }
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "Not Enough Ressources", "", 1.5f);
            GameManager.Manager.HideToolTip();
        }
    }
}
