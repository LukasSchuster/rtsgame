﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class MinionUpgradeController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private const string SOLD_PANEL_NAME = "SoldPanel";

    private const float HEALTH_MULTIPLIAR = 1.3f;
    private const float ATTACK_SPD_MULTIPLIAR = 0.8f;
    private const float ATTACK_DMG_MULTIPLIAR = 1.4f;
    private const float MOVE_SPD_MULTIPLIAR = 1.3f;
    private const float BIGSHIELD_HEALTH_MULTIPLIAR = 1.5f;
    private const float SHARPSWORD_MULTIPLIAR = 1.6f;

    private const int ATTACK_DMG_COST = 450;
    private const int ATTACK_SPD_COST = 550;
    private const int HEALTH_COST = 800;
    private const int MOVE_SPD_COST = 850;
    private const int BIG_SHIELD_COST = 1000;
    private const int SHARP_SWORD_COST = 1000; 

    public static List<MinionController> Minions = new List<MinionController>();

    public Sprite MinionIcon;

    public Button AttackDMGButton;
    public Button AttackSPDButton;
    public Button MoveSpdButton;
    public Button HealthButton;
    public Button SharpSwordButton;
    public Button BigShieldButton;

    public Text StatsText;

    private bool attackDmgBought = false;
    private bool attackSpdBought = false;
    private bool moveSpdBought = false;
    private bool healthBought = false;
    private bool sharpSwordBought = false;
    private bool bigShieldBought = false;

    void Start()
    {
        AttackDMGButton.onClick.AddListener(delegate { OnAttackDMGButtonClicked(); });
        AttackSPDButton.onClick.AddListener(delegate { OnAttackSPDButtonClicked(); });
        MoveSpdButton.onClick.AddListener(delegate { OnMoveSpdButtonClicked(); });
        HealthButton.onClick.AddListener(delegate { OnHealthButtonClicked(); });
        SharpSwordButton.onClick.AddListener(delegate { OnSharpSwordButtonClicked(); });
        BigShieldButton.onClick.AddListener(delegate { OnBigShieldButtonClicked(); });

        UpdateStatsText();
    }

    public void ShowToolTipFor(string name)
    {
        switch (name)
        {
            case "AttackDMGButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "Attack Damage", "The Minions Hit there targtes harder!", 0, 0, ATTACK_DMG_COST);
                break;

            case "AttackSPDButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "Attack Speed", "Your Minions can Attack faster!", 0, 0, ATTACK_SPD_COST);
                break;

            case "MoveSpdButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "Move Speed", "The Minions can move faster!", 0, 0, MOVE_SPD_COST);
                break;

            case "HealthButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "More health", "Your Minions spawn with more health!", 0, 0, HEALTH_COST);
                break;

            case "SharpSwordButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "Sharp Swords", "The Minions deal more Damage with a sharper Swords!", 0, 0, SHARP_SWORD_COST);
                break;

            case "BigShieldButton":
                GameManager.Manager.ShowToolTip(MinionIcon, "Big Shields", "Your Minions can take more Damage with bigger Shields!", 0, 0, BIG_SHIELD_COST);
                break;
        }
    }

    #region Button Events

    private void OnBigShieldButtonClicked()
    {
        if (bigShieldBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= BIG_SHIELD_COST)
            GameManager.Manager.GetRessources().DecreaseFood(BIG_SHIELD_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].GetComponent<Health>().MaxHealth *= BIGSHIELD_HEALTH_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        bigShieldBought = true;
        SetUpgradeBoughtPanelActive(BigShieldButton, true);

        UpdateStatsText();
    }

    private void OnSharpSwordButtonClicked()
    {
        if (sharpSwordBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= SHARP_SWORD_COST)
            GameManager.Manager.GetRessources().DecreaseFood(SHARP_SWORD_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].AttackDmg *= SHARPSWORD_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        sharpSwordBought = true;
        SetUpgradeBoughtPanelActive(SharpSwordButton, true);

        UpdateStatsText();
    }

    private void OnHealthButtonClicked()
    {
        if (healthBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= HEALTH_COST)
            GameManager.Manager.GetRessources().DecreaseFood(HEALTH_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].GetComponent<Health>().MaxHealth *= HEALTH_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        healthBought = true;
        SetUpgradeBoughtPanelActive(HealthButton, true);

        UpdateStatsText();
    }

    private void OnMoveSpdButtonClicked()
    {
        if (moveSpdBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= MOVE_SPD_COST)
            GameManager.Manager.GetRessources().DecreaseFood(MOVE_SPD_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].GetComponent<NavMeshAgent>().speed *= MOVE_SPD_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        moveSpdBought = true;
        SetUpgradeBoughtPanelActive(MoveSpdButton, true);

        UpdateStatsText();
    }

    private void OnAttackSPDButtonClicked()
    {
        if (attackSpdBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= ATTACK_SPD_COST)
            GameManager.Manager.GetRessources().DecreaseFood(ATTACK_SPD_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].AttackDelay *= ATTACK_SPD_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        attackSpdBought = true;
        SetUpgradeBoughtPanelActive(AttackSPDButton, true);

        UpdateStatsText();
    }

    private void OnAttackDMGButtonClicked()
    {
        if (attackDmgBought)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Sold Out!", "You allready bought this Upgrade,", 2f);
            return;
        }

        if (GameManager.Manager.GetRessources().Food >= ATTACK_DMG_COST)
            GameManager.Manager.GetRessources().DecreaseFood(ATTACK_DMG_COST);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Food!", "You dont have enough Food to buy this.", 1.5f);
            return;
        }

        for (int i = 0; i < Minions.Count; i++)
            Minions[i].AttackDmg *= ATTACK_DMG_MULTIPLIAR;

        GameManager.Manager.HideToolTip();
        attackDmgBought = true;
        SetUpgradeBoughtPanelActive(AttackDMGButton, true);

        UpdateStatsText();
    }

    #endregion

    private void UpdateStatsText()
    {
        if (Minions[0] == null) return;

        StatsText.text = string.Format("Damage: {0} {1} Health: {2} {3} Attack Delay: {4} {5} Move Speed: {6}",
            Minions[0].AttackDmg,
            Environment.NewLine,
            Minions[0].gameObject.GetComponent<Health>().MaxHealth,
            Environment.NewLine,
            Minions[0].AttackDelay,
            Environment.NewLine,
            Minions[0].gameObject.GetComponent<NavMeshAgent>().speed
            );
       
    }

    private void SetUpgradeBoughtPanelActive(Button button ,bool value)
    {
        button.transform.Find(SOLD_PANEL_NAME).gameObject.SetActive(value);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.UIDePressed();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.UIPressed();
    }
}
