﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Ressources))]
public class StrategieCameraController : MonoBehaviour
{
    private const int MAX_BUILD_DISTANCE = 120;
    private const float MAINPANELPERCENTSCREEN = 24.4f;
    private const int MOUSECLICKDISTANCETOLLERANCE = 20;
    private const int MENUPANELHEIGHT = 65;
    private const int FLOORLAYERINT = 8;
    private const int CAMERAMOVEMANTFACTORKEY = 500;
    private const int CAMERAMOVEMANTFACTORMOUSE = 70;
    private const float MINDISTANCETOFLOOR = 3f;

    private const float CAMERAJUMP_Z_DIST = 20;

    [Header("LayMasks")]
    public LayerMask CameraFloorMask;

    [Header("Building Mode")]
    public Color BuildingPlacedColor;
    public Color CantPlaceBuildingColor;
    public Color CanPlaceBuildingColor;

    public enum CameraMode{ SelectMode = 1, BuildMode = 2, BuildWallMode = 3, HeroMode = 4, SkillMode = 5}
    public CameraMode CamMode;

    public int MoveAreaInPercent;
    public float Speed;
    public float ZoomSpeed;
    public float RotationSpeed;

    [HideInInspector]
    public GameObject BuildingToPlace;
    [HideInInspector]
    public bool FreezeMovement = false;
    [HideInInspector]
    public bool FreezeSelection = false;
    [HideInInspector]
    public Ressources ressources;
    [HideInInspector]
    public Vector3 CamPosition;
    [HideInInspector]
    public Quaternion CamRotation;
    [HideInInspector]
    public CameraMode lastMode;
    [HideInInspector]
    public bool HeroModeJustChanged = false;

    private List<GameObject> selectedObjects = new List<GameObject>();

    #region Mouse Input Rect

    public Color RectFillColor;
    public Color RectBorderColor;

    private Vector2 mouseClickedPos;
    private bool mouseClickHeld = false;

    #endregion

    #region Wall

    [Header("Wall")]
    public GameObject WallPillarPrefab;
    public GameObject WallPrefab;

    public int WallPricePerUnit;
    public float MaxWallLenght = 3;

    private GameObject wallStart;
    private GameObject wallEnd;
    private GameObject wall;
    private GameObject wallPreview;
    private Renderer wallRenderer;

    private float wallDistance = 0;

    private bool creatingWall;
    private bool startWallPlaced;
    private bool firstFrame = true;

    #endregion

    void Awake()
    {
        ressources = GetComponent<Ressources>();
    }

    void Update()
    {
        switch (CamMode)
        {
            case CameraMode.SelectMode:
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroPause))
                {
                    if (GameManager.Manager.GetBuildingCanvasController().BuildPanelShowing)
                        GameManager.Manager.GetBuildingCanvasController().HideCanvas();
                    else
                    {
                        if (GameManager.Manager.IsGamePaused())
                            GameManager.Manager.ResumeGame();
                        else
                            GameManager.Manager.PauseGame();
                    }
                }

                if (GameManager.Manager.IsGamePaused())
                    return;

                KeyInput();
                InputRectange();
                Zoom();
                MoveCameraPrecise();
                GetInputSelected();

                HeroModeJustChanged = false;
                break;

            case CameraMode.BuildMode:
                Cursor.visible = true;
                GetInputBuildMode();
                MoveCameraPrecise();
                break;

            case CameraMode.BuildWallMode:
                Cursor.visible = true;
                Zoom();
                GetInputBuildWallMode();
                MoveCameraPrecise();
                break;

            case CameraMode.HeroMode:
                break;

            case CameraMode.SkillMode:
                break;
        }
    }

    void OnGUI()
    {
        if (mouseClickHeld && mouseClickedPos.y >= Screen.height / 100 * MAINPANELPERCENTSCREEN && CamMode == CameraMode.SelectMode && !GameManager.Manager.CheckIfUIPressedThisFrame())
        {
            Rect rect = Utilitys.GetRectScreen(mouseClickedPos, Input.mousePosition);

            Utilitys.DrawRect(rect, RectFillColor);
            Utilitys.DrawRectBorder(rect, 2, RectBorderColor);
        }
    }

    public void ChangeModeTo(string mode)
    {
        if (mode == "HeroMode")
        {
            if (!GameManager.Manager.GetHeroController().health.Allive)
            {
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Hero is not Allive!", "It will respawn soon...", 2f);
                return;
            }
        }

        GameManager.Manager.HidePanel();
        GameManager.Manager.HideToolTip();
        GameManager.Manager.HideUnitCreatePanel();
        GameManager.Manager.HideUpgradePanel();
        GameManager.Manager.HideBuildingPanel();
        GameManager.Manager.HideHint();
        GameManager.Manager.UIDePressed();

        lastMode = CamMode;

        if (lastMode.ToString() == mode)
            return;

        switch (mode)
        {
            case "SelectMode":
                mouseClickHeld = false;
                CamMode = CameraMode.SelectMode;
                break;
            case "BuildMode":
                CamMode = CameraMode.BuildMode;
                break;
            case "BuildWallMode":
                firstFrame = true;
                CamMode = CameraMode.BuildWallMode;
                break;
            case "HeroMode":
                GameManager.Manager.GetHeroController().ActivateHeroMode();
                CamPosition = transform.position;
                CamRotation = transform.rotation;
                CamMode = CameraMode.HeroMode;
                break;
            case "SkillMode":
                GameManager.Manager.ActivateHeroSkillsPanel(true);
                GameManager.Manager.GetHeroController().SkillsJustChanged();
                break;
        }
    }

    private void SelectObjectsInRect()
    {
        if (FreezeSelection || GameManager.Manager.IsGamePaused() || Input.mousePosition.y <= MENUPANELHEIGHT)
            return;

        for (int i = 0; i < GameManager.SelectedObjects.Count; i++)
            GameManager.SelectedObjects[i].OnDeSelected(ref GameManager.SelectedObjects);

        GameManager.Manager.HideUpgradePanel();
        GameManager.SelectedObjects.Clear();

        RaycastHit hit1;
        RaycastHit hit2;

        Physics.Raycast(Camera.main.ScreenPointToRay(mouseClickedPos), out hit1);
        Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit2);

        float minX = Mathf.Min(hit1.point.x, hit2.point.x);
        float minY = Mathf.Min(hit1.point.z, hit2.point.z);
        float maxX = Mathf.Max(hit1.point.x, hit2.point.x);
        float maxY = Mathf.Max(hit1.point.z, hit2.point.z);

        Rect rect = Rect.MinMaxRect(minX, minY, maxX, maxY);

        foreach (SelectableObject selectableObject in GameManager.SelectableObjectsLinked)
        {
            if (selectableObject == null)
                continue;

            if (rect.Contains(new Vector2(selectableObject.transform.position.x, selectableObject.transform.position.z)))
                selectableObject.OnSelected(ref GameManager.SelectedObjects);
        }

        if (Utilitys.CheckSelectionForMultipleTypes(GameManager.SelectedObjects))
        {
            for (int i = 0; i < GameManager.SelectedObjects.Count; i++)
                GameManager.SelectedObjects[i].OnDeSelected(ref GameManager.SelectedObjects);

            // Damit ich trotzdem die liste clearen kann um sie anschließend wieder neu zu füllen
            var tempList = Utilitys.GetThePriorSelectableObjectFromList(GameManager.SelectedObjects);
            GameManager.SelectedObjects.Clear();

            for (int i = 0; i < tempList.Count; i++)
                tempList[i].OnSelected(ref GameManager.SelectedObjects);
        }

        if (GameManager.SelectedObjects.Count == 1)
        {
            if (Utilitys.CheckForOnlyOneSelectedType())
                GameManager.Manager.ShowUpgradePanel();
            else
                GameManager.Manager.HideUpgradePanel();
        }
    }

    private void SelectSingleObject()
    {
        if (FreezeSelection || GameManager.Manager.IsGamePaused() || Input.mousePosition.y <= MENUPANELHEIGHT)
            return;

        for (int i = 0; i < GameManager.SelectedObjects.Count; i++)
            GameManager.SelectedObjects[i].OnDeSelected(ref GameManager.SelectedObjects);

        GameManager.Manager.HideUpgradePanel();
        GameManager.SelectedObjects.Clear();

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.parent != null)
            {
                Component[] components = hit.transform.parent.GetComponents(typeof(Component));
                for (int i = 0; i < components.Length; i++)
                    if (components[i] is SelectableObject) (components[i] as SelectableObject).OnSelected(ref GameManager.SelectedObjects);
            }
            else
            {
                Component[] components = hit.transform.GetComponents(typeof(Component));
                for (int i = 0; i < components.Length; i++)
                    if (components[i] is SelectableObject) (components[i] as SelectableObject).OnSelected(ref GameManager.SelectedObjects);
            }
        }

        if (GameManager.SelectedObjects.Count == 1)
        {
            if (Utilitys.CheckForOnlyOneSelectedType())
                GameManager.Manager.ShowUpgradePanel();
            else
                GameManager.Manager.HideUpgradePanel();
        }
    }

    private void InputRectange()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseClickHeld = true;
            mouseClickedPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0) && mouseClickHeld)
        {
            if (GameManager.Manager.CheckIfUIPressedThisFrame())
            {
                mouseClickHeld = false;
                return;
            }

            if (Vector2.Distance(mouseClickedPos, Input.mousePosition) <= MOUSECLICKDISTANCETOLLERANCE)
            {
                SelectSingleObject();
                mouseClickHeld = false;
            }
            else
            {
                SelectObjectsInRect();
                mouseClickHeld = false;
            }
        }
    }

    private void GetInputBuildMode()
    {
        if (FreezeSelection || GameManager.Manager.TextInputMode)
            return;

        if (Input.GetKey(KeyCode.Escape))
        {
            mouseClickHeld = false;
            CamMode = CameraMode.SelectMode;
            Building tempBuilding = BuildingToPlace.GetComponent<Building>();

            ressources.IncreaseFood(tempBuilding.FoodCost);
            ressources.IncreaseMoney(tempBuilding.GoldCost);
            ressources.IncreaseWood(tempBuilding.WoodCost);

            Destroy(BuildingToPlace);
        }

        mouseClickedPos = Input.mousePosition;

        if (BuildingToPlace != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit[] hits = Physics.RaycastAll(ray);

            foreach (var hit in hits)
            {
                if (hit.transform.gameObject.layer == FLOORLAYERINT)
                {
                    var tempPos = GameManager.Manager.BuildingMap.GivePositionNear(hit.point);
                    BuildingToPlace.transform.position = tempPos;

                    if (Input.GetAxis("Mouse ScrollWheel") > 0)
                        Utilitys.RotateGameObjectByDeg(BuildingToPlace, 22.5f);
                    else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                        Utilitys.RotateGameObjectByDeg(BuildingToPlace, -22.5f);

                    if (GameManager.Manager.BuildingMap.CheckPositionNear(hit.point) && Vector3.Distance(Utilitys.GiveMousePositionInWorldOnFloor(), Vector3.zero) <= MAX_BUILD_DISTANCE)
                    {
                        Utilitys.ChangeColorOfGameObjectAndChilds(BuildingToPlace, CanPlaceBuildingColor);

                        if (Input.GetMouseButton(0))
                        {
                            Utilitys.ChangeColorOfGameObjectAndChilds(BuildingToPlace, BuildingPlacedColor);
                            GameManager.Manager.BuildingMap.UsePosition(hit.point, BuildingToPlace.GetComponent<Building>());
                            CamMode = CameraMode.SelectMode;

                            Building tempBuilding = BuildingToPlace.GetComponent<Building>();

                            if (!PauseMenuManager.ShadowOn)
                            {
                                foreach (Transform child in tempBuilding.transform)
                                {
                                    if (child.gameObject.GetComponent<MeshRenderer>() != null)
                                        child.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                                }
                            }

                            if (tempBuilding.PlacedParticleEffectGameObject != null)
                                Instantiate(tempBuilding.PlacedParticleEffectGameObject, BuildingToPlace.transform.position, tempBuilding.PlacedParticleEffectGameObject.transform.rotation);

                            if (tempBuilding.PlacedSound != null && !tempBuilding.PlacedSound.isPlaying)
                                tempBuilding.PlacedSound.Play();

                            tempBuilding.OnPlaced();
                        }
                    }
                    else
                    {
                        Utilitys.ChangeColorOfGameObjectAndChilds(BuildingToPlace, CantPlaceBuildingColor);

                        if (Input.GetMouseButton(0))
                            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Space!", "You cant build here, another Building is in the Way", 1f);
                    }

                    return;
                }
            }
        }
    }

    #region WallMode

    private void GetInputBuildWallMode()
    {
        if (firstFrame)
        {
            firstFrame = false;
            wallPreview = Instantiate(WallPillarPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Utilitys.ChangeColorOfGameObjectAndChilds(wallPreview, CanPlaceBuildingColor);
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (GameManager.Manager.BuildingMap.CheckPositionNear(Utilitys.GiveMousePositionInWorldOnFloor()))
                StartWallPlacement();
            else
            {
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "No Space!", "There is no Space for this Wall", 1.5f);
                ChangeModeTo("SelectMode");
                if (wallPreview != null)
                    Destroy(wallPreview);
                return;
            }
        }
        else if(Input.GetMouseButtonUp(0))
            EndWallPlaceMent(wallDistance);
        else
        {
            if (creatingWall)
                UpdateWallPosition();
            else
                wallPreview.transform.position = GameManager.Manager.BuildingMap.GivePositionNear(Utilitys.GiveMousePositionInWorldOnFloor());
        }
    }

    private void StartWallPlacement()
    {
        Destroy(wallPreview);
        creatingWall = true;
        wallStart = Instantiate(WallPillarPrefab, Utilitys.GiveMousePositionInWorldOnFloor(), Quaternion.identity) as GameObject;
        wallEnd = Instantiate(WallPillarPrefab, Utilitys.GiveMousePositionInWorldOnFloor(), Quaternion.identity) as GameObject;
        wall = Instantiate(WallPrefab, Utilitys.GiveMousePositionInWorldOnFloor(), Quaternion.identity) as GameObject;
        wall.GetComponent<WallController>().SetPillars(wallStart, wallEnd);
        wallRenderer = wall.GetComponentInChildren<Renderer>();
    }

    private void EndWallPlaceMent(float distance)
    {
        if (CanBuildWall(distance))
        {
            Vector3 wallStartPos = wallStart.transform.position;
            Vector3 wallEndPos = wallEnd.transform.position;

            Vector3 wallCurrentPos = wallStartPos;

            int counter = 0;

            while (Vector3.Distance(wallCurrentPos, wallEndPos) > MaxWallLenght && counter <= 200)
            {
                Vector3 tempDirection = (wallCurrentPos - wallEndPos).normalized;
                Vector3 tempNextPos = wallCurrentPos + (-tempDirection * MaxWallLenght);

                GameObject tempWallStart = Instantiate(WallPillarPrefab, wallStartPos, Quaternion.identity) as GameObject;
                GameObject tempWallEnd = Instantiate(WallPillarPrefab, tempNextPos, Quaternion.identity) as GameObject;

                tempWallStart.transform.LookAt(tempWallEnd.transform.position);
                tempWallEnd.transform.LookAt(tempWallStart.transform.position);

                GameObject tempWall = Instantiate(WallPrefab, wallCurrentPos + MaxWallLenght / 2 * wallStart.transform.forward, Quaternion.identity) as GameObject;
                Transform child = tempWall.transform.Find("WallPart");

                if (child != null)
                    child.localScale = new Vector3(child.localScale.x, child.localScale.y, MaxWallLenght);

                tempWall.transform.LookAt(tempWallStart.transform.position);

                tempWall.GetComponent<WallController>().SetPillars(tempWallStart, tempWallEnd);

                GameManager.Manager.BuildingMap.UsePosition(tempWall.transform.position, tempWall.GetComponent<Building>());

                counter++;
                wallCurrentPos = tempNextPos;
            }

            GameObject tempWallEndFinal = Instantiate(WallPillarPrefab, wallEndPos, Quaternion.identity) as GameObject;
            GameObject tempWallFinal = Instantiate(WallPrefab, wallCurrentPos + Vector3.Distance(wallCurrentPos, wallEndPos) / 2 * wallStart.transform.forward, Quaternion.identity) as GameObject;
            tempWallFinal.transform.localScale = new Vector3(tempWallFinal.transform.localScale.x, tempWallFinal.transform.localScale.y, Vector3.Distance(wallCurrentPos, wallEndPos) * 2);
            tempWallFinal.transform.LookAt(tempWallEndFinal.transform.position);
            tempWallFinal.GetComponent<WallController>().SetPillars(null, tempWallEndFinal);

            Destroy(wall);
            Destroy(wallStart);
            Destroy(wallEnd);

            ressources.DecreaseMoney((int)(WallPricePerUnit * wallDistance));
            creatingWall = false;
            ChangeModeTo("SelectMode");
        }
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "Not Enough Ressources!", "You dont have enough Ressources for this Wall", 1.5f);
            Destroy(wall);
            Destroy(wallEnd);
            Destroy(wallStart);

            creatingWall = false;
            ChangeModeTo("SelectMode");
        }
    }

    private void UpdateWallPosition()
    {
        var distance = Vector3.Distance(wallStart.transform.position, wallEnd.transform.position);
        wallDistance = distance;

        if (Utilitys.GiveMousePositionInWorld() != Vector3.zero)
        {
            wallEnd.transform.position = Utilitys.GiveMousePositionInWorldOnFloor();
            wallStart.transform.LookAt(wallEnd.transform.position);
            wallEnd.transform.LookAt(wallStart.transform.position);

            wall.transform.position = wallStart.transform.position + distance / 2 * wallStart.transform.forward;
            wall.transform.LookAt(wallStart.transform.position);
            wall.transform.localScale = new Vector3(wall.transform.localScale.x, wall.transform.localScale.y, distance * 2);
            wallRenderer.material.mainTextureScale = new Vector2(wall.transform.localScale.z / 3, wall.transform.localScale.x);

            if (CanBuildWall(distance))
            {
                Utilitys.ChangeColorOfGameObjectAndChilds(wall, CanPlaceBuildingColor);
                Utilitys.ChangeColorOfGameObjectAndChilds(wallEnd, CanPlaceBuildingColor);
                Utilitys.ChangeColorOfGameObjectAndChilds(wallStart, CanPlaceBuildingColor);
            }
            else
            {
                Utilitys.ChangeColorOfGameObjectAndChilds(wall, CantPlaceBuildingColor);
                Utilitys.ChangeColorOfGameObjectAndChilds(wallEnd, CantPlaceBuildingColor);
                Utilitys.ChangeColorOfGameObjectAndChilds(wallStart, CantPlaceBuildingColor);
            }
        }
    }

    private bool CanBuildWall(float distance)
    {
        if (wallStart == null || wallEnd == null)
            return false;

        Ray ray = new Ray(new Vector3(wallStart.transform.position.x, wallStart.transform.position.y + wallStart.transform.localScale.y / 2, wallStart.transform.position.z), new Vector3(wallEnd.transform.position.x, wallEnd.transform.position.y + wallEnd.transform.localScale.y / 2, wallEnd.transform.position.z));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.layer == 9)
                return false;
        }

        if (ressources.Money >= WallPricePerUnit * distance)
            return true;

        return false;
    }

    #endregion

    private void GetInputSelected()
    {
        if (Input.GetMouseButtonDown(1) && GameManager.SelectedObjects.Count > 0)
        {
            foreach (SelectableObject item in GameManager.SelectedObjects)
                item.CallOnSelectedRightClick();
            return;
        }
        else if (Input.GetMouseButton(1) && GameManager.SelectedObjects.Count <= 0)
        {
            float dirY = Input.GetAxis("Mouse Y");
            transform.Rotate(-dirY * RotationSpeed, 0, 0, Space.World);

            Quaternion tempRotation = transform.rotation;
            tempRotation.x = Mathf.Clamp(tempRotation.x, 0.2f, 0.6f);
            transform.rotation = tempRotation;
        }
    }

    private void Zoom()
    {
        if (FreezeMovement)
            return;

        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.up);

        if (Physics.Raycast(ray, out hit, CameraFloorMask))
        {
            if (Vector3.Distance(hit.point, transform.position) <= MINDISTANCETOFLOOR)
                transform.Translate(Vector3.up * 0.2f, Space.World);
            else
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    var temp = transform.position;
                    temp.y -= ZoomSpeed;
                    temp.y = Mathf.Clamp(temp.y, 2, 30);
                    transform.position = temp;
                    return;
                }
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            var temp = transform.position;
            temp.y += ZoomSpeed;
            temp.y = Mathf.Clamp(temp.y, 2, 30);
            transform.position = temp;
            return;
        }
    }

    private void MoveCameraPrecise()
    {
        if (Input.GetMouseButton(1) || FreezeMovement || GameManager.Manager.TextInputMode)
            return;

        if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveForward) || Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveBackwards) || Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveLeft) || Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveRight))
        {
            float x = 0;
            float z = 0;

            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveLeft))
                x -= Speed * CAMERAMOVEMANTFACTORKEY * Time.deltaTime;
            else if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveRight))
                x += Speed * CAMERAMOVEMANTFACTORKEY * Time.deltaTime;

            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveForward))
                z += Speed * CAMERAMOVEMANTFACTORKEY * Time.deltaTime;
            else if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveBackwards))
                z -= Speed * CAMERAMOVEMANTFACTORKEY * Time.deltaTime;

            Vector3 direction = new Vector3(x, 0, z);
            transform.position += direction;

            Vector3 tempPosition = transform.position;

            if (transform.position.x < 0)
                tempPosition.x = 0;
            else if (tempPosition.x > GameManager.Manager.GetTerrain().terrainData.size.x)
                tempPosition.x = GameManager.Manager.GetTerrain().terrainData.size.x;

            if (transform.position.z < 0)
                tempPosition.z = 0;
            else if (tempPosition.z > GameManager.Manager.GetTerrain().terrainData.size.z)
                tempPosition.z = GameManager.Manager.GetTerrain().terrainData.size.z;

            if (tempPosition != transform.position)
                transform.position = tempPosition;

            return;
        }

        if (Input.mousePosition.x >= Screen.width - 10 || Input.mousePosition.x <= 10 || Input.mousePosition.y >= Screen.height -10 || Input.mousePosition.y <= 10)
        {

            float distance = Vector2.Distance(Input.mousePosition, new Vector2(Screen.width / 2, Screen.height / 2));
            float speed = Speed / 200 * distance;

            Vector3 direction = (new Vector3(Screen.width / 2, Screen.height / 2, 0) - Input.mousePosition).normalized;

            Vector3 tempPos = new Vector3();
            tempPos.x = transform.position.x + (direction.x * -speed * Time.deltaTime * CAMERAMOVEMANTFACTORMOUSE);
            tempPos.y = transform.position.y;
            tempPos.z = transform.position.z + (direction.y * -speed * Time.deltaTime * CAMERAMOVEMANTFACTORMOUSE);

            tempPos.x = Mathf.Clamp(tempPos.x, 0, GameManager.Manager.GetTerrain().terrainData.size.x);
            tempPos.z = Mathf.Clamp(tempPos.z, 0, GameManager.Manager.GetTerrain().terrainData.size.z);

            transform.position = tempPos;
        }
    }

    private void KeyInput()
    {
        if (GameManager.Manager.TextInputMode)
            return;

        //Change Mode to HeroMode
        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeySwitchMode) && !HeroModeJustChanged)
            ChangeModeTo("HeroMode");

        //Jump To selected Building or Unit
        if (GameManager.SelectedObjects.Count > 0)
        {
            if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyBuildJumpToSelection))
            {
                for (int i = 0; i < GameManager.SelectedObjects.Count; i++)
                {
                    if (GameManager.SelectedObjects[i] is Unit || GameManager.SelectedObjects[i] is Building)
                        JumpToSelection(GameManager.SelectedObjects[0].gameObject);
                }
            }
        }

        //Remove Selected Building/s or Unit/s if it is Removeable
        if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildRemove) && GameManager.SelectedObjects.Count != 0)
        {
            foreach (var item in GameManager.SelectedObjects)
            {
                Component[] components = item.GetComponents(typeof(Component));
                foreach (var component in components)
                {
                    if (component is IDestroyable)
                    {
                        (component as IDestroyable).Destroy();
                        break;
                    }
                }
            }
        }

        //Change Mode to SkillMode
        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyBuildSkills))
        {
            GameManager.Manager.ActivateHeroSkillsPanel(GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf ? false : true);

            if (GameManager.Manager.GetHeroSkillsPanelController().transform.parent.gameObject.activeSelf)
            {
                Time.timeScale = 0;
                ChangeModeTo("SkillMode");
            }
            else
            {
                Time.timeScale = 1;
                GameManager.Manager.GetHeroSkillsPanelController().OnBackButtonClicked();
            }
        }
    }

    private void JumpToSelection(GameObject selected)
    {
        Camera.main.transform.position = new Vector3(selected.transform.position.x, Camera.main.transform.position.y, selected.transform.position.z - CAMERAJUMP_Z_DIST);
        Camera.main.transform.LookAt(selected.transform);
    }
}
