﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Building : SelectableObject
{
    [Header("Cost to Heal")]
    public int Gp_Cost_per_Health_Point = 1;

    [Header("Building Fields")]
    public int SizeRadius;
    public float TimeToShowHealthBarOnAttacked = 1.5f;

    public GameObject PlacedParticleEffectGameObject;
    public AudioSource PlacedSound;

    [HideInInspector]
    public GameObject BuildingPanel;
    [HideInInspector]
    public GameObject Villagar;

    private List<Material> myMaterials = new List<Material>();
    private List<Color> myMaterialColors = new List<Color>();

    public void GetMaterials()
    {
        if (GetComponent<Renderer>() != null)
        {
            myMaterials.Add(GetComponent<Renderer>().material);
            myMaterialColors.Add(GetComponent<Renderer>().material.color);
        }

        foreach (Transform child in transform)
        {
            if (child.GetComponent<Renderer>() != null)
            {
                myMaterials.Add(child.GetComponent<Renderer>().material);
                if (child.GetComponent<Renderer>().material.color != null)
                    myMaterialColors.Add(child.GetComponent<Renderer>().material.color);
            }
        }
    }

    public void SpawnVillagar(Vector3 pos)
    {
        Villagar = Instantiate(GameManager.Manager.GetVillagar(), pos, Quaternion.identity) as GameObject;
    }

    public abstract void AddToBuildingsList();

    public virtual void OnPlaced() { }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnSelected(ref selectedObjects);
    }

    public void HitEffect(float duration)
    {
        for (int i = 0; i < myMaterials.Count; i++)
            myMaterials[i].color = GameManager.Manager.BuildingHitColor;

        GameManager.Manager.StartCoroutine(HitEffectTimer(duration));
    }

    public bool RepairBuilding()
    {
        int cost = (int)Gp_Cost_per_Health_Point * (int)(this.Health.MaxHealth - (int)this.Health.CurrentHealth);

        if (GameManager.Manager.GetRessources().GodPoints >= cost)
        {
            this.Health.IncreaseHealth(this.Health.MaxHealth - this.Health.CurrentHealth);
            GameManager.Manager.GetRessources().DecreaseGodPoints(cost);

            Instantiate(GameManager.Manager.GetBuildingRepairParticle(), transform.position, GameManager.Manager.GetBuildingRepairParticle().transform.rotation);

            return true;
        }
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Mana!", "You dont have enough Mana to repair this Building", 2f);
            return false;
        }
    }

    IEnumerator HitEffectTimer(float duration)
    {
        yield return new WaitForSeconds(duration);

        for (int i = 0; i < myMaterials.Count; i++)
            if (myMaterialColors.Count >= i && myMaterials.Count >= i)
                myMaterials[i].color = myMaterialColors[i];
    }
}
