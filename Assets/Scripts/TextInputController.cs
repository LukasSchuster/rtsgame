﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TextInputController : MonoBehaviour
{
    private const float FADEOUTTIME = 2f;

    public GameObject SubmitedTextPanel;
    public GameObject SubmitTextPrefab;
    public Text inputFieldPlaceholder;

    private Queue<GameObject> Texts = new Queue<GameObject>();

    private InputField inputField;
    private Image inputFieldImage;

    private Coroutine fadeOutCoroutine;

    private bool active = false;

    void Awake()
    {
        inputField = GetComponent<InputField>();
        inputFieldImage = GetComponent<Image>();
    }

    void Start()
    {
        inputField.onEndEdit.AddListener(delegate { OnInputFieldSubmitted(); });

        inputField.enabled = false;
        inputFieldImage.enabled = false;
        inputFieldPlaceholder.enabled = false;

        SubmitedTextPanel.SetActive(false);
    }

    void Update()
    {
        if (Time.timeScale == 0)
            return;

        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroChatWindow))
        {
            if (active)
            {
                GameManager.Manager.SetTextInputMode(false);

                if (fadeOutCoroutine != null)
                    StopCoroutine(fadeOutCoroutine);
                fadeOutCoroutine = StartCoroutine(FadeOutTimer());

                inputField.enabled = false;
                inputFieldImage.enabled = false;
                inputFieldPlaceholder.enabled = false;

                active = false;
            }
            else
            {
                GameManager.Manager.SetTextInputMode(true);

                if (fadeOutCoroutine != null)
                    StopCoroutine(fadeOutCoroutine);

                inputField.enabled = true;
                inputFieldImage.enabled = true;
                inputFieldPlaceholder.enabled = true;
                SubmitedTextPanel.SetActive(true);

                inputField.Select();
                inputField.ActivateInputField();

                active = true;
            }
        }
    }

    private void OnInputFieldSubmitted()
    {
        if (inputField.text != string.Empty)
        {
            GameObject newTextGameObject = Instantiate(SubmitTextPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            newTextGameObject.transform.SetParent(SubmitedTextPanel.transform, false);
            CheckForCheats();

            newTextGameObject.GetComponent<Text>().text = string.Format("{0}   {1}: {2}", GameManager.Manager.GameTime.text ,"Player", inputField.text);

            Texts.Enqueue(newTextGameObject);

            if (Texts.Count >= 6)
            {
                GameObject toDestroy = Texts.Peek();
                Texts.Dequeue();
                Destroy(toDestroy);
            }
        }

        inputField.text = string.Empty;
    }

    private void CheckForCheats()
    {
        switch (inputField.text)
        {
            case "ress":
                GameManager.Manager.GetRessources().IncreaseFood(10000);
                GameManager.Manager.GetRessources().IncreaseMoney(10000);
                GameManager.Manager.GetRessources().IncreaseWood(10000);
                GameManager.Manager.GetRessources().IncreaseGodPoints(GameManager.Manager.GetRessources().MaxGodPoints);
                inputField.text = "Ressources Cheat Used!";
                break;
        }
    }

    IEnumerator FadeOutTimer()
    {
        yield return new WaitForSeconds(FADEOUTTIME);

        SubmitedTextPanel.SetActive(false);
    }
}
