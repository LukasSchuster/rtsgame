﻿using UnityEngine;
using System.Collections.Generic;

public class BuildingMap : MonoBehaviour
{
    private const int MapSpaceing = 1;
    private List<Vector2> UsedPositions = new List<Vector2>();

    public bool CheckPositionNear(Vector3 pos)
    {
        var tempPosToCheck = GivePositionNear(pos);

        if (UsedPositions.Contains(new Vector2(tempPosToCheck.x, tempPosToCheck.z)))
            return false;
        return true;
    }

    public void FreePosition(Vector3 pos, Building building)
    {
        var vectorToUse = GivePositionNear(pos);

        for (int x = (int)vectorToUse.x - building.SizeRadius; x <= (int)vectorToUse.x + building.SizeRadius; x++)
        {
            for (int z = (int)vectorToUse.z - building.SizeRadius; z <= (int)vectorToUse.z + building.SizeRadius; z++)
                UsedPositions.Remove(new Vector2(x, z));
        }
    }

    public void FreePosition(Vector3 pos, int size)
    {
        var vectorToUse = GivePositionNear(pos);

        for (int x = (int)vectorToUse.x - size; x <= (int)vectorToUse.x + size; x++)
        {
            for (int z = (int)vectorToUse.z - size; z <= (int)vectorToUse.z + size; z++)
                UsedPositions.Remove(new Vector2(x, z));
        }
    }

    public void UsePosition(Vector3 pos, Building building)
    {
        var vectorToUse = GivePositionNear(pos);

        for (int x = (int)vectorToUse.x - building.SizeRadius; x <= (int)vectorToUse.x + building.SizeRadius; x++)
        {
            for (int z = (int)vectorToUse.z - building.SizeRadius; z <= (int)vectorToUse.z + building.SizeRadius; z++)
                UsedPositions.Add(new Vector2(x, z));
        }
    }

    public void UsePosition(Vector3 pos, int size)
    {
        var vectorToUse = GivePositionNear(pos);

        for (int x = (int)vectorToUse.x - size; x <= (int)vectorToUse.x + size; x++)
        {
            for (int z = (int)vectorToUse.z - size; z <= (int)vectorToUse.z + size; z++)
                UsedPositions.Add(new Vector2(x, z));
        }
    }

    public Vector3 GivePositionNear(Vector3 pos)
    {
        float xSpace = pos.x / MapSpaceing;
        int x = (int)xSpace * MapSpaceing;

        float zSpace = pos.z / MapSpaceing;
        int z = (int)zSpace * MapSpaceing;

        return new Vector3(x, pos.y, z);
    }
}
