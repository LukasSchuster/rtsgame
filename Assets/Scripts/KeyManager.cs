﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class KeyManager : MonoBehaviour
{
    #region Save Key Names

    private const string SAVE_BTN_HERO_MOVE_FORWARD = "HeroKeyMoveForward";
    private const string SAVE_BTN_HERO_MOVE_BACKWARDS = "HeroKeyMoveBackward";
    private const string SAVE_BTN_HERO_MOVE_LEFT = "HeroKeyMoveLeft";
    private const string SAVE_BTN_HERO_MOVE_RIGHT = "HeroKeyMoveRight";
    private const string SAVE_BTN_HERO_JUMP = "HeroKeyJump";
    private const string SAVE_BTN_HERO_SWITCHMODE = "HeroKeySwitchMode";
    private const string SAVE_BTN_HERO_REPAIR = "HeroKeyRepair";
    private const string SAVE_BTN_HERO_PAUSE = "HeroKeyPause";
    private const string SAVE_BTN_HERO_SKILLS = "HeroKeySkills";
    private const string SAVE_BTN_HERO_HIDEHUD = "HeroKeyHideHud";
    private const string SAVE_BTN_HERO_CHAT = "HeroKeyChat";
    private const string SAVE_BTN_HERO_SPRINT= "HeroKeySprint";

    private const string SAVE_BTN_BUILD_BUILDMENU = "BuildKeyBuildMenu";
    private const string SAVE_BTN_BUILD_REMOVE = "BuildKeyRemove";
    private const string SAVE_BTN_BUILD_SKILLS = "BuildKeySkills";
    private const string SAVE_BTN_BUILD_MOVE_FORWARD = "BuildKeyMoveForward";
    private const string SAVE_BTN_BUILD_MOVE_BACKWARDS = "BuildKeyMoveBackward";
    private const string SAVE_BTN_BUILD_MOVE_LEFT = "BuildKeyMoveLeft";
    private const string SAVE_BTN_BUILD_MOVE_RIGHT = "BuildKeyMoveRight";
    private const string SAVE_BTN_BUILD_MOVECAMTOSELECTION = "BuildKeyJumpTo";

    #endregion

    private const string EVENT_KEY_GAMEOBJECT_NAME = "Text";
    private const string BUTTONKEYTEXT = "Text";
    private const string BUTTONDESCRIPTIONTEXT = "toControll";

    [Header("Buttons Hero")]
    public Button BtnHeroMoveForward;
    public Button BtnHeroMoveBackwards;
    public Button BtnHeroMoveLeft;
    public Button BtnHeroMoveRight;
    public Button BtnHeroJump;
    public Button BtnHeroSwitchMode;
    public Button BtnHeroRepair;
    public Button BtnHeroPause;
    public Button BtnHeroSkills;
    public Button BtnHeroHideHud;
    public Button BtnHeroChat;
    public Button BtnHeroSprint;

    [Header("Buttons Build")]
    public Button BtnBuildBuildMenu;
    public Button BtnBuildRemove;
    public Button BtnBuildSkills;
    public Button BtnBuildMoveForward;
    public Button BtnBuildMoveBackwards;
    public Button BtnBuildMoveLeft;
    public Button BtnBuildMoveRight;
    public Button BtnBuildMoveCamToSelection;

    private List<Button> keyButtons = new List<Button>();

    private Button currentButton;
    private Text currentText;
    private KeyCode currentKeyCode;
    private bool canEditButton = true;

    void Start()
    {
        BtnHeroMoveForward.onClick.AddListener(delegate { OnBtnHeroMoveForwardClicked(); });
        BtnHeroMoveBackwards.onClick.AddListener(delegate { OnBtnHeroMoveBackwardsClicked(); });
        BtnHeroMoveLeft.onClick.AddListener(delegate { OnBtnHeroMoveLeftClicked(); });
        BtnHeroMoveRight.onClick.AddListener(delegate { OnBtnHeroMoveRightClicked(); });
        BtnHeroJump.onClick.AddListener(delegate { OnBtnHeroJumpClicked(); });
        BtnHeroSwitchMode.onClick.AddListener(delegate { OnBtnHeroSwitchModeClicked(); });
        BtnHeroRepair.onClick.AddListener(delegate { OnBtnHeroRepairClicked(); });
        BtnHeroPause.onClick.AddListener(delegate { OnBtnHeroPauseClicked(); });
        BtnHeroSkills.onClick.AddListener(delegate { OnBtnHeroSkillsClicked(); });
        BtnHeroHideHud.onClick.AddListener(delegate { OnBtnHeroHideHudClicked(); });
        BtnHeroChat.onClick.AddListener(delegate { OnBtnHeroChatClicked(); });
        BtnHeroSprint.onClick.AddListener(delegate { OnBtnHeroSprintClicked(); });

        BtnBuildBuildMenu.onClick.AddListener(delegate { onBtnBuildBuildMenuClicked(); });
        BtnBuildRemove.onClick.AddListener(delegate { OnBtnBuildRemoveClicked(); });
        BtnBuildSkills.onClick.AddListener(delegate { OnBtnBuildSkillsClicked(); });
        BtnBuildMoveForward.onClick.AddListener(delegate { OnBtnBuildMoveForwardClicked(); });
        BtnBuildMoveBackwards.onClick.AddListener(delegate { OnBtnBuildMoveBackwardsClicked(); });
        BtnBuildMoveLeft.onClick.AddListener(delegate { OnBtnBuildMoveLeftClicked(); });
        BtnBuildMoveRight.onClick.AddListener(delegate { OnBtnBuildMoveRightClicked(); });
        BtnBuildMoveCamToSelection.onClick.AddListener(delegate { OnBtnBuildMoveCamToSelectionClicked(); });

        SetPrefsToKeys();

        SetButtonTextOnStart(BtnHeroMoveForward);
        SetButtonTextOnStart(BtnHeroMoveBackwards);
        SetButtonTextOnStart(BtnHeroMoveLeft);
        SetButtonTextOnStart(BtnHeroMoveRight);
        SetButtonTextOnStart(BtnHeroJump);
        SetButtonTextOnStart(BtnHeroSwitchMode);
        SetButtonTextOnStart(BtnHeroRepair);
        SetButtonTextOnStart(BtnHeroPause);
        SetButtonTextOnStart(BtnHeroSkills);
        SetButtonTextOnStart(BtnHeroHideHud);
        SetButtonTextOnStart(BtnHeroChat);
        SetButtonTextOnStart(BtnHeroSprint);

        SetButtonTextOnStart(BtnBuildBuildMenu);
        SetButtonTextOnStart(BtnBuildRemove);
        SetButtonTextOnStart(BtnBuildSkills);
        SetButtonTextOnStart(BtnBuildMoveForward);
        SetButtonTextOnStart(BtnBuildMoveBackwards);
        SetButtonTextOnStart(BtnBuildMoveLeft);
        SetButtonTextOnStart(BtnBuildMoveRight);
        SetButtonTextOnStart(BtnBuildMoveCamToSelection);

        #region add Buttons to list Region

        keyButtons.Add(BtnHeroMoveForward);
        keyButtons.Add(BtnHeroMoveBackwards);
        keyButtons.Add(BtnHeroMoveLeft);
        keyButtons.Add(BtnHeroMoveRight);
        keyButtons.Add(BtnHeroJump);
        keyButtons.Add(BtnHeroSwitchMode);
        keyButtons.Add(BtnHeroRepair);
        keyButtons.Add(BtnHeroPause);
        keyButtons.Add(BtnHeroSkills);
        keyButtons.Add(BtnHeroHideHud);
        keyButtons.Add(BtnHeroChat);
        keyButtons.Add(BtnHeroSprint);

        keyButtons.Add(BtnBuildBuildMenu);
        keyButtons.Add(BtnBuildRemove);
        keyButtons.Add(BtnBuildSkills);
        keyButtons.Add(BtnBuildMoveForward);
        keyButtons.Add(BtnBuildMoveBackwards);
        keyButtons.Add(BtnBuildMoveLeft);
        keyButtons.Add(BtnBuildMoveRight);
        keyButtons.Add(BtnBuildMoveCamToSelection);

        #endregion
    }

    void OnGUI()
    {
        Event e = Event.current;

        if (e.isKey)
            if (e.keyCode != KeyCode.None && !canEditButton)
            {
                currentKeyCode = e.keyCode;

                if (IsKeyAllreadyUsed(currentKeyCode.ToString()))
                {
                    GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "The Key is allready in use!", "Try to define another key.", 1.5f);
                    return;
                }

                switch (currentButton.GetComponent<StringTag>().Tag)
                {
                    case "KeyHeroMoveForward":
                        GameManager.Manager.GameSettings.KeyHeroMoveForward = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_MOVE_FORWARD, currentKeyCode);
                        break;

                    case "KeyHeroMoveBackwards":
                        GameManager.Manager.GameSettings.KeyHeroMoveBackwards = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_MOVE_BACKWARDS, currentKeyCode);
                        break;

                    case "KeyHeroMoveLeft":
                        GameManager.Manager.GameSettings.KeyHeroMoveLeft = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_MOVE_LEFT, currentKeyCode);
                        break;

                    case "KeyHeroMoveRight":
                        GameManager.Manager.GameSettings.KeyHeroMoveRight = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_MOVE_RIGHT, currentKeyCode);
                        break;

                    case "KeyHeroJump":
                        GameManager.Manager.GameSettings.KeyHeroJump = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_JUMP, currentKeyCode);
                        break;

                    case "KeySwitchMode":
                        GameManager.Manager.GameSettings.KeySwitchMode = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_SWITCHMODE, currentKeyCode);
                        break;

                    case "KeyHeroRepair":
                        GameManager.Manager.GameSettings.KeyHeroRepair = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_REPAIR, currentKeyCode);
                        break;

                    case "KeyHeroPause":
                        GameManager.Manager.GameSettings.KeyHeroPause = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_PAUSE, currentKeyCode);
                        break;

                    case "KeyHeroSkills":
                        GameManager.Manager.GameSettings.KeyHeroSkills = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_SKILLS, currentKeyCode);
                        break;

                    case "BtnHeroHideHud":
                        GameManager.Manager.GameSettings.KeyHeroHideHud = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_HIDEHUD, currentKeyCode);
                        break;

                    case "BtnHeroChat":
                        GameManager.Manager.GameSettings.KeyHeroChatWindow = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_CHAT, currentKeyCode);
                        break;

                    case "BtnHeroSprint":
                        GameManager.Manager.GameSettings.KeyHeroSprint = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_HERO_SPRINT, currentKeyCode);
                        break;

                    //Seperator--------

                    case "KeyBuildBuildMenu":
                        GameManager.Manager.GameSettings.KeyBuildMenu = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_BUILDMENU, currentKeyCode);
                        break;

                    case "KeyBuildRemove":
                        GameManager.Manager.GameSettings.KeyBuildRemove = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_REMOVE, currentKeyCode);
                        break;

                    case "KeyBuildSkills":
                        GameManager.Manager.GameSettings.KeyBuildSkills = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_SKILLS, currentKeyCode);
                        break;

                    case "KeyBuildMoveForward":
                        GameManager.Manager.GameSettings.KeyBuildMoveForward = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_MOVE_FORWARD, currentKeyCode);
                        break;

                    case "KeyBuildMoveBackwards":
                        GameManager.Manager.GameSettings.KeyBuildMoveBackwards = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_MOVE_BACKWARDS, currentKeyCode);
                        break;

                    case "KeyBuildMoveLeft":
                        GameManager.Manager.GameSettings.KeyBuildMoveLeft = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_MOVE_LEFT, currentKeyCode);
                        break;

                    case "KeyBuildMoveRight":
                        GameManager.Manager.GameSettings.KeyBuildMoveRight = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_MOVE_RIGHT, currentKeyCode);
                        break;

                    case "BtnBuildMoveCamToSelection":
                        GameManager.Manager.GameSettings.KeyBuildJumpToSelection = currentKeyCode;
                        currentText.text = currentKeyCode.ToString();
                        SaveKeyToPrefs(SAVE_BTN_BUILD_MOVECAMTOSELECTION, currentKeyCode);
                        break;
                }
                canEditButton = true;
            }
    }

    private void SetButtonTextOnStart(Button button)
    {
        Text toEdit = null;

        foreach (Transform child in button.transform)
            if (child.GetComponent<Text>() != null && child.name == BUTTONKEYTEXT)
                toEdit = child.GetComponent<Text>();

        if (toEdit != null)
        {
            switch (button.GetComponent<StringTag>().Tag)
            {
                case "KeyHeroMoveForward":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroMoveForward.ToString();
                    break;

                case "KeyHeroMoveBackwards":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroMoveBackwards.ToString();
                    break;

                case "KeyHeroMoveLeft":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroMoveLeft.ToString();
                    break;

                case "KeyHeroMoveRight":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroMoveRight.ToString();
                    break;

                case "KeyHeroJump":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroJump.ToString();
                    break;

                case "KeySwitchMode":
                    toEdit.text = GameManager.Manager.GameSettings.KeySwitchMode.ToString();
                    break;

                case "KeyHeroRepair":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroRepair.ToString();
                    break;

                case "KeyHeroPause":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroPause.ToString();
                    break;

                case "KeyHeroSkills":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroSkills.ToString();
                    break;

                case "BtnHeroHideHud":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroHideHud.ToString();
                    break;

                case "BtnHeroChat":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroChatWindow.ToString();
                    break;

                case "BtnHeroSprint":
                    toEdit.text = GameManager.Manager.GameSettings.KeyHeroSprint.ToString();
                    break;

                //Seperator -----------------

                case "KeyBuildBuildMenu":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildMenu.ToString();
                    break;

                case "KeyBuildRemove":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildRemove.ToString();
                    break;

                case "KeyBuildSkills":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildSkills.ToString();
                    break;

                case "KeyBuildMoveForward":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildMoveForward.ToString();
                    break;

                case "KeyBuildMoveBackwards":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildMoveBackwards.ToString();
                    break;

                case "KeyBuildMoveLeft":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildMoveLeft.ToString();
                    break;

                case "KeyBuildMoveRight":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildMoveRight.ToString();
                    break;

                case "BtnBuildMoveCamToSelection":
                    toEdit.text = GameManager.Manager.GameSettings.KeyBuildJumpToSelection.ToString();
                    break;
            }
        }
        else
            Debug.Log("toEdit is NULL");
    }

    private bool IsKeyAllreadyUsed(string key)
    {
        for (int i = 0; i < keyButtons.Count; i++)
        {
            GameObject eventKeyGameObject = keyButtons[i].transform.Find(EVENT_KEY_GAMEOBJECT_NAME).gameObject;
            Text eventKeyText = eventKeyGameObject.GetComponent<Text>();

            if (eventKeyText.text.ToUpper() == key.ToUpper())
                return true;
        }
        return false;
    }

    private void SetNewKey(Button button)
    {
        if (!canEditButton)
            return;

        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Press a Button...", "", 1f);

        foreach (Transform child in button.transform)
        {
            if (child.GetComponent<Text>() != null && child.name == BUTTONKEYTEXT)
            {
                child.GetComponent<Text>().text = "...";
                currentText = child.GetComponent<Text>();
                currentButton = button;
                canEditButton = false;
            }
        }
    }

    private void SaveKeyToPrefs(string keyName, KeyCode key)
    {
        PlayerPrefs.SetString(keyName, key.ToString());
        PlayerPrefs.Save();
    }

    private void SetPrefsToKeys()
    {
        #region Hero

        string tempHeroMoveForward = PlayerPrefs.GetString(SAVE_BTN_HERO_MOVE_FORWARD);
        if (tempHeroMoveForward != null && tempHeroMoveForward != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroMoveForward = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroMoveForward);

        string tempHeroMoveBackward = PlayerPrefs.GetString(SAVE_BTN_HERO_MOVE_BACKWARDS);
        if (tempHeroMoveBackward != null && tempHeroMoveBackward != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroMoveBackwards = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroMoveBackward);

        string tempHeroMoveLeft = PlayerPrefs.GetString(SAVE_BTN_HERO_MOVE_LEFT);
        if (tempHeroMoveLeft != null && tempHeroMoveLeft != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroMoveLeft = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroMoveLeft);

        string tempHeroMoveRight = PlayerPrefs.GetString(SAVE_BTN_HERO_MOVE_RIGHT);
        if (tempHeroMoveRight != null && tempHeroMoveRight != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroMoveRight = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroMoveRight);

        string tempHeroJump = PlayerPrefs.GetString(SAVE_BTN_HERO_JUMP);
        if (tempHeroJump != null && tempHeroJump != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroJump = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroJump);

        string tempHeroPause = PlayerPrefs.GetString(SAVE_BTN_HERO_PAUSE);
        if (tempHeroPause != null && tempHeroPause != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroPause = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroPause);

        string tempHeroRepair = PlayerPrefs.GetString(SAVE_BTN_HERO_REPAIR);
        if (tempHeroRepair != null && tempHeroRepair != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroRepair = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroRepair);

        string tempHeroSkills = PlayerPrefs.GetString(SAVE_BTN_HERO_SKILLS);
        if (tempHeroSkills != null && tempHeroSkills != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroSkills = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroSkills);

        string tempHeroSprint = PlayerPrefs.GetString(SAVE_BTN_HERO_SPRINT);
        if (tempHeroSprint != null && tempHeroSprint != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroSprint = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroSprint);

        string tempHeroSwitch = PlayerPrefs.GetString(SAVE_BTN_HERO_SWITCHMODE);
        if (tempHeroSwitch != null && tempHeroSwitch != string.Empty)
            GameManager.Manager.GameSettings.KeySwitchMode = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroSwitch);

        string tempHeroChat = PlayerPrefs.GetString(SAVE_BTN_HERO_CHAT);
        if (tempHeroChat != null && tempHeroChat != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroChatWindow = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroChat);

        string tempHeroHideHud = PlayerPrefs.GetString(SAVE_BTN_HERO_HIDEHUD);
        if (tempHeroHideHud != null && tempHeroHideHud != string.Empty)
            GameManager.Manager.GameSettings.KeyHeroHideHud = (KeyCode)Enum.Parse(typeof(KeyCode), tempHeroHideHud);

        #endregion

        #region Build

        string tempBuildBuildMenu = PlayerPrefs.GetString(SAVE_BTN_BUILD_BUILDMENU);
        if (tempBuildBuildMenu != null && tempBuildBuildMenu != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildMenu = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildBuildMenu);

        string tempBuildMoveForward = PlayerPrefs.GetString(SAVE_BTN_BUILD_MOVE_FORWARD);
        if (tempBuildMoveForward != null && tempBuildMoveForward != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildMoveForward = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildMoveForward);

        string tempBuildMoveBackward = PlayerPrefs.GetString(SAVE_BTN_BUILD_MOVE_BACKWARDS);
        if (tempBuildMoveBackward != null && tempBuildMoveBackward != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildMoveBackwards = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildMoveBackward);

        string tempBuildMoveLeft = PlayerPrefs.GetString(SAVE_BTN_BUILD_MOVE_LEFT);
        if (tempBuildMoveLeft != null && tempBuildMoveLeft != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildMoveLeft = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildMoveLeft);

        string tempBuildMoveRight = PlayerPrefs.GetString(SAVE_BTN_BUILD_MOVE_RIGHT);
        if (tempBuildMoveRight != null && tempBuildMoveRight != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildMoveRight = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildMoveRight);

        string tempBuildMoveCamTo = PlayerPrefs.GetString(SAVE_BTN_BUILD_MOVECAMTOSELECTION);
        if (tempBuildMoveCamTo != null && tempBuildMoveCamTo != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildJumpToSelection = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildMoveCamTo);

        string tempBuildRemove = PlayerPrefs.GetString(SAVE_BTN_BUILD_REMOVE);
        if (tempBuildRemove != null && tempBuildRemove != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildRemove = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildRemove);

        string tempBuildSkills = PlayerPrefs.GetString(SAVE_BTN_BUILD_SKILLS);
        if (tempBuildSkills != null && tempBuildSkills != string.Empty)
            GameManager.Manager.GameSettings.KeyBuildSkills = (KeyCode)Enum.Parse(typeof(KeyCode), tempBuildSkills);

        #endregion
    }

    #region btn events

    private void OnBtnHeroRepairClicked()
    {
        SetNewKey(BtnHeroRepair);
    }

    private void onBtnBuildBuildMenuClicked()
    {
        SetNewKey(BtnBuildBuildMenu);
    }

    private void OnBtnBuildMoveCamToSelectionClicked()
    {
        SetNewKey(BtnBuildMoveCamToSelection);
    }

    private void OnBtnHeroSprintClicked()
    {
        SetNewKey(BtnHeroSprint);
    }

    private void OnBtnHeroChatClicked()
    {
        SetNewKey(BtnHeroChat);
    }

    private void OnBtnHeroHideHudClicked()
    {
        SetNewKey(BtnHeroHideHud);
    }

    private void OnBtnBuildMoveRightClicked()
    {
        SetNewKey(BtnBuildMoveRight);
    }

    private void OnBtnBuildMoveLeftClicked()
    {
        SetNewKey(BtnBuildMoveLeft);
    }

    private void OnBtnBuildMoveBackwardsClicked()
    {
        SetNewKey(BtnBuildMoveBackwards);
    }

    private void OnBtnBuildMoveForwardClicked()
    {
        SetNewKey(BtnBuildMoveForward);
    }

    private void OnBtnBuildSkillsClicked()
    {
        SetNewKey(BtnBuildSkills);
    }

    private void OnBtnBuildRemoveClicked()
    {
        SetNewKey(BtnBuildRemove);
    }

    private void OnBtnHeroSkillsClicked()
    {
        SetNewKey(BtnHeroSkills);
    }

    private void OnBtnHeroPauseClicked()
    {
        SetNewKey(BtnHeroPause);
    }

    private void OnBtnHeroSwitchModeClicked()
    {
        SetNewKey(BtnHeroSwitchMode);
    }

    private void OnBtnHeroJumpClicked()
    {
        SetNewKey(BtnHeroJump);
    }

    private void OnBtnHeroMoveRightClicked()
    {
        SetNewKey(BtnHeroMoveRight);
    }

    private void OnBtnHeroMoveLeftClicked()
    {
        SetNewKey(BtnHeroMoveLeft);
    }

    private void OnBtnHeroMoveBackwardsClicked()
    {
        SetNewKey(BtnHeroMoveBackwards);
    }

    private void OnBtnHeroMoveForwardClicked()
    {
        SetNewKey(BtnHeroMoveForward);
    }

    #endregion
}
