﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    public void LoadScene(int index)
    {
        if (index <= SceneManager.sceneCountInBuildSettings && index >= 0)
        {
            Time.timeScale = 1;
            SceneLoader.LoadScene(index);
        }
        else
            print("Index of Scene to Load is not in the SceneManager");
    }
}