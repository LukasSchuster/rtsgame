﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TutorialHandler : MonoBehaviour
{
    private const string CONT_MOVE_AROUND = "Move Around: ";
    private const string CONT_CAM_ANGLE_BUILD = "Ajust Angle: ";
    private const string CONT_SCROLL_BUILD = "Scroll: ";
    private const string CONT_ATTACK_HERO = "Attack: ";

    private const float SKIP_DELAY = 0.5f;
    private const string GAMEMANAGER_NAME = "GameManager";

    [Header("Panels")]
    public GameObject TutorialFinishedPanel;
    public GameObject TutorialPanel;
    public GameObject TutorialControllsPanelHero;
    public GameObject TutorialControllsPanelBuild;
    public Button ExitButton;

    [Header("Tasks")]
    public List<TutorialHandlerTask> Tasks = new List<TutorialHandlerTask>();

    [Header("Hero Controlls Panel")]
    #region Hero Controlls Panel

    public Text TCPHeroMoveAroundText;
    public Text TCPHeroAttackText;

    private bool heroForward = false;
    private bool heroBackwardward = false;
    private bool heroLeft = false;
    private bool heroRight = false;
    private bool heroAttack = false;

    #endregion

    [Header("Build Controlls Panel")]
    #region Build Controlls Panel

    public Text TCPBuildMoveAroundText;
    public Text TCPBuildScrollText;
    public Text TCPBuildCamAngleText;

    private bool buildForward = false;
    private bool buildBackwardward = false;
    private bool buildLeft = false;
    private bool buildRight = false;
    private bool buildScroll = false;
    private bool buildCamAngle = false;

    #endregion

    private GameManager gamemanager;
    private TutorialHandlerTask currentTask;

    private int taskIndex = 0;
    private bool skip = false;

    void Awake()
    {
        gamemanager = GameObject.Find(GAMEMANAGER_NAME).GetComponent<GameManager>();
        currentTask = Tasks[taskIndex];
        gamemanager.ShowTutorialPanel(currentTask.Image, currentTask.Name, currentTask.Description, currentTask.Hint, taskIndex + 1, Tasks.Count);
    }

    void Start()
    {
        CheckBuildControlls();
        CheckHeroControlls();
    }

    void Update()
    {
        CheckBuildControlls();
        CheckHeroControlls();

        if (skip)
        {
            if (Input.GetKey(KeyCode.Z))
                ExitButton.onClick.Invoke();
            return;
        }

        if (currentTask.CheckIfAccomplished(gamemanager))
        {
            taskIndex++;
            if (taskIndex < Tasks.Count)
            {
                if (taskIndex == 7)
                    TutorialControllsPanelHero.SetActive(true);

                currentTask = Tasks[taskIndex];
                gamemanager.ShowTutorialPanel(currentTask.Image, currentTask.Name, currentTask.Description, currentTask.Hint, taskIndex + 1, Tasks.Count);
                StartCoroutine(SkipTimer()); 
            }
            else
            {
                skip = true;
                if (TutorialFinishedPanel != null)
                    TutorialFinishedPanel.SetActive(true);
                if (TutorialPanel != null)
                    TutorialPanel.SetActive(false);
            }
        }
    }

    private void CheckBuildControlls()
    {
        if (TutorialControllsPanelBuild != null)
        {
            if (!TutorialControllsPanelBuild.activeSelf) return;

            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveForward)) buildForward = true;
            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveBackwards)) buildBackwardward = true;
            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveRight)) buildRight = true;
            if (Input.GetKey(GameManager.Manager.GameSettings.KeyBuildMoveLeft)) buildLeft = true;

            if (Input.GetAxis("Mouse ScrollWheel") != 0) buildScroll = true;

            if (Input.GetMouseButton(1)) buildCamAngle = true;

            if (buildForward && buildBackwardward && buildRight && buildLeft)
            {
                TCPBuildMoveAroundText.text = string.Format("<color=#08FF00FF>{0}</color> {1}, {2}, {3}, {4}", CONT_MOVE_AROUND,
                    gamemanager.GameSettings.KeyBuildMoveForward.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveLeft.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveBackwards.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveRight.ToString());
            }
            else
            {
                TCPBuildMoveAroundText.text = string.Format("<color=#FF0000FF>{0}</color> {1}, {2}, {3}, {4}", CONT_MOVE_AROUND,
                    gamemanager.GameSettings.KeyBuildMoveForward.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveLeft.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveBackwards.ToString(),
                    gamemanager.GameSettings.KeyBuildMoveRight.ToString());
            }

            if (buildScroll)
               TCPBuildScrollText.text = string.Format("<color=#08FF00FF>{0}</color> {1}", CONT_SCROLL_BUILD, "Scrollwheel");
            else
                TCPBuildScrollText.text = string.Format("<color=#FF0000FF>{0}</color> {1}", CONT_SCROLL_BUILD, "Scrollwheel");

            if (buildCamAngle)
                TCPBuildCamAngleText.text = string.Format("<color=#08FF00FF>{0}</color> Hold left mouse", CONT_CAM_ANGLE_BUILD);
            else
                TCPBuildCamAngleText.text = string.Format("<color=#FF0000FF>{0}</color> Hold left mouse", CONT_CAM_ANGLE_BUILD);

            if (buildForward && buildBackwardward && buildRight && buildLeft && buildScroll && buildCamAngle)
                Destroy(TutorialControllsPanelBuild, 0.7f);
        }
    }

    private void CheckHeroControlls()
    {
        if (TutorialControllsPanelHero != null)
        {
            if (!TutorialControllsPanelHero.activeSelf) return;

            if (gamemanager.GetGameController().CamMode == StrategieCameraController.CameraMode.HeroMode)
            {
                if (Input.GetKey(gamemanager.GameSettings.KeyHeroMoveForward)) heroForward = true;
                if (Input.GetKey(gamemanager.GameSettings.KeyHeroMoveBackwards)) heroBackwardward = true;
                if (Input.GetKey(gamemanager.GameSettings.KeyHeroMoveLeft)) heroLeft = true;
                if (Input.GetKey(gamemanager.GameSettings.KeyHeroMoveRight)) heroRight = true;

                if (Input.GetMouseButton(0) && !gamemanager.GetHeroController().IsRunning) heroAttack = true;

                if (heroForward && heroBackwardward && heroLeft && heroRight)
                {
                    TCPHeroMoveAroundText.text = string.Format("<color=#08FF00FF>{0}</color> {1}, {2}, {3}, {4}", CONT_MOVE_AROUND,
                    gamemanager.GameSettings.KeyHeroMoveForward.ToString(),
                    gamemanager.GameSettings.KeyHeroMoveLeft.ToString(),
                    gamemanager.GameSettings.KeyHeroMoveBackwards.ToString(),
                    gamemanager.GameSettings.KeyHeroMoveRight.ToString());
                }
                else
                {
                    TCPHeroMoveAroundText.text = string.Format("<color=#FF0000FF>{0}</color> {1}, {2}, {3}, {4}", CONT_MOVE_AROUND,
                        gamemanager.GameSettings.KeyHeroMoveForward.ToString(),
                        gamemanager.GameSettings.KeyHeroMoveLeft.ToString(),
                        gamemanager.GameSettings.KeyHeroMoveBackwards.ToString(),
                        gamemanager.GameSettings.KeyHeroMoveRight.ToString());
                }

                if (heroAttack)
                    TCPHeroAttackText.text = string.Format("<color=#08FF00FF>{0}</color> Left mouse", CONT_ATTACK_HERO);
                else
                    TCPHeroAttackText.text = string.Format("<color=#FF0000FF>{0}</color> Left mouse", CONT_ATTACK_HERO);

                if (heroAttack && heroForward && heroBackwardward && heroLeft && heroRight)
                    Destroy(TutorialControllsPanelHero, 0.8f);
            }
        }
    }

    IEnumerator SkipTimer()
    {
        gamemanager.HideTurorialPanel();
        skip = true;

        yield return new WaitForSeconds(SKIP_DELAY);

        skip = false;
        gamemanager.ShowTutorialPanel(currentTask.Image, currentTask.Name, currentTask.Description, currentTask.Hint, taskIndex + 1, Tasks.Count);
    }
}
