﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class PauseMenuManager : MonoBehaviour
{
    private const int GRASS_SHADER_DENSITY_THIN = 2;
    private const int GRASS_SHADER_DENSITY_DENSE = 4;
    private const int GRASS_SHADER_DENSITY_EXTREME = 6;

    private const int GRASS_SHADER_DISTANCE_SHORT = 48;
    private const int GRASS_SHADER_DISTANCE_MEDIUM = 30;
    private const int GRASS_SHADER_DISTANCE_FAR = 25;
    private const int GRASS_SHADER_DISTANCE_EXTREME = 12;

    private const int GRASS_TERRAIN_DISTANCE_SHORT = 10;
    private const int GRASS_TERRAIN_DISTANCE_MEDIUM = 30;
    private const int GRASS_TERRAIN_DISTANCE_FAR = 60;
    private const int GRASS_TERRAIN_DISTANCE_EXTREME = 100;

    public static bool ShadowOn = true;

    public Button ResumeButton;
    public Button OptionsButton;
    public Button ExitSessionButton;
    public Button ExitGameButton;

    [Header("Options")]
    public GameObject OptionsPanel;
    public Toggle SkyBoxDetailsTogle;
    public Toggle ShadowTogle;
    public Toggle EnvironmentTogle;
    public Slider CameraSpeedSlider;
    public Toggle AntiAlaisingTogle;
    public Toggle CrossHairToggle;
    public Slider CrossHairSizeSlider;

    [Header("Options Controlls")]
    public GameObject ControllsPanel;
    public Button ControllsButton;

    [Header("Extende Options")]
    public GameObject ExtendedOptionsPanel;
    public Button ExtendedOptionsButton;

    public Dropdown GrassTypeDropDown;
    public Dropdown GrassDistanceDropDown;
    public Dropdown GrassDensityDropDown;
    public Toggle GrassShadowToggle;

    private bool showShadows = true;
    private bool shaderGrass = false;

    void Awake()
    {
        ExitGameButton.onClick.AddListener(delegate { OnExitGameButtonClicked(); });
        ResumeButton.onClick.AddListener(delegate { OnResumeClicked(); });
        OptionsButton.onClick.AddListener(delegate { OnOptionsClicked(); });
        ShadowTogle.onValueChanged.AddListener(delegate { OnShadowTogleChanged(); });
        EnvironmentTogle.onValueChanged.AddListener(delegate { OnEnvironmentTogleChanged(); });
        CameraSpeedSlider.onValueChanged.AddListener(delegate { OnCameraSpeedSliderChanged(); });
        AntiAlaisingTogle.onValueChanged.AddListener(delegate { OnAntiAliasingValueChanged(); });
        SkyBoxDetailsTogle.onValueChanged.AddListener(delegate { OnSkyBoxDetailsTogleChanged(); });
        ControllsButton.onClick.AddListener(delegate { OnControllsButtonClicked(); });
        CrossHairToggle.onValueChanged.AddListener(delegate { OnCrossHairToggleChanged(); });
        CrossHairSizeSlider.onValueChanged.AddListener(delegate { OnCrossHairSizeSliderChanged(); });

        ExtendedOptionsButton.onClick.AddListener(delegate { OnExtendedOptionsButtonClicked(); });
        GrassTypeDropDown.onValueChanged.AddListener(delegate { OnGrassTypeDropDownChanged(); });
        GrassDistanceDropDown.onValueChanged.AddListener(delegate { OnGrassDistanceDropDownChanged(); });
        GrassDensityDropDown.onValueChanged.AddListener(delegate { OnGrassDensityDropDownChanged(); });
        GrassShadowToggle.onValueChanged.AddListener(delegate { OnGrassShadowToggleChanged(); });
    }

    void Update()
    {
        if (gameObject.activeSelf)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void OnEnable()
    {
        Cursor.visible = true;
    }

    void OnDisable()
    {
        Cursor.visible = false;
    }

    public void ApplyChanges()
    {
        OnGrassShadowToggleChanged();
        OnGrassDistanceDropDownChanged();
        OnGrassDensityDropDownChanged();
        OnGrassTypeDropDownChanged();
        OnCrossHairSizeSliderChanged();
        OnCrossHairToggleChanged();
        OnSkyBoxDetailsTogleChanged();
        OnCameraSpeedSliderChanged();
        OnAntiAliasingValueChanged();
        OnEnvironmentTogleChanged();
        OnShadowTogleChanged();
        GameManager.Manager.GetAudioManager().OnEffectsSliderChanged();
        GameManager.Manager.GetAudioManager().OnMusicSliderChanged();
    }

    public void SetOptionsPanelActive(bool value)
    {
        OptionsPanel.gameObject.SetActive(value);
    }

    private void OnGrassShadowToggleChanged()
    {
        if (GrassShadowToggle.isOn)
        {
            GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Grass Shadows turned On.", "", 1.5f);
        }
        else
        {
            GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Grass Shadows turned Off.", "", 1.5f);
        }

        GameManager.Manager.GameSettings.ShaderGrassShadow = GrassShadowToggle.isOn;
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_SHADOW, GrassShadowToggle.isOn);
    }

    private void OnGrassDistanceDropDownChanged()
    {
        switch (GrassDistanceDropDown.value)
        {
            case 0:
                if (shaderGrass)
                    GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_EdgeLength", GRASS_SHADER_DISTANCE_SHORT);
                else
                    GameManager.Manager.GetTerrain().detailObjectDistance = GRASS_TERRAIN_DISTANCE_SHORT;

                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Shader Grass Render Distance: {0}", "Short"), "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDistance = GameSettings.ShaderGrassDistances.Short;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DISTANCE, 0);
                break;

            case 1:
                if (shaderGrass)
                    GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_EdgeLength", GRASS_SHADER_DISTANCE_MEDIUM);
                else
                    GameManager.Manager.GetTerrain().detailObjectDistance = GRASS_TERRAIN_DISTANCE_MEDIUM;

                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Shader Grass Render Distance: {0}", "Medium"), "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDistance = GameSettings.ShaderGrassDistances.Medium;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DISTANCE, 1);
                break;

            case 2:
                if (shaderGrass)
                    GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_EdgeLength", GRASS_SHADER_DISTANCE_FAR);
                else
                    GameManager.Manager.GetTerrain().detailObjectDistance = GRASS_TERRAIN_DISTANCE_FAR;

                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Shader Grass Render Distance: {0}", "Far"), "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDistance = GameSettings.ShaderGrassDistances.Far;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DISTANCE, 2);
                break;

            case 3:
                if (shaderGrass)
                    GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_EdgeLength", GRASS_SHADER_DISTANCE_EXTREME);
                else
                    GameManager.Manager.GetTerrain().detailObjectDistance = GRASS_TERRAIN_DISTANCE_EXTREME;

                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Shader Grass Render Distance: {0}", "Extreme"), "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDistance = GameSettings.ShaderGrassDistances.Extreme;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DISTANCE, 3);
                break;
        }
    }

    private void OnGrassDensityDropDownChanged()
    {
        switch (GrassDensityDropDown.value)
        {
            case 0:
                GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_MaxTessellation", GRASS_SHADER_DENSITY_THIN);
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Grass Density changed to Thin.", "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDensity = GameSettings.ShaderGrassDensitys.Thin;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DENSITY, 0);
                break;

            case 1:
                GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_MaxTessellation", GRASS_SHADER_DENSITY_DENSE);
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Grass Density changed to Dense.", "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDensity = GameSettings.ShaderGrassDensitys.Dense;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DENSITY, 1);
                break;

            case 2:
                GameManager.Manager.GetGrassPlane().GetComponent<MeshRenderer>().material.SetFloat("_MaxTessellation", GRASS_SHADER_DENSITY_EXTREME);
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Grass Density changed to Extreme.", "", 1.5f);
                GameManager.Manager.GameSettings.ShaderGrassDensity = GameSettings.ShaderGrassDensitys.Extreme;
                GameManager.Manager.GameSettings.SaveUserPrefSettingEnum(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_DENSITY, 2);
                break;
        }
    }

    private void OnGrassTypeDropDownChanged()
    {
        switch (GrassTypeDropDown.value)
        {
            case 0:
                shaderGrass = true;
                GameManager.Manager.GetGrassPlane().SetActive(true);
                GameManager.Manager.GetTerrain().detailObjectDistance = 0;
                GameManager.Manager.GameSettings.ShaderGrass = true;
                GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_ON, true);
                break;

            case 1:
                shaderGrass = false;
                GameManager.Manager.GetGrassPlane().SetActive(false);
                GameManager.Manager.GameSettings.ShaderGrass = false;
                GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_SHADER_GRASS_ON, false);
                break;
        }
    }

    private void OnExtendedOptionsButtonClicked()
    {
        ExtendedOptionsPanel.gameObject.SetActive(ExtendedOptionsPanel.activeSelf ? false : true);

        ControllsPanel.SetActive(false);
    }

    private void OnCrossHairSizeSliderChanged()
    {
        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Crosshair scale changed to: {0:0.00}", CrossHairSizeSlider.value), "", 1.5f);

        GameManager.Manager.GameSettings.CrossHairScale = CrossHairSizeSlider.value;
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_CROSSHAIR_SCALE, CrossHairSizeSlider.value);
    }

    private void OnCrossHairToggleChanged()
    {
        GameManager.Manager.GameSettings.DrawCrossHair = CrossHairToggle.isOn;

        if (CrossHairToggle.isOn)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Crosshair turned On!", "", 1.5f);
            GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_CROSSHAIR, true);
        }
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Crosshair turned Off!", "", 1.5f);
            GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_CROSSHAIR, false);
        }
    }

    private void OnControllsButtonClicked()
    {
        ControllsPanel.SetActive(ControllsPanel.activeSelf ? false : true);

        ExtendedOptionsPanel.SetActive(false);
    }

    private void OnSkyBoxDetailsTogleChanged()
    {
        GameManager.Manager.GetSkyBoxTerrain().SetActive(SkyBoxDetailsTogle.isOn);
        GameManager.Manager.GameSettings.DrawSkyBox = SkyBoxDetailsTogle.isOn;
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_SKYBOX, SkyBoxDetailsTogle.isOn);

        if (SkyBoxDetailsTogle.isOn)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "SkyBox Details Turned On", "", 1.5f);
        else
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "SkyBox Details Turned Off", "", 1.5f);
    }

    private void OnAntiAliasingValueChanged()
    {
        Camera.main.GetComponent<Antialiasing>().enabled = AntiAlaisingTogle.isOn;
        GameManager.Manager.GameSettings.AntiAliasing = AntiAlaisingTogle.isOn;
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_ANTI_ALIASING, AntiAlaisingTogle.isOn);

        if (AntiAlaisingTogle.isOn)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "AntiAliasing Turned On", "", 1.5f);
        else
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "AntiAliasing Turned Off", "", 1.5f);
    }

    private void OnCameraSpeedSliderChanged()
    {
        GameManager.Manager.GetGameController().Speed = CameraSpeedSlider.value;
        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Camera Speed: {0:0.00}", CameraSpeedSlider.value), "", 1.5f);

        GameManager.Manager.GameSettings.CameraSpeed = CameraSpeedSlider.value;
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_CAMERA_SPEED, CameraSpeedSlider.value);
    }

    private void OnOptionsClicked()
    {
        OptionsPanel.gameObject.SetActive(OptionsPanel.gameObject.activeSelf ? false : true);
        ExtendedOptionsPanel.gameObject.SetActive(false);
        ControllsPanel.gameObject.SetActive(false);
    }

    private void OnExitGameButtonClicked()
    {
        Application.Quit();
    }

    private void OnResumeClicked()
    {
        GameManager.Manager.ResumeGame();
        OptionsPanel.gameObject.SetActive(false);
        ExtendedOptionsPanel.gameObject.SetActive(false);
        ControllsPanel.gameObject.SetActive(false);
    }

    private void OnEnvironmentTogleChanged()
    {
        if (EnvironmentTogle.isOn)
        {
            GameManager.Manager.GetTerrain().drawTreesAndFoliage = true;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Environment Rendering Turned On", "", 1.5f);
            GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_ENVIRONMENT, true);
        }
        else
        {
            GameManager.Manager.GetTerrain().drawTreesAndFoliage = false;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Environment Rendering Turned Off", "", 1.5f);
            GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_ENVIRONMENT, false);
        }

        GameManager.Manager.GameSettings.DrawEnvironment = EnvironmentTogle.isOn;
    }

    private void OnShadowTogleChanged()
    {
        if (ShadowTogle.isOn)
        {
            for (int i = 0; i < GameManager.Manager.GetAllGameobjects().Length; i++)
            {
                if (GameManager.Manager.GetAllGameobjects()[i].GetComponent<MeshRenderer>() != null)
                    GameManager.Manager.GetAllGameobjects()[i].GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                ShadowOn = true;
            }

            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Shadows Turned On", "", 1.5f);
        }
        else
        {
            for (int i = 0; i < GameManager.Manager.GetAllGameobjects().Length; i++)
            {
                if (GameManager.Manager.GetAllGameobjects()[i].GetComponent<MeshRenderer>() != null)
                    GameManager.Manager.GetAllGameobjects()[i].GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                ShadowOn = false;
            }

            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, "Shadows Turned Off", "", 1.5f);
        }

        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_DRAW_SHADOWS, ShadowTogle.isOn);
        GameManager.Manager.GameSettings.Shadows = ShadowTogle.isOn;
    }
}
