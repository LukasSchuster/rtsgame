﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    #region Save Key names

    public string SAVE_EFFECTS_VOLUME = "KeyEffectsVolume";
    public string SAVE_MUSIC_VOLUME = "KeyMusicVolume";
    public string SAVE_ANTI_ALIASING = "KeyAntiAliasing";
    public string SAVE_DRAW_ENVIRONMENT = "KeyDrawEnvironment";
    public string SAVE_DRAW_SKYBOX = "KeyDrawSkyBox";
    public string SAVE_DRAW_SHADOWS = "KeyDrawShadows";
    public string SAVE_CAMERA_SPEED = "KeyCamSpeed";
    public string SAVE_DRAW_CROSSHAIR = "KeyDrawCrosshair";
    public string SAVE_CROSSHAIR_SCALE = "KeyCrosshairScale";
    public string SAVE_SHADER_GRASS_SHADOW = "KeyShaderGrassShadow";
    public string SAVE_SHADER_GRASS_ON = "KeyShaderGrassOn";
    public string SAVE_SHADER_GRASS_DISTANCE = "KeyShaderGrassDistance";
    public string SAVE_SHADER_GRASS_DENSITY = "KeyShaderGrassDensity";

    #endregion

    public float EffectsVolume = 0.5f;
    public float MusicVolume = 0.5f;

    public bool AntiAliasing = false;
    public bool Shadows = true;
    public bool DrawEnvironment = true;
    public bool DrawSkyBox = true;
    public bool ShaderGrass = true;
    public bool ShaderGrassShadow = true;
    public bool DrawCrossHair = true;
    public float CrossHairScale = 0.5f;
    public float CameraSpeed = 0.1f;

    public enum ShaderGrassDistances { Short = 0, Medium = 1, Far = 2, Extreme = 3};
    public ShaderGrassDistances ShaderGrassDistance = ShaderGrassDistances.Medium;

    public enum ShaderGrassDensitys { Thin = 0, Dense = 1, Extreme = 2 };
    public ShaderGrassDensitys ShaderGrassDensity = ShaderGrassDensitys.Dense;

    public KeyCode KeyHeroMoveForward = KeyCode.W;
    public KeyCode KeyHeroMoveBackwards = KeyCode.S;
    public KeyCode KeyHeroMoveLeft = KeyCode.A;
    public KeyCode KeyHeroMoveRight = KeyCode.D;
    public KeyCode KeyHeroJump = KeyCode.Space;
    public KeyCode KeySwitchMode = KeyCode.Tab;
    public KeyCode KeyHeroPause = KeyCode.Escape;
    public KeyCode KeyHeroSkills = KeyCode.C;
    public KeyCode KeyHeroHideHud = KeyCode.H;
    public KeyCode KeyHeroChatWindow = KeyCode.Return;
    public KeyCode KeyHeroSprint = KeyCode.LeftShift;
    public KeyCode KeyHeroRepair = KeyCode.R;

    public KeyCode KeyBuildMenu = KeyCode.B;
    public KeyCode KeyBuildRemove = KeyCode.Delete;
    public KeyCode KeyBuildSkills = KeyCode.C;
    public KeyCode KeyBuildMoveForward = KeyCode.W;
    public KeyCode KeyBuildMoveBackwards = KeyCode.S;
    public KeyCode KeyBuildMoveLeft = KeyCode.A;
    public KeyCode KeyBuildMoveRight = KeyCode.D;
    public KeyCode KeyBuildJumpToSelection = KeyCode.Space;

    public void SetSettingsToUserPrefs()
    {
        #region Get Saved Values

        EffectsVolume = PlayerPrefs.GetFloat(SAVE_EFFECTS_VOLUME);
        MusicVolume = PlayerPrefs.GetFloat(SAVE_MUSIC_VOLUME);

        float tempSaveCamSpeed = PlayerPrefs.GetFloat(SAVE_CAMERA_SPEED);
        if (tempSaveCamSpeed != 0)
            CameraSpeed = tempSaveCamSpeed;

        float tempSaveCrosshairScale = PlayerPrefs.GetFloat(SAVE_CROSSHAIR_SCALE);
        if (tempSaveCrosshairScale != 0)
            CrossHairScale = tempSaveCrosshairScale;

        AntiAliasing = PlayerPrefs.GetInt(SAVE_ANTI_ALIASING) == 0 ? false : true;
        Shadows = PlayerPrefs.GetInt(SAVE_DRAW_SHADOWS) == 0 ? false : true;
        DrawEnvironment = PlayerPrefs.GetInt(SAVE_DRAW_ENVIRONMENT) == 0 ? false : true;
        DrawSkyBox = PlayerPrefs.GetInt(SAVE_DRAW_SKYBOX) == 0 ? false : true;
        ShaderGrass = PlayerPrefs.GetInt(SAVE_SHADER_GRASS_ON) == 0 ? false : true;
        ShaderGrassShadow = PlayerPrefs.GetInt(SAVE_SHADER_GRASS_SHADOW) == 0 ? false : true;
        DrawCrossHair = PlayerPrefs.GetInt(SAVE_DRAW_CROSSHAIR) == 0 ? false : true;

        int tempShaderGrassDistanceIndex = PlayerPrefs.GetInt(SAVE_SHADER_GRASS_DISTANCE);
        switch (tempShaderGrassDistanceIndex)
        {
            case 0:
                ShaderGrassDistance = ShaderGrassDistances.Short;
                break;

            case 1:
                ShaderGrassDistance = ShaderGrassDistances.Medium;
                break;

            case 2:
                ShaderGrassDistance = ShaderGrassDistances.Far;
                break;

            case 3:
                ShaderGrassDistance = ShaderGrassDistances.Extreme;
                break;
        }

        int tempShaderGrassDensityIndex = PlayerPrefs.GetInt(SAVE_SHADER_GRASS_DENSITY);
        switch (tempShaderGrassDensityIndex)
        {
            case 0:
                ShaderGrassDensity = ShaderGrassDensitys.Thin;
                break;

            case 1:
                ShaderGrassDensity = ShaderGrassDensitys.Dense;
                break;

            case 2:
                ShaderGrassDensity = ShaderGrassDensitys.Extreme;
                break;
        }

        #endregion

        #region Apply Saved Values to UI and Entitys 

        GameManager.Manager.GetAudioManager().EffectsVolume = EffectsVolume;
        GameManager.Manager.GetAudioManager().MusicVolume = MusicVolume;
        GameManager.Manager.GetAudioManager().EffectsSlider.value = EffectsVolume;
        GameManager.Manager.GetAudioManager().MusicSlider.value = MusicVolume;
        GameManager.Manager.GetPauseMenuManager().GrassShadowToggle.isOn = ShaderGrassShadow;
        GameManager.Manager.GetPauseMenuManager().GrassDistanceDropDown.value = (int)ShaderGrassDistance;
        GameManager.Manager.GetPauseMenuManager().GrassDensityDropDown.value = (int)ShaderGrassDensity;
        GameManager.Manager.GetPauseMenuManager().GrassTypeDropDown.value = ShaderGrass ? 0 : 1;
        GameManager.Manager.GetPauseMenuManager().CrossHairSizeSlider.value = CrossHairScale;
        GameManager.Manager.GetPauseMenuManager().CrossHairToggle.isOn = DrawCrossHair;
        GameManager.Manager.GetPauseMenuManager().SkyBoxDetailsTogle.isOn = DrawSkyBox;
        GameManager.Manager.GetPauseMenuManager().AntiAlaisingTogle.isOn = AntiAliasing;
        GameManager.Manager.GetPauseMenuManager().CameraSpeedSlider.value = CameraSpeed;
        GameManager.Manager.GetPauseMenuManager().EnvironmentTogle.isOn = DrawEnvironment;
        GameManager.Manager.GetPauseMenuManager().ShadowTogle.isOn = Shadows;

        GameManager.Manager.GetPauseMenuManager().ApplyChanges();

        #endregion
    }

    public void SaveUserPrefSetting(string keyName, bool value)
    {
        if (value) PlayerPrefs.SetInt(keyName, 1);
        else PlayerPrefs.SetInt(keyName, 0);

        PlayerPrefs.Save();
    }

    public void SaveUserPrefSetting(string keyName, float value)
    {
        PlayerPrefs.SetFloat(keyName, value);
        PlayerPrefs.Save();
    }

    public void SaveUserPrefSettingEnum(string keyName, int index)
    {
        PlayerPrefs.SetInt(keyName, index);
        PlayerPrefs.Save();
    }
}