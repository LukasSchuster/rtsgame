﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameStats : MonoBehaviour
{
    [Header("UI")]
    public Text MinionsKilledText;
    public Text PlayersKilledText;
    public Text SkillsBoughtText;
    public Text SkillsUsedText;
    public Text CollectedWoodText;
    public Text CollectedFoodText;
    public Text CollectedGoldText;

    [Header("Stats")]
    public int MinionKillCount = 0;
    public int PlayerKillCount = 0;
    public int SkillsBought = 0;
    public int SkillsUsed = 0;
    public int CollectedWood = 0;
    public int CollectedGold = 0;
    public int CollectedFood = 0;

    public void PrintGameStatsToPanel()
    {
        MinionsKilledText.text = string.Format("Minions Killed: {0}", MinionKillCount);
        PlayersKilledText.text = string.Format("Players Killed: {0}", PlayerKillCount);
        SkillsBoughtText.text = string.Format("Skills Bought: {0}", SkillsBought);
        SkillsUsedText.text = string.Format("Skills Used: {0}", SkillsUsed);
        CollectedWoodText.text = string.Format("Collected Wood: {0}", CollectedWood);
        CollectedFoodText.text = string.Format("Collected Food: {0}", CollectedFood);
        CollectedGoldText.text = string.Format("Collected Gold: {0}", CollectedGold);
    }
}
