﻿using UnityEngine;
using System.Collections;

public class TutorialHandlerTask : MonoBehaviour
{
    private const int MINIONS_TO_KILL = 2;

    public Sprite Image;
    public string Name;
    public string Description;
    public string Hint;

    public enum TutorialTaksType { Select = 1, Build = 2, Kill = 3, Input = 4, Upgrade = 5}
    public TutorialTaksType Type = TutorialTaksType.Input;

    public enum SelectBuildingType { Coinfarmer = 1, MinionSpawner = 2 }
    public SelectBuildingType SelectType = SelectBuildingType.Coinfarmer;

    public enum BuildBuildingType { Tower = 1, WoodCutter = 2, Temple = 3, Baker = 4}
    public BuildBuildingType BuildType = BuildBuildingType.Tower;

    public enum InputKeyType { Skills = 1, HeroMode = 2, UseSkill = 3}
    public InputKeyType InputType = InputKeyType.Skills;

    public bool CheckIfAccomplished(GameManager manager)
    {
        switch (Type)
        {
            case TutorialTaksType.Select:
                for (int i = 0; i < GameManager.SelectedObjects.Count; i++)
                {
                    if (GameManager.SelectedObjects[i] is CoinFarmerController && SelectType == SelectBuildingType.Coinfarmer) return true;
                    if (GameManager.SelectedObjects[i] is MinionSpawnerController && SelectType == SelectBuildingType.MinionSpawner) return true;
                }
                return false;

            case TutorialTaksType.Build:
                if (GameManager.Manager.TowerBuild && BuildType == BuildBuildingType.Tower) return true;
                if (GameManager.Manager.CutterBuild && BuildType == BuildBuildingType.WoodCutter) return true;
                if (GameManager.Manager.TempleBuild && BuildType == BuildBuildingType.Temple) return true;
                if (GameManager.Manager.BakerBuild && BuildType == BuildBuildingType.Baker) return true;
                return false;

            case TutorialTaksType.Kill:
                if (GameManager.Manager.GameStats.MinionKillCount >= MINIONS_TO_KILL) return true;
                return false;

            case TutorialTaksType.Input:
                if (Input.GetKey(KeyCode.Tab) && InputType == InputKeyType.HeroMode) return true;
                if (Input.GetKey(KeyCode.C) && InputType == InputKeyType.Skills) return true;
                if (((Input.GetKey(KeyCode.Alpha1)) || (Input.GetKey(KeyCode.Alpha2)) || (Input.GetKey(KeyCode.Alpha3)) || (Input.GetKey(KeyCode.Alpha4)) || (Input.GetKey(KeyCode.Alpha5)) || (Input.GetKey(KeyCode.Alpha6))) && InputType == InputKeyType.UseSkill)
                    return true;

                return false;

            case TutorialTaksType.Upgrade:
                if (manager.IsUpgradeBought())
                    return true;
                return false;

            default:
                return false;
        }
    }
}
