﻿using UnityEngine;
using System.Collections;

public class MakeOverlay : MonoBehaviour
{
    public bool OnAwake;
    public Material Material;
    public float Duration;
    public float ProjectorHeightOffset;
    public bool RandomOrtiantation;
    public bool ShrinkOnDeath;

    [Header("Movement")]
    public bool Moving;
    public float MoveSpeed;
    public float MoveDuration;
    public Vector3 Direction;


    void Awake()
    {
        if (OnAwake)
        {
            Overlay overlay = new Overlay(Material, new Vector3(transform.position.x, Utilitys.GivePositionOnFloor(transform.position).y + ProjectorHeightOffset, transform.position.z), Duration, RandomOrtiantation, ShrinkOnDeath);

            if (Moving) overlay.CreateMoving(Direction, MoveSpeed, MoveDuration);
            else overlay.Create();
        }
    }
}
