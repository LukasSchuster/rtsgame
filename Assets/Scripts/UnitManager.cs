﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitManager : MonoBehaviour
{
    public static UnitManager Manager;

    public LinkedList<Health> FriendlyUnits { get { return friendlyUnits; } }
    public LinkedList<Health> EnemyUnits { get { return enemyUnits; } }

    private LinkedList<Health> friendlyUnits = new LinkedList<Health>();
    private LinkedList<Health> enemyUnits = new LinkedList<Health>();

    private List<Health> friendlyUnitsToRemove = new List<Health>();
    private List<Health> enemyUnitsToRemove = new List<Health>();

    private bool removeThisFrame = false;

    void Awake()
    {
        Manager = this;

        friendlyUnitsToRemove.Capacity = 20;
        enemyUnitsToRemove.Capacity = 20;
    }

    public void AddFriendlyUnit(Health unit)
    {
        if (friendlyUnits.Count == 0)
            friendlyUnits.AddFirst(unit);
        else
            friendlyUnits.AddLast(unit);
    }

    public void AddEnemyUnit(Health unit)
    {
        if (enemyUnits.Count == 0)
            enemyUnits.AddFirst(unit);
        else
            enemyUnits.AddLast(unit);
    }

    public void RemoveEnemyUnit(Health unit)
    {
        enemyUnitsToRemove.Add(unit);

        if (!removeThisFrame)
        {
            removeThisFrame = true;
            StartCoroutine(RemoveTimer());
        }
    }

    public void RemoveFriendlyUnit(Health unit)
    {
        friendlyUnitsToRemove.Add(unit);

        if (!removeThisFrame)
        {
            removeThisFrame = true;
            StartCoroutine(RemoveTimer());
        }
    }

    IEnumerator RemoveTimer()
    {
        yield return new WaitForEndOfFrame();
        removeThisFrame = false;

        if (enemyUnitsToRemove.Count > 0)
        {
            for (int i = 0; i < enemyUnitsToRemove.Count; i++)
            {
                if (enemyUnits.Contains(enemyUnitsToRemove[i]))
                    enemyUnits.Remove(enemyUnitsToRemove[i]);
            }
        }

        if (friendlyUnitsToRemove.Count > 0)
        {
            for (int i = 0; i < friendlyUnitsToRemove.Count; i++)
            {
                if (friendlyUnits.Contains(friendlyUnitsToRemove[i]))
                    friendlyUnits.Remove(friendlyUnitsToRemove[i]);
            }
        }

        enemyUnitsToRemove.Clear();
        friendlyUnitsToRemove.Clear();
    }
}
