﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class UIHint : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    public string HintText;
    public bool ShowHint = true;
    public bool BlockRays = false;
    public bool ShowKeyCodeExtension = false;

    public enum KeyCodeChar
    {
        KeyHeroMoveForward = 1,
        KeyHeroMoveBackwards = 2,
        KeyHeroMoveLeft = 3, 
        KeyHeroMoveRight = 4, 
        KeyHeroJump = 5, 
        KeySwitchMode = 6, 
        KeyHeroPause = 7, 
        KeyHeroSkills = 8, 
        KeyHeroHideHud = 9, 
        KeyBuildRemove = 10,
        KeyBuildSkills = 11,
        KeyBuildMoveForward = 12,
        KeyBuildMoveBackwards = 13,
        KeyBuildMoveLeft = 14,
        KeyBuildMoveRight = 15
    }
    public KeyCodeChar Key;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (BlockRays) GameManager.Manager.UIPressed();
        if (ShowHint)
        {
            if (ShowKeyCodeExtension) GameManager.Manager.ShowHint(string.Format("{0} ({1})", HintText, GetKeyFromKeyCodeString()));
            else GameManager.Manager.ShowHint(HintText);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (BlockRays) GameManager.Manager.UIDePressed();
        if (ShowHint) GameManager.Manager.HideHint();
    }

    private string GetKeyFromKeyCodeString()
    {
        switch (Key)
        {
            case KeyCodeChar.KeyHeroMoveForward:
                return GameManager.Manager.GameSettings.KeyHeroMoveForward.ToString();
            case KeyCodeChar.KeyHeroMoveBackwards:
                return GameManager.Manager.GameSettings.KeyHeroMoveBackwards.ToString();
            case KeyCodeChar.KeyHeroMoveLeft:
                return GameManager.Manager.GameSettings.KeyHeroMoveLeft.ToString();
            case KeyCodeChar.KeyHeroMoveRight:
                return GameManager.Manager.GameSettings.KeyHeroMoveRight.ToString();
            case KeyCodeChar.KeyHeroJump:
                return GameManager.Manager.GameSettings.KeyHeroJump.ToString();
            case KeyCodeChar.KeySwitchMode:
                return GameManager.Manager.GameSettings.KeySwitchMode.ToString();
            case KeyCodeChar.KeyHeroPause:
                return GameManager.Manager.GameSettings.KeyHeroPause.ToString();
            case KeyCodeChar.KeyHeroSkills:
                return GameManager.Manager.GameSettings.KeyHeroSkills.ToString();
            case KeyCodeChar.KeyHeroHideHud:
                return GameManager.Manager.GameSettings.KeyHeroHideHud.ToString();
            case KeyCodeChar.KeyBuildRemove:
                return GameManager.Manager.GameSettings.KeyBuildRemove.ToString();
            case KeyCodeChar.KeyBuildSkills:
                return GameManager.Manager.GameSettings.KeyBuildSkills.ToString();
            case KeyCodeChar.KeyBuildMoveForward:
                return GameManager.Manager.GameSettings.KeyBuildMoveForward.ToString();
            case KeyCodeChar.KeyBuildMoveBackwards:
                return GameManager.Manager.GameSettings.KeyBuildMoveBackwards.ToString();
            case KeyCodeChar.KeyBuildMoveLeft:
                return GameManager.Manager.GameSettings.KeyBuildMoveLeft.ToString();
            case KeyCodeChar.KeyBuildMoveRight:
                return GameManager.Manager.GameSettings.KeyBuildMoveRight.ToString();
            default:
                return string.Empty;
        }
    }
}
