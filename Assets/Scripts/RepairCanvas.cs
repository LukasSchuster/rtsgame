﻿using UnityEngine;
using System.Collections;

public class RepairCanvas : MonoBehaviour
{
    public static GameObject Create(Vector3 pos, Sprite image, string name, string description, string repairText, string repairCost)
    {
        GameObject canvasGO = Instantiate(GameManager.Manager.GetRepairCanvasPrefab(), pos, Quaternion.identity) as GameObject;
        canvasGO.GetComponent<RepairCanvasController>().SetValues(image, name, description, repairCost, repairText);

        return canvasGO;
    }

    public static GameObject Create(Vector3 pos, Sprite image, Health health, string name, string description, string repairText, string repairCost, int gpCostPerHp)
    {
        GameObject canvasGO = Instantiate(GameManager.Manager.GetRepairCanvasPrefab(), pos, Quaternion.identity) as GameObject;
        canvasGO.GetComponent<RepairCanvasController>().SetValues(image, name, description, repairCost, repairText, health, gpCostPerHp);

        return canvasGO;
    }
}
