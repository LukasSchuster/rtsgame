﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SwordFighter : Unit
{
    private const int DAMAGERMARGINPERCENTAGE = 10;

    [Header("Specific")]
    public float ViewDistance;
    public float AttackDistance;
    public float AttackDelay;
    public AudioClip AttackSoundClip;

    private Animation myAnimation;
    private Health targetHealth;
    private bool attacking;

    void Awake()
    {
        myAnimation = GetComponent<Animation>();
    }

    void Start()
    {
        OnCreated();

        Health.OnDeath += Health_OnDeath;
        Health.OnDamaged += Health_OnDamaged;

        UnitManager.Manager.AddFriendlyUnit(Health);
    }

    private void Health_OnDamaged(float damage)
    {
        //DamageIndicator.Create(new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), damage, 1.7f, 2f, GameManager.Manager.FriendlyDamageColor);
    }

    private void Health_OnDeath()
    {
        UnitManager.Manager.RemoveFriendlyUnit(Health);
        Destroy(gameObject);
    }

    void Update()
    {
        if (targetHealth == null || !targetHealth.Allive)
        {
            attacking = false;
            CheckForEnemysInSight();
        }
        else
            AttackTarget();

        ControllAnaimations();
    }

    private void AttackTarget()
    {
        if (Vector3.Distance(transform.position, targetHealth.transform.position) <= AttackDistance)
        {
            if (!attacking)
            {
                StartCoroutine(AttacKTimer());
                attacking = true;
            }
            Agent.velocity = Vector3.zero;
        }
        else
        {
            attacking = false;
            Agent.SetDestination(targetHealth.transform.position);
        }
    }

    private void CheckForEnemysInSight()
    {
        float shortestDistance = 0;
        Health enemy = null;

        foreach (Health enemyHealth in UnitManager.Manager.EnemyUnits)
        {
            if (enemyHealth == null)
                continue;

            float currentDistance = Vector3.Distance(enemyHealth.transform.position, transform.position);

            if (currentDistance >= ViewDistance)
                continue;
            else
            {
                if (shortestDistance == 0 || shortestDistance > currentDistance)
                {
                    shortestDistance = currentDistance;
                    enemy = enemyHealth;
                }
            }
        }

        if (enemy != null)
            targetHealth = enemy;
    }

    private void ControllAnaimations()
    {
        if (Agent.velocity.sqrMagnitude > 0.5f)
            myAnimation.Play("walk", PlayMode.StopAll);
        else
        {

            if (attacking)
                myAnimation.Play("attack", PlayMode.StopAll);
            else
                myAnimation.Play("free", PlayMode.StopAll);
        }
    }

    public override void CallOnSelectedRightClick()
    {
        base.CallOnSelectedRightClick();
        targetHealth = null;
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, Description, null);
        base.OnSelected(ref selectedObjects);
    }

    IEnumerator AttacKTimer()
    {
        yield return new WaitForSeconds(AttackDelay);

        if (gameObject != null)
        {
            if (attacking)
            {
                if (AttackSoundClip != null)
                    AudioSource.PlayClipAtPoint(AttackSoundClip, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

                if (targetHealth != null)
                {
                    float damageMagrin = AttackDamage / 100 * DAMAGERMARGINPERCENTAGE; 
                    targetHealth.DecreaseHealth(UnityEngine.Random.Range(AttackDamage - damageMagrin, AttackDamage + damageMagrin));
                }

                StartCoroutine(AttacKTimer());
            }
        }
    }
}
