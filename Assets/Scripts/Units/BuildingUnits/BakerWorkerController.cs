﻿using UnityEngine;
using System.Collections;
using System;

public class BakerWorkerController : MonoBehaviour
{
    public float MaxDistanceToBush;
     
    [HideInInspector]
    public bool BushInRange;
    [HideInInspector]
    public Vector3 Target;
    [HideInInspector]
    public Vector3 HomeTarget;

    private AudioSource audioSource;
    private BakeryController controller;
    private FoodBushController targetBush;
    private Animation animationController;
    private NavMeshAgent agent;
    private Vector3 currentTarget;
    private FoodBushController[] bushes;
    private bool isActive = true;
    private bool targetChosen = false;
    private bool hasGathered = false;

    void Awake()
    {
        animationController = GetComponent<Animation>();
        agent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();

        HomeTarget = transform.position;
        bushes = FindObjectsOfType(typeof(FoodBushController)) as FoodBushController[];

        if (bushes.Length == 0)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Bushes in Sight", "There are no Bushes in Sight of this Baker", 1.5f);

        PickTargetBush();
        animationController["Walk"].speed = 1.7f;
    }

    void Update()
    {
        if (!isActive)
            return;

        if (!targetChosen && agent.velocity.sqrMagnitude < 1)
            ChooseTarget();

        ControllAnimation();
    }

    public void SetController(BakeryController controller)
    {
        this.controller = controller;
    }

    private void PlayGatherSound()
    {
        if (audioSource == null)
            return;

        if (!audioSource.isPlaying)
            audioSource.Play();
    }

    private void ControllAnimation()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
            animationController.Play("Walk", PlayMode.StopAll);
        else
        {
            if (Vector3.Distance(transform.position, Target) >= Vector3.Distance(transform.position, HomeTarget))
                animationController.Play("Idle", PlayMode.StopAll);
            else
            {
                PlayGatherSound();
                animationController.Play("Lumbering", PlayMode.StopAll);
            }
        }
    }

    private void ChooseTarget()
    {
        if (Vector3.Distance(transform.position, Target) >= Vector3.Distance(transform.position, HomeTarget))
        {
            currentTarget = Target;
            if (hasGathered)
                controller.AddFood();
        }
        else
        {
            if (targetBush.Destroyed)
            {
                PickTargetBush();
                return;
            }

            currentTarget = HomeTarget;
            targetBush.DecreaseFood((int)(controller.Amount / 2));
            if (!hasGathered)
                hasGathered = true;
        }

        StartCoroutine(StartTravelTimer());
        targetChosen = true;
    }

    private void PickTargetBush()
    {
        if (bushes.Length == 0)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Bushes in Sight", "The Worker has no Bushes in Sight!", 1.2f);
            return;
        }

        Target = Vector3.zero;

        int bushIndex = 0;

        for (int i = 0; i < bushes.Length; i++)
        {
            if (bushes[i] == null)
                continue;

            if (Target == Vector3.zero)
            {
                Target = bushes[i].transform.position;
                targetBush = bushes[i];
                bushIndex = i;
                continue;
            }

            if (Vector3.Distance(HomeTarget, bushes[i].transform.position) < Vector3.Distance(Target, HomeTarget))
            {
                Target = bushes[i].transform.position;
                targetBush = bushes[i];
                bushIndex = i;
            }
        }

        if (Target == Vector3.zero || Target == null)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Bushes in Sight", "The Worker has no Bushes in Sight!", 1.2f);
            SetInactive();
        }

        bushes[bushIndex] = null;

        if (Vector3.Distance(Target, HomeTarget) > MaxDistanceToBush)
        {
            BushInRange = false;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Bushes in Sight", "There is no Bush near the Baker", 1.5f);
        }
        else
            BushInRange = true;

        currentTarget = Target;
    }

    private void SetInactive()
    {
        agent.SetDestination(HomeTarget);
        isActive = false;
    }

    IEnumerator StartTravelTimer()
    {
        yield return new WaitForSeconds(controller.Delay);
        targetChosen = false;
        agent.SetDestination(currentTarget);
    }
}
