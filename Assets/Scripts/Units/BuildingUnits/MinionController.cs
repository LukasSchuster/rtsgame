﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Health))]
public class MinionController : MonoBehaviour, IPoolable
{
    private const int LAST_CHECKPOINT_VIEW_DISTANCE = 45;
    private const int STANDARD_VIEW_DISTANCE = 20;
    private const int DAMAGEMARGINPERCENTAGE = 10;
    private const string EMEMY_BUILDING = "EnemyUnit";

    public float ViewDistance;
    public float AttackDistance;
    public float AttackDmg;
    public float AttackDelay;
    public float MinDistanceToWayPoint = 2f;
    public float TimeToShowHealthBarOnDamaged = 2f;
    public WayPoint CurrentWayPoint;
    public AudioClip AttackSoundClip;

    [HideInInspector]
    public ObjectPool<MinionController> Pool;

    private FogOfWarElement fogOfWarElement;
    private Animation myAnimation;
    private NavMeshAgent agent;
    private Health health;
    private Health TargetHealth;
    private bool attacking = false;
    private bool allive = true;

    private Wave CurrentWave;
    private MapProp mapProp;
    private Sprite mapPropIcon;

    void Awake()
    {
        if (!MinionUpgradeController.Minions.Contains(this))
            MinionUpgradeController.Minions.Add(this);

        mapProp = GetComponent<MapProp>();
        mapPropIcon = mapProp.IconTexture;
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<Health>();
        myAnimation = GetComponent<Animation>();
    }

    void Start()
    {
        fogOfWarElement = FogOfWarManager.AddElement(health, 0.75f);

        CurrentWayPoint = WayPointManager.GetClosestWayPoint(transform.position);

        ViewDistance = STANDARD_VIEW_DISTANCE;

        if (gameObject.activeSelf)
            agent.SetDestination(CurrentWayPoint.Position);

        if (mapProp != null)
            mapProp.EnableIcon();

        health.OnDeath += Health_OnDeath;

        UnitManager.Manager.AddFriendlyUnit(health);
    }

    private void Health_OnDeath()
    {
        if (!allive) return;

        if (fogOfWarElement != null)
            if (fogOfWarElement.ParentHealth != null) Destroy(fogOfWarElement.gameObject);

        if (mapProp != null)
            mapProp.DisableIcon();

        UnitManager.Manager.RemoveFriendlyUnit(health);

        CurrentWave.Decrement();

        Pool.StoreObject(gameObject);

        allive = false;
    }

    void Update()
    {
        ControllAnimations();
        CheckforWaypointReached();

        if (TargetHealth == null || !TargetHealth.Allive)
        {
            attacking = false;
            CheckForEnemysInSight();

            if (TargetHealth == null || TargetHealth.CurrentHealth <= 0)
                agent.SetDestination(CurrentWayPoint.Position);
        }
        else
            AttackTarget();
    }

    public void SetWave(Wave wave)
    {
        CurrentWave = wave;
    }

    private void AttackTarget()
    {
        if (Vector3.Distance(transform.position, TargetHealth.transform.position) - TargetHealth.ObjectRadius <= AttackDistance)
        {
            if (!attacking)
            {
                StartCoroutine(AttacKTimer());
                attacking = true;
            }
            agent.velocity = Vector3.zero;
        }
        else
        {
            attacking = false;
            agent.SetDestination(TargetHealth.transform.position);
        }
    }

    private void CheckForEnemysInSight()
    {
        float shortestDistance = 0;
        Health enemy = null;

        foreach (Health enemyHealth in UnitManager.Manager.EnemyUnits)
        {
            float currentDistance = Vector3.Distance(enemyHealth.transform.position, transform.position);

            if (currentDistance >= ViewDistance)
                continue;
            else
            {
                if (shortestDistance == 0 || shortestDistance > currentDistance)
                {
                    shortestDistance = currentDistance;
                    enemy = enemyHealth;
                }
            }
        }

        if (enemy != null)
            TargetHealth = enemy;
        else
        {
            float minDistance = 0;
            Health minDistanceHealth = null;

            for (int i = 0; i < GameManager.Manager.GetEnemyMinionControllerList().Count; i++)
            {
                if (GameManager.Manager.GetEnemyMinionControllerList()[i] != null)
                {
                    if (Vector3.Distance(GameManager.Manager.GetEnemyMinionControllerList()[i].transform.position, transform.position) <= ViewDistance)
                    {
                        if (Vector3.Distance(GameManager.Manager.GetEnemyMinionControllerList()[i].transform.position, transform.position) < minDistance || minDistanceHealth == null)
                        {
                            minDistanceHealth = GameManager.Manager.GetEnemyMinionControllerList()[i].gameObject.GetComponent<Health>();
                            minDistance = Vector3.Distance(GameManager.Manager.GetEnemyMinionControllerList()[i].transform.position, transform.position);
                        }
                    }
                }
            }

            if (minDistanceHealth != null)
                TargetHealth = minDistanceHealth;
        }
    }

    private void CheckforWaypointReached()
    {
        if (Vector3.Distance(transform.position, CurrentWayPoint.Position) <= MinDistanceToWayPoint)
        {
            if (CurrentWayPoint.NextWayPoint != null)
            {
                CurrentWayPoint = CurrentWayPoint.NextWayPoint;
                agent.SetDestination(CurrentWayPoint.Position);
            }
            else
            {
                // Increase Sight
                ViewDistance = LAST_CHECKPOINT_VIEW_DISTANCE;
            }
        }
    }

    private void ControllAnimations()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
            myAnimation.Play("Walk", PlayMode.StopAll);
        else
        {

            if (attacking)
                myAnimation.Play("Attack1h1", PlayMode.StopAll);
            else
                myAnimation.Play("Idle1", PlayMode.StopAll);
        }
    }

    IEnumerator AttacKTimer()
    {
        yield return new WaitForSeconds(AttackDelay);

        if (gameObject != null)
        {
            if (attacking)
            {
                if (AttackSoundClip != null)
                    AudioSource.PlayClipAtPoint(AttackSoundClip, transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);

                if (TargetHealth != null)
                {
                    float damageMargin = AttackDmg / 100 * DAMAGEMARGINPERCENTAGE;
                    TargetHealth.DecreaseHealth(UnityEngine.Random.Range(AttackDmg - damageMargin, AttackDmg + damageMargin));
                }

                StartCoroutine(AttacKTimer());
            }
        }
    }

    public void ResetObject()
    {
        //mapProp = mapProp.AddMapProp(mapPropIcon, transform).GetComponent<MapProp>();
        allive = true;
        health.Reset();
        Start();
    }

    public void SetPool(ObjectPool<MinionController> pool)
    {
        Pool = pool;
    }
}
