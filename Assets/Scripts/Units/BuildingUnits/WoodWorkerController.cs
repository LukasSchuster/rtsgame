﻿using UnityEngine;
using System.Collections;

public class WoodWorkerController : MonoBehaviour
{
    public float MaxDistanceToTree;

    public AudioSource ChopSound;

    [HideInInspector]
    public bool TreeInRange;
    [HideInInspector]
    public Vector3 Target;
    [HideInInspector]
    public Vector3 HomeTarget;

    private bool isActive = true;
    private bool targetChosen = false;
    private bool hasGathered = false;
    private TreeController TargetTree;
    private Animation animationController;
    private Vector3 currentTarget;
    private WoodCutterController controller;
    private NavMeshAgent agent;
    private TreeController[] Trees;

    void Awake()
    {
        animationController = GetComponent<Animation>();
        agent = GetComponent<NavMeshAgent>();

        HomeTarget = transform.position;
        Trees = FindObjectsOfType(typeof(TreeController)) as TreeController[];

        if (Trees.Length == 0)
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Trees in Sight", "The Woodworker has no trees in Sight!", 2f);

        PickTargetTree();
        animationController["Walk"].speed = 1.7f;
    }

    void Update()
    {
        if (!isActive)
            return;

        if (!targetChosen && agent.velocity.sqrMagnitude < 1)
            ChooseTarget();

        ControllAnimation();
    }

    private void PlayChopSound()
    {
        if (ChopSound != null)
        {
            if (!ChopSound.isPlaying)
                ChopSound.Play();
        }
    }

    public void PickTargetTree()
    {
        Target = Vector3.zero;

        int treeIndex = 0;

        for (int i = 0; i < Trees.Length; i++)
        {
            if (Trees[i] == null)
                continue;

            if (Target == Vector3.zero)
            {
                Target = Trees[i].transform.position;
                TargetTree = Trees[i];
                treeIndex = i;
                continue;
            }

            if (Vector3.Distance(HomeTarget, Trees[i].transform.position) < Vector3.Distance(Target, HomeTarget))
            {
                Target = Trees[i].transform.position;
                TargetTree = Trees[i];
                treeIndex = i;
            }
        }

        if (Target == Vector3.zero || Target == null)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Trees in Sight", "The Woodworker has no trees in Sight!", 2f);
            SetInactive();
        }

        Trees[treeIndex] = null;

        if (Vector3.Distance(Target, HomeTarget) > MaxDistanceToTree)
        {
            TreeInRange = false;
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "No Tree in Sight", "There is no Tree near the Woodcutter", 2f);
        }
        else
            TreeInRange = true;

        currentTarget = Target;
    }

    private void ChooseTarget()
    {
        if (Vector3.Distance(transform.position, Target) >= Vector3.Distance(transform.position, HomeTarget))
        {
            currentTarget = Target;
            if (hasGathered)
                controller.AddWood();
        }
        else
        {
            if (TargetTree.Destroyed)
            {
                PickTargetTree();
                return;
            }

            currentTarget = HomeTarget;
            TargetTree.DecreaseWood((int)(controller.Amount / 2));
            if (!hasGathered)
                hasGathered = true;
        }

        StartCoroutine(startTravelTimer());
        targetChosen = true;
    }

    private void ControllAnimation()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
            animationController.Play("Walk", PlayMode.StopAll);
        else
        {
            if (Vector3.Distance(transform.position, Target) >= Vector3.Distance(transform.position, HomeTarget))
                animationController.Play("Idle", PlayMode.StopAll);
            else
            {
                animationController.Play("Lumbering", PlayMode.StopAll);
                transform.LookAt(Target);
                PlayChopSound();
            }
        }
    }

    private void SetInactive()
    {
        agent.SetDestination(HomeTarget);
        isActive = false;
    }

    IEnumerator startTravelTimer()
    {
        yield return new WaitForSeconds(controller.Delay);
        targetChosen = false;
        agent.SetDestination(currentTarget);
    }

    public void SetController(WoodCutterController controller)
    {
        this.controller = controller;
    }
}
