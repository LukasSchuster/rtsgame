﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioPlayer : MonoBehaviour
{
    public bool OnAwakeAndLoop;

    public List<AudioClip> Clips = new List<AudioClip>();

    public float MinDelay;
    public float MaxDelay;

    private float delay;

    void Start()
    {
        if (OnAwakeAndLoop)
        {
            delay = Random.Range(MinDelay, MaxDelay);
            StartCoroutine(DelayTimer());
        }
    }

    public void PlayAudio()
    {
        AudioSource.PlayClipAtPoint(GetRandomAudioClip(), transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);
    }

    private AudioClip GetRandomAudioClip()
    {
        int i = Random.Range(0, Clips.Count);
        return Clips[i];
    }

    IEnumerator DelayTimer()
    {
        yield return new WaitForSeconds(delay);

        if (gameObject != null)
        {
            AudioSource.PlayClipAtPoint(GetRandomAudioClip(), transform.position, GameManager.Manager.GetAudioManager().EffectsVolume);
            delay = Random.Range(MinDelay, MaxDelay);
            StartCoroutine(DelayTimer());
        }
    }
}
