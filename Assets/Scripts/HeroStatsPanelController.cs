﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeroStatsPanelController : MonoBehaviour
{
    private HeroController hero;

    [Header("UI")]
    public Text SpeedText;
    public Text DamageText;
    public Text FireBallRangeText;

    void Start()
    {
        hero = GameManager.Manager.GetHeroController();
    }

    void Update()
    {
        DamageText.text = string.Format("Damage: {0}", hero.Damage);
        FireBallRangeText.text = string.Format("Fire Range: {0}", hero.Range);
    }
}
