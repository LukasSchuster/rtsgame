﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FogOfWarElement : MonoBehaviour
{
    public Health ParentHealth;

    private Image image;

    void Awake()
    {
        image = GetComponent<Image>();
    }

    void Start()
    {
        transform.Rotate(90, 0, 0);
    }

    void Update()
    {
        Vector3 tempPos = ParentHealth.transform.position;
        tempPos.y = GameManager.Manager.GetFogOfWarPlane().transform.position.y;
        transform.position = tempPos;
    }

    public void SetValues(Health health, float scale)
    {
        image.rectTransform.localScale = new Vector3(scale, scale, scale);

        ParentHealth = health;
        ParentHealth.OnDeath += ParentHealth_OnDeath;
    }

    private void ParentHealth_OnDeath()
    {
        FogOfWarManager.RemoveElement(this);
    }
}
