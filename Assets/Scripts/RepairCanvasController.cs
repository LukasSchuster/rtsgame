﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RepairCanvasController : MonoBehaviour
{
    private const int TARGET_RAY_LENGHT = 50;
    private const float HEALTH_LERP_SPEED = 1.5f;

    public LayerMask LayerMask;
    public Image Image;
    public Image HealthBarImage;
    public Text Name, Description, Cost, RepairText, HealthbarText;
    public int GpCostPerHealthPoint;

    private Health health;

    void Update()
    {
        int x = Screen.width / 2;
        int y = Screen.height / 2;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y));
        RaycastHit info;

        if (Physics.Raycast(ray, out info, TARGET_RAY_LENGHT, LayerMask))
            transform.position = info.point;

        transform.LookAt(transform.position - info.normal);

        if (health != null)
        {
            float wantedHealthValue = (health.CurrentHealth / health.MaxHealth);
            HealthBarImage.fillAmount = Mathf.Lerp(HealthBarImage.fillAmount, wantedHealthValue, HEALTH_LERP_SPEED * Time.deltaTime);
            HealthbarText.text = string.Format("{0}/{1}", health.MaxHealth, health.CurrentHealth);

            if (health.CurrentHealth != health.MaxHealth)
                Cost.text = string.Format("{0}", (health.MaxHealth - health.CurrentHealth) * GpCostPerHealthPoint);
            else
                Cost.text = "0";
        }
    }

    public void SetValues(Sprite image, string name, string description, string cost, string repairText, Health health, int gpCost)
    {
        this.Image.sprite = image;
        this.health = health;
        this.Name.text = name;
        this.Description.text = description;
        this.Cost.text = cost;
        this.RepairText.text = repairText;
        this.GpCostPerHealthPoint = gpCost;
    }

    public void SetValues(Sprite image, string name, string description, string cost, string repairText)
    {
        this.Image.sprite = image;
        this.Name.text = name;
        this.Description.text = description;
        this.Cost.text = cost;
        this.RepairText.text = repairText;
    }
}
