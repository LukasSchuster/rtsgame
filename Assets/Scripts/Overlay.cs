﻿using UnityEngine;
using System.Collections;

public class Overlay
{
    private const int FADEOUT_STEPS = 30;

    private Material material;
    private Vector3 position;
    private float duration;
    private float moveDuration;
    private bool randomOriantation;
    private bool shrinkOnDestroy;

    private Vector3 direction;
    private float speed;

    private float timer;

    public Overlay(Material mat, Vector3 position, float duration, bool randomOriantation, bool shrinkOnDestroy)
    {
        this.material = mat;
        this.duration = duration;
        this.position = position;
        this.randomOriantation = randomOriantation;
        this.shrinkOnDestroy = shrinkOnDestroy;
    }

    public void Create()
    {
        if (GameManager.Manager.OverlayProjectorPrefab != null)
        {
            GameObject overlayGameObject = GameManager.Instantiate(GameManager.Manager.OverlayProjectorPrefab, position, GameManager.Manager.OverlayProjectorPrefab.transform.rotation) as GameObject;
            Projector projector = overlayGameObject.GetComponent<Projector>();
            AutoRemove remove = overlayGameObject.GetComponent<AutoRemove>();

            remove.LifeTime = duration;
            projector.material = material;

            if (randomOriantation)
                overlayGameObject.transform.Rotate(0, Random.Range(0, 360), 0, Space.World);
        }
    }

    public void CreateMoving(Vector3 direction, float speed, float moveDuration)
    {
        this.direction = direction;
        this.speed = speed;
        this.moveDuration = moveDuration;

        if (GameManager.Manager.OverlayProjectorPrefab != null)
        {
            GameObject overlayGameObject = GameManager.Instantiate(GameManager.Manager.OverlayProjectorPrefab, position, GameManager.Manager.OverlayProjectorPrefab.transform.rotation) as GameObject;
            Projector projector = overlayGameObject.GetComponent<Projector>();
            AutoRemove remove = overlayGameObject.GetComponent<AutoRemove>();

            remove.LifeTime = shrinkOnDestroy ? duration * 2f : duration;
            projector.material = material;

            if (randomOriantation)
                overlayGameObject.transform.Rotate(0, Random.Range(0, 360), 0, Space.World);

            GameManager.Manager.StartCoroutine(Update(overlayGameObject));
        }
    }

    IEnumerator Update(GameObject objectToMove)
    {
        yield return new WaitForFixedUpdate();

        if (objectToMove != null)
        {
            timer += Time.deltaTime;

            if (timer <= moveDuration)
                objectToMove.transform.Translate(direction * speed, Space.World);
            else if (timer >= duration)
                objectToMove.transform.Translate(-direction * speed * 2, Space.World);

            GameManager.Manager.StartCoroutine(Update(objectToMove));
        }
    }

    IEnumerator FadeOutUpdate(Material toFade)
    {
        yield return new WaitForSeconds(duration / FADEOUT_STEPS);

        if (toFade != null)
        {
            Color tempColor = toFade.color;
            tempColor.a -= (float)255 / (float)FADEOUT_STEPS;
            toFade.color = tempColor;

            GameManager.Manager.StartCoroutine(FadeOutUpdate(toFade));
        }
    }
}
