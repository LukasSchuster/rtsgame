﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public Slider EffectsSlider;
    public Slider MusicSlider;

    public float EffectsVolume = 1;
    public float MusicVolume = 1;

    private List<AudioSource> effectsAudioSources = new List<AudioSource>();
    private List<AudioSource> musicAudioSources = new List<AudioSource>();

    void Start()
    {
        EffectsSlider.onValueChanged.AddListener(delegate { OnEffectsSliderChanged(); });
        MusicSlider.onValueChanged.AddListener(delegate { OnMusicSliderChanged(); });
    }

    public void AddAudiosources(AudioSource[] sources, bool effect)
    {
        if (effect)
            effectsAudioSources.AddRange(sources);
        else
            musicAudioSources.AddRange(sources);
    }

    public void OnEffectsSliderChanged()
    {
        EffectsVolume = EffectsSlider.value;
        GameManager.Manager.GameSettings.EffectsVolume = EffectsSlider.value;

        for (int i = 0; i < effectsAudioSources.Count; i++)
        {
            if (effectsAudioSources[i] != null)
                effectsAudioSources[i].volume = EffectsVolume;
        }

        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Effects Volume: {0}", (int)(EffectsVolume * 100)), "", 1.5f);
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_EFFECTS_VOLUME, EffectsVolume);
    }

    public void OnMusicSliderChanged()
    {
        MusicVolume = MusicSlider.value;
        GameManager.Manager.GameSettings.MusicVolume = MusicSlider.value;

        for (int i = 0; i < musicAudioSources.Count; i++)
        {
            if (musicAudioSources[i] != null)
                musicAudioSources[i].volume = MusicVolume;
        }

        GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Hint, string.Format("Music Volume: {0}", (int)(MusicVolume * 100)), "", 1.5f);
        GameManager.Manager.GameSettings.SaveUserPrefSetting(GameManager.Manager.GameSettings.SAVE_MUSIC_VOLUME, MusicVolume);
    }
}
