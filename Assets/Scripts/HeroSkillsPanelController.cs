﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HeroSkillsPanelController : MonoBehaviour
{
    public GameObject CurrentSkillsPanel;
    public GameObject SkillsPanel;
    public Button BackButton;

    public GameObject SkillBuyButtonPrefab;

    public Color NotBoughtColor;
    public GameObject FakeSkillImage;

    void Start()
    {
        foreach (Skill skill in GameManager.Manager.GetHeroSkills())
        {
            GameObject temp = Instantiate(SkillBuyButtonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            temp.transform.SetParent(SkillsPanel.transform, false);

            temp.GetComponent<SkillBuyButtonBehavior>().SetValues(skill, skill.Image, skill.Name, skill.Description, skill.CoolDownTime, skill.GodPointsCost, NotBoughtColor, SkillsPanel, CurrentSkillsPanel);
            UIHint tempUIHint = temp.AddComponent<UIHint>();
            tempUIHint.BlockRays = true;
            tempUIHint.ShowHint = false;
        }

        BackButton.onClick.AddListener(new UnityAction(OnBackButtonClicked));

        GameManager.Manager.ActivateHeroSkillsPanel(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(GameManager.Manager.GameSettings.KeyHeroSkills) || Input.GetKeyDown(GameManager.Manager.GameSettings.KeyBuildSkills))
            OnDeactivated();
    }

    void OnEnable()
    {
        GameManager.Manager.GetSkillHeroCamera().SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        GameManager.Manager.GetHeroController().IsShowingSkillBuyPanel = true;
    }

    void OnDisable()
    {
        GameManager.Manager.GetSkillHeroCamera().SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameManager.Manager.GetHeroController().IsShowingSkillBuyPanel = false;
    }

    public void OnBackButtonClicked()
    {
        GameManager.Manager.GetGameController().ChangeModeTo(GameManager.Manager.GetGameController().lastMode.ToString());

        GameManager.Manager.GetSkillPanelController().Controller_OnHeroActivated();

        OnDeactivated();
    }

    public void OnDeactivated()
    {
        GameManager.Manager.GetSkillPanelController().Controller_OnHeroActivated();
        GameManager.Manager.ActivateHeroSkillsPanel(false);
        GameManager.Manager.HideSkillToolTip();
    }
}
