﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Skill : MonoBehaviour
{
    private const int DAMAGEPERCENTMARGIN = 10;

    [Header("Effect")]
    public GameObject ParticleEffect;
    public float ParticleLifeTime;

    [Header("Stats")]
    public float Damage;
    public float Radius;
    public int GodPointsCost;
    public float CoolDownTime;
    public int GoldCost;

    [Header("UI")]
    public Sprite Image;
    public string Name;
    public string Description;

    [Header("Sound")]
    public AudioClip SoundClip;

    [Header("Lighting")]
    public bool EmitLight;
    public bool LightFadeOut;
    public bool Shadows;
    public Color LightColor;
    public int LightRange = 10;
    public float LightIntensity = 1;
    public float LightningTime = 0.5f;
    public int FadeSteps;

    [Header("Effect")]
    public float EffectDelay;
    public enum EffectMode { None = 1, StunEffect = 2, SlowEffect = 3, Poison = 4 }
    public EffectMode Effect = EffectMode.None;
    public float EffectDamage;

    private SkillEffect skillEffect;
    [HideInInspector]
    public bool canFireSkill = true;

    public bool OnActivated()
    {
        if (!canFireSkill)
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Error, "Skill not Ready!", "", 1.2f);
            return false;
        }

        if (GameManager.Manager.GetRessources().GodPoints >= GodPointsCost)
            GameManager.Manager.GetRessources().DecreaseGodPoints(GodPointsCost);
        else
        {
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not Enough Mana", "", 1f);
            return false;
        }

        Vector3 position = Utilitys.GiveMousePositionInWorldOnFloor();
        position.y += 0.5f;

        if (SoundClip != null)
            AudioSource.PlayClipAtPoint(SoundClip, position, GameManager.Manager.GetAudioManager().EffectsVolume);

        GameObject ParticleGameObject = Instantiate(ParticleEffect, position, Quaternion.identity) as GameObject;
        GameManager.Manager.StartCoroutine(ParticleRemoveTimer(ParticleGameObject));

        if (EmitLight)
            AdjustLight(ParticleGameObject);

        List<Health> tempEnemyList = new List<Health>();

        foreach (var enemyHealth in UnitManager.Manager.EnemyUnits)
        {
            if (Vector3.Distance(position, enemyHealth.transform.position) <= Radius)
            {
                float damageMargin = Damage / 100 * DAMAGEPERCENTMARGIN;
                enemyHealth.DecreaseHealth(Random.Range(Damage - damageMargin, Damage + damageMargin));
                tempEnemyList.Add(enemyHealth);
            }
        }

        if (Effect != EffectMode.None)
            ChooseEffect(tempEnemyList);

        ActivateSkillCoolDown();

        return true;
    }

    private void ActivateSkillCoolDown()
    {
        int index = GameManager.Manager.GetHeroController().Skills.IndexOf(this);
        GameManager.Manager.GetSkillPanelController().skillButtonSliders[index].gameObject.SetActive(true);
        GameManager.Manager.GetSkillPanelController().skillButtonSliders[index].maxValue = CoolDownTime;
        GameManager.Manager.GetSkillPanelController().skillButtonSliders[index].value = CoolDownTime;
        GameManager.Manager.StartCoroutine(SkillCoolDownTimer(GameManager.Manager.GetSkillPanelController().skillButtonSliders[index]));
        canFireSkill = false;
    }

    private void ChooseEffect(List<Health> enemyList)
    {
        switch (Effect)
        {
            case EffectMode.StunEffect:
                skillEffect = new StunEffect(EffectDamage, ParticleLifeTime, Radius, EffectDelay, enemyList);
                break;
            case EffectMode.SlowEffect:
                skillEffect = new SlowEffect(EffectDamage, ParticleLifeTime, Radius, EffectDelay, enemyList);
                break;
            case EffectMode.Poison:
                skillEffect = new PoisonEffect(EffectDamage, ParticleLifeTime, Radius, EffectDelay, enemyList);
                break;
        }
    }

    private void AdjustLight(GameObject gameObject)
    {
        Light particleLight = gameObject.AddComponent<Light>();
        particleLight.color = LightColor;
        particleLight.range = LightRange;
        particleLight.intensity = LightIntensity;

        if (Shadows)
            particleLight.shadows = LightShadows.Hard;

        GameManager.Manager.StartCoroutine(LightTimer(particleLight));

        if (LightFadeOut)
            GameManager.Manager.StartCoroutine(LightFadeTimer(particleLight));
    }

    IEnumerator SkillEffectUpdate()
    {
        yield return new WaitForFixedUpdate();

    }

    IEnumerator LightFadeTimer(Light toController)
    {
        yield return new WaitForSeconds(LightningTime / FadeSteps);

        if (toController != null)
        {
            toController.intensity -= LightIntensity / FadeSteps;
            GameManager.Manager.StartCoroutine(LightFadeTimer(toController));
        }
    }

    IEnumerator LightTimer(Light toControll)
    {
        yield return new WaitForSeconds(LightningTime);

        if (toControll != null)
            toControll.intensity = 0;
    }

    IEnumerator ParticleRemoveTimer(GameObject gameObjectToRemove)
    {
        yield return new WaitForSeconds(ParticleLifeTime);

        Destroy(gameObjectToRemove);
    }

    IEnumerator SkillCoolDownTimer(Slider slider)
    {
        yield return new WaitForFixedUpdate();

        slider.value -= Time.deltaTime;

        if (slider.value <= slider.minValue)
        {
            canFireSkill = true;
            slider.gameObject.SetActive(false);
        }
        else
            GameManager.Manager.StartCoroutine(SkillCoolDownTimer(slider));
    }
}
