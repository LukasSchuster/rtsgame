﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class BuildingCanvasController : MonoBehaviour
{
    private const int FARMER_MAX_OF_TYPE = 2;
    private const int TOWER_MAX_OF_TYPE = 6;
    private const int CUTTER_MAX_OF_TYPE = 3;
    private const int BAKER_MAX_OF_TYPE = 3;
    private const int SUBHERO_MAX_OF_TYPE = 1;
    private const int TEMPLE_MAX_OF_TYPE = 2;
    private const int MINIONSPAWNER_MAX_OF_TYPE = 3;

    public Button ButtonToOpenBuildPanel;
    public GameObject BuildPanel;
    public GameObject InnerBuildPanel;

    public GameObject BuildingImagePrefab;

    [Header("Max Count Texts")]
    public Text FarmerMaxCountText;
    public Text TowerMaxCountText;
    public Text BakerMaxCountText;
    public Text CutterMaxCountText;
    public Text SubHeroMaxCountText;
    public Text TempleMaxCountText;
    public Text MinionSpawnerMaxCountText;

    [Header("Building Overlays")]
    public GameObject FarmerOverlay;
    public GameObject TowerOverlay;
    public GameObject BakerOverlay;
    public GameObject CutterOverlay;
    public GameObject SubHeroOverlay;
    public GameObject TempleOverlay;
    public GameObject MinionSpawnerOverlay;

    private List<Sprite> buildingImages = new List<Sprite>();
    private StrategieCameraController controller;
    private Ressources ressources;
    private Animator buildPanelAnimator;

    [HideInInspector]
    public bool BuildPanelShowing = false;

    void Awake()
    {
        controller = Camera.main.GetComponent<StrategieCameraController>();
        ressources = controller.GetComponent<Ressources>();
        buildPanelAnimator = BuildPanel.GetComponent<Animator>();
    }

    void Start()
    {
        ButtonToOpenBuildPanel.onClick.AddListener(() => OnBuildButtonClick());

        foreach (Building building in GameManager.Buildings)
        {
            if (building.Image == null)
                continue;

            if (!buildingImages.Contains(building.Image))
                buildingImages.Add(building.Image);
        }

        UpdateCanvas();
    }

    void Update()
    {
        if (Input.GetKeyDown(GameManager.Manager.GameSettings.KeyBuildMenu) && GameManager.Manager.GetGameController().CamMode == StrategieCameraController.CameraMode.SelectMode)
        {
            if (BuildPanelShowing) OnBuildButtonDeClicked();
            else OnBuildButtonClick();
        }
    }

    public void UpdateCanvas()
    {
        int farmerCount = 0;
        int bakerCount = 0;
        int cutterCount = 0;
        int subHeroCount = 0;
        int templeCount = 0;
        int minionSpawnerCount = 0;
        int towerCount = 0;

        #region Building For Loop

        for (int i = 0; i < GameManager.Buildings.Count; i++)
        {
            if (GameManager.Buildings[i] is CoinFarmerController)
            {
                farmerCount++;
                continue;
            }

            if (GameManager.Buildings[i] is BakeryController)
            {
                bakerCount++;
                continue;
            }

            if (GameManager.Buildings[i] is WoodCutterController)
            {
                cutterCount++;
                continue;
            }

            if (GameManager.Buildings[i] is SubheroesCreatorController)
            {
                subHeroCount++;
                continue;
            }

            if (GameManager.Buildings[i] is TempleController)
            {
                templeCount++;
                continue;
            }

            if (GameManager.Buildings[i] is MinionSpawnerController)
            {
                minionSpawnerCount++;
                continue;
            }

            if (GameManager.Buildings[i] is TowerController)
            {
                towerCount++;
                continue;
            }
        }

        #endregion

        SetMaxCountText(FarmerMaxCountText, farmerCount, FARMER_MAX_OF_TYPE);
        SetMaxCountText(BakerMaxCountText, bakerCount, BAKER_MAX_OF_TYPE);
        SetMaxCountText(CutterMaxCountText, cutterCount, CUTTER_MAX_OF_TYPE);
        SetMaxCountText(SubHeroMaxCountText, subHeroCount, SUBHERO_MAX_OF_TYPE);
        SetMaxCountText(TempleMaxCountText, templeCount, TEMPLE_MAX_OF_TYPE);
        SetMaxCountText(MinionSpawnerMaxCountText, minionSpawnerCount, MINIONSPAWNER_MAX_OF_TYPE);
        SetMaxCountText(TowerMaxCountText, towerCount, TOWER_MAX_OF_TYPE);

        if (farmerCount >= FARMER_MAX_OF_TYPE) FarmerOverlay.SetActive(true);
        else FarmerOverlay.SetActive(false);

        if (towerCount >= TOWER_MAX_OF_TYPE) TowerOverlay.SetActive(true);
        else TowerOverlay.SetActive(false);

        if (bakerCount >= BAKER_MAX_OF_TYPE) BakerOverlay.SetActive(true);
        else BakerOverlay.SetActive(false);

        if (cutterCount >= CUTTER_MAX_OF_TYPE) CutterOverlay.SetActive(true);
        else CutterOverlay.SetActive(false);

        if (subHeroCount >= SUBHERO_MAX_OF_TYPE) SubHeroOverlay.SetActive(true);
        else SubHeroOverlay.SetActive(false);

        if (templeCount >= TEMPLE_MAX_OF_TYPE) TempleOverlay.SetActive(true);
        else TempleOverlay.SetActive(false);

        if (minionSpawnerCount >= MINIONSPAWNER_MAX_OF_TYPE) MinionSpawnerOverlay.SetActive(true);
        else MinionSpawnerOverlay.SetActive(false);
    }

    public void HideCanvas()
    {
        OnBuildButtonDeClicked();
    }

    private void SetMaxCountText(Text text, int count, int MaxCount)
    {
        text.text = string.Format("{0} / {1}", count, MaxCount);
    }

    public void OnBuildButtonClick()
    {
        UpdateCanvas();

        if (!BuildPanelShowing)
        {
            BuildPanelShowing = true;
            buildPanelAnimator.SetBool("show", true);
        }
        else
        {
            OnBuildButtonDeClicked();
            return;
        }

        controller.FreezeMovement = BuildPanel.activeSelf == true ? true : false;
        controller.FreezeSelection = BuildPanel.activeSelf == true ? true : false;

        GameManager.Manager.HideMinionUpgradePanel();
        GameManager.Manager.HideUpgradePanel();
        GameManager.Manager.HideUnitCreatePanel();
        GameManager.Manager.HideToolTip();
    }

    public void OnBuildButtonDeClicked()
    {
        BuildPanelShowing = false;
        buildPanelAnimator.SetBool("show", false);

        controller.FreezeMovement = false;
        controller.FreezeSelection = false;
        GameManager.Manager.HideToolTip();
    }

    public void ClickedBuilding(string name)
    {
        OnBuildButtonDeClicked();

        Building tempBuilding = GameManager.Manager.GetBuilding(name, false);

        if (tempBuilding is WallController)
        {
            controller.ChangeModeTo("BuildWallMode");
            return;
        }

        if (ressources.Money >= tempBuilding.GoldCost && ressources.Wood >= tempBuilding.WoodCost && ressources.Food >= tempBuilding.FoodCost)
        {
            if (IsBuildingMaxed(tempBuilding))
            {
                GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Max Buildings Placed!", "You allready have build the maximum buildings of this type.", 1.5f);
                return;
            }

            ressources.DecreaseFood(tempBuilding.FoodCost);
            ressources.DecreaseMoney(tempBuilding.GoldCost);
            ressources.DecreaseWood(tempBuilding.WoodCost);

            controller.ChangeModeTo("BuildMode");

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) { }

            controller.BuildingToPlace = Instantiate(GameManager.Manager.GetBuildingGameObject(name), hit.point, transform.rotation) as GameObject;

            Utilitys.ChangeColorOfGameObjectAndChilds(controller.BuildingToPlace, controller.CanPlaceBuildingColor);
        }
        else
            GameManager.Manager.ShowMessageBox(GameManager.MessageBoxMode.Warning, "Not enough Ressources!", "You dont have Enough Ressources to build this.", 1.5f);
    }

    private bool IsBuildingMaxed(Building building)
    {
        int maxCount = 0;
        int count = 0;

        bool isBaker = false;
        bool isFarmer = false;
        bool isCutter = false;
        bool isMinionSpawner = false;
        bool isTemple = false;
        bool isSubheroSpawner = false;

        if (building is BakeryController)
        {
            maxCount = BAKER_MAX_OF_TYPE;
            isBaker = true;
        }
        else if (building is CoinFarmerController)
        {
            maxCount = FARMER_MAX_OF_TYPE;
            isFarmer = true;
        }
        else if (building is MinionSpawnerController)
        {
            maxCount = MINIONSPAWNER_MAX_OF_TYPE;
            isMinionSpawner = true;
        }
        else if (building is SubheroesCreatorController)
        {
            maxCount = SUBHERO_MAX_OF_TYPE;
            isSubheroSpawner = true;
        }
        else if (building is TempleController)
        {
            maxCount = TEMPLE_MAX_OF_TYPE;
            isTemple = true;
        }
        else if (building is WoodCutterController)
        {
            maxCount = CUTTER_MAX_OF_TYPE;
            isCutter = true;
        }
        else return false;

        for (int i = 0; i < GameManager.Buildings.Count; i++)
        {
            if (GameManager.Buildings[i] is BakeryController && isBaker) count++;
            else if (GameManager.Buildings[i] is CoinFarmerController && isFarmer) count++;
            else if (GameManager.Buildings[i] is MinionSpawnerController && isMinionSpawner) count++;
            else if (GameManager.Buildings[i] is SubheroesCreatorController && isSubheroSpawner) count++;
            else if (GameManager.Buildings[i] is TempleController && isTemple) count++;
            else if (GameManager.Buildings[i] is WoodCutterController && isCutter) count++;
        }

        if (count >= maxCount) return true;
        else return false;
    }

    public void ShowToolTipFor(string name)
    {
        Building tempBuilding = GameManager.Manager.GetBuilding(name, true);

        GameManager.Manager.ShowToolTip(tempBuilding.Image, name, tempBuilding.Description, tempBuilding.GoldCost, tempBuilding.WoodCost, tempBuilding.FoodCost);
    }
}
