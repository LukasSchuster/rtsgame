﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;

public class CameraFogDisabler : MonoBehaviour
{
    private bool active;
    private WeatherController wController;
    private GlobalFog globalFog;

    void Start()
    {
        wController = GameManager.Manager.GetWeatherController();
        globalFog = Camera.main.GetComponent<GlobalFog>();

        if (wController.WeatherMode == WeatherController.WeatherAppMode.Sunny) active = false;
        else active = true;
    }

    void OnPreRender()
    {
        if (active)
        {
            RenderSettings.fog = false;
            globalFog.enabled = false;
        }
    }
    void OnPostRender()
    {
        if (active)
        {
            RenderSettings.fog = true;
            globalFog.enabled = true;
        }
    }
}
