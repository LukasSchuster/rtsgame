﻿using UnityEngine;
using System.Collections;

public class HealthHitInfo
{
    public enum HitType { Player = 1, Minion = 2, Building = 3}
    public HitType Type { get; private set; }
    public bool IsDead { get; private set; }

    public HealthHitInfo() { }

    public HealthHitInfo(HitType type, bool isDead)
    {
        this.Type = type;
        this.IsDead = isDead;
    }

    public void SetType(HitType type)
    {
        this.Type = type;
    }

    public void SetIsDead(bool isDead)
    {
        this.IsDead = isDead;
    }
}
