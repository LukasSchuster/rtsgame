﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class ToolTipIUElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Sprite Image;
    public string Name;
    public string Description;

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.ShowToolTip(Image, Name, Description, 0, 0, 0);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.HideToolTip();
    }
}
