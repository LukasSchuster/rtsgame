﻿using UnityEngine;
using System.Collections;

public class VillagarController : MonoBehaviour
{
    private const float MINDISTANCETOTARGET = 7f;
    private const float TARGETREACHEDTIME = 2f;

    private const float MAXSCALE = 2.2f;
    private const float MINSCALE = 1.5f;

    private NavMeshAgent agent;
    private Animation myAnimation;
    private Vector3 target;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        myAnimation = GetComponent<Animation>();
    }

    void Start()
    {
        SetScale();

        target = ChooseTarget();
        agent.SetDestination(target);
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, target) <= MINDISTANCETOTARGET)
        {
            agent.SetDestination(transform.position);
            StartCoroutine(TargetReachedTimer());
        }

        ControllAnimation();
    }

    private void SetScale()
    {
        float scale = Random.Range(MINSCALE, MAXSCALE);
        transform.localScale = new Vector3(scale, scale, scale);
    }

    private Vector3 ChooseTarget()
    {
        Vector3 currentTarget = target;
        Building targetBuilding = null;

        while (targetBuilding is WallController || targetBuilding == null || targetBuilding.transform.position == target)
        {
            int index = Random.Range(0, GameManager.Buildings.Count - 1);

            targetBuilding = GameManager.Buildings[index];
        }

        return targetBuilding.transform.position;
    }

    private void ControllAnimation()
    {
        if (agent.velocity.sqrMagnitude > 0.5f)
            myAnimation.Play("Walk", PlayMode.StopAll);
        else
            myAnimation.Play("Idle", PlayMode.StopAll);
    }

    IEnumerator TargetReachedTimer()
    {
        target = ChooseTarget();
        yield return new WaitForSeconds(2f);

        agent.SetDestination(target);
    }
}
