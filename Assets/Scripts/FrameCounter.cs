﻿using UnityEngine;
using UnityEngine.UI;

public class FrameCounter : MonoBehaviour
{
    public Text FpsText;
    public float UpdateSpeed;

    private float factor;
    private int frames;
    private float timeLeft;

    void Start()
    {
        timeLeft = UpdateSpeed;
    }

    void OnEnable()
    {
        timeLeft = UpdateSpeed;
    }

    void Update()
    {
        timeLeft -= Time.unscaledDeltaTime;
        factor += 1 / Time.unscaledDeltaTime;
        frames++;

        if (timeLeft <= 0f)
        {
            float fps = factor / frames;

            FpsText.text = string.Format("Fps: {0}", (int)fps);

            if (fps < 35)
                FpsText.color = Color.yellow;
            else if (fps < 25)
                FpsText.color = Color.red;
            else
                FpsText.color = Color.green;

            timeLeft = UpdateSpeed;
            factor = 0f;
            frames = 0;
        }
    }
}