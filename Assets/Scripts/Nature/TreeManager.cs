﻿using UnityEngine;
using System.Collections.Generic;

public class TreeManager : MonoBehaviour
{
    private static List<TreeController> trees = new List<TreeController>();

    public static List<TreeController> Trees
    {
        get { return trees; }
    }

    void Start()
    {
        AddTreeControllerToTreeInstances();
    }

    public static bool RemoveTree(TreeController treeController)
    {
        RemoveTreeAppearence(treeController);

        if (trees.Contains(treeController))
        {
            trees.Remove(treeController);
            return true;
        }
        else
            return false;
    }

    public static bool AddTree(TreeController treeController)
    {
        if (trees.Contains(treeController))
            return false;
        else
        {
            trees.Add(treeController);
            return true;
        }
    }

    private static void RemoveTreeAppearence(TreeController treeController)
    {
        if (treeController != null)
        {
            List<TreeInstance> newTrees = new List<TreeInstance>(GameManager.Manager.GetTerrain().terrainData.treeInstances);

            newTrees.Remove(treeController.MyTreeInstance);

            GameManager.Manager.GetTerrain().terrainData.treeInstances = newTrees.ToArray();
        }
    }

    private void AddTreeControllerToTreeInstances()
    {
        int treeCount = GameManager.Manager.GetTerrain().terrainData.treeInstances.Length;

        for (int i = 0; i < treeCount; i++)
        {
            GameObject treeObject = new GameObject("TreeObject");
            Vector3 realTreePosition = Vector3.Scale(GameManager.Manager.GetTerrain().terrainData.treeInstances[i].position, GameManager.Manager.GetTerrain().terrainData.size) + GameManager.Manager.GetTerrain().transform.position;
            treeObject.transform.position = realTreePosition;

            TreeController controller = treeObject.AddComponent<TreeController>();
            controller.MyTreeInstance = GameManager.Manager.GetTerrain().terrainData.treeInstances[i];
            controller.CurrentWood = 2000;
        }
    }
}
