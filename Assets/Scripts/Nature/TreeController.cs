﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreeController : SelectableObject
{
    private const float REMOVEDELAY = 0.2f;
    private const int SIZE = 2;

    public int CurrentWood;
    [HideInInspector]
    public bool Destroyed = false;
    [HideInInspector]
    public TreeInstance MyTreeInstance;

    void Start()
    {
        GameManager.AddSelectableObject(this);
        GameManager.Manager.BuildingMap.UsePosition(transform.position, 2);
        TreeManager.AddTree(this);
    }

    public void DecreaseWood(int Amount)
    {
        CurrentWood -= Amount;

        if (CurrentWood <= 0)
        {
            CurrentWood = 0;
            RemoveTree();
        }

        if (selected && !Destroyed)
        {
            GameManager.SelectedObjects.Clear();
            OnSelected(ref GameManager.SelectedObjects);
        }
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        GameManager.Manager.ShowPanel(Image, Name, "Wood Left: " + CurrentWood, null);
        base.OnSelected(ref selectedObjects);
    }

    private void RemoveTree()
    {
        if (Destroyed)
            return; 

        Destroyed = true;
        StartCoroutine(RemoveTimer());
    }

    IEnumerator RemoveTimer()
    {
        yield return new WaitForSeconds(REMOVEDELAY);

        GameManager.RemoveSelectableObject(this);
        GameManager.Manager.BuildingMap.FreePosition(transform.position, SIZE);
        TreeManager.RemoveTree(this);
        Destroy(gameObject);
    }
}
