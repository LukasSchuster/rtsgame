﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodBushController : SelectableObject
{
    private const float REMOVEDELAY = 0.2f;

    [Header("Specific Fields")]
    public int CurrentFood;
    [HideInInspector]
    public bool Destroyed = false;

    void Start()
    {
        GameManager.AddSelectableObject(this);
        GameManager.Manager.BuildingMap.UsePosition(transform.position, 2);
    }

    public void DecreaseFood(int amount)
    {
        CurrentFood -= amount;

        if (CurrentFood <= 0)
        {
            CurrentFood = 0;
            Remove();
            Destroyed = true;
        }

        if (selected && !Destroyed)
        {
            GameManager.SelectedObjects.Clear();
            OnSelected(ref GameManager.SelectedObjects);
        }
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        selected = false;
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        selected = true;
        GameManager.Manager.ShowPanel(Image, Name, string.Format("Food Left: {0}", CurrentFood), null);
        base.OnSelected(ref selectedObjects);
    }

    private void Remove()
    {
        if (Destroyed)
            return;

        StartCoroutine(RemoveTimer());
    }

    IEnumerator RemoveTimer()
    {
        yield return new WaitForSeconds(REMOVEDELAY);

        GameManager.RemoveSelectableObject(this);
        GameManager.Manager.BuildingMap.FreePosition(transform.position, 2);
        Destroy(gameObject);
    }
}
