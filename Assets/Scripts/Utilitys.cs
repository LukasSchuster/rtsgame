﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Utilitys
{
    private const int LAYERTOIGNOREINPUTINDEX = 11;

    private static Texture2D texture;

    public static Texture2D Texture
    {
        get
        {
            if (texture == null)
            {
                texture = new Texture2D(1, 1);
                texture.SetPixel(0, 0, Color.white);
                texture.Apply();
            }
            return texture;
        }
    }

    public static void DrawTexture(Rect rect, Texture image)
    {
        GUI.DrawTexture(rect, image);
    }

    public static void DrawRect(Rect rect, Color color)
    {
        GUI.color = color;
        GUI.DrawTexture(rect, Texture);
        GUI.color = Color.white;
    }

    public static void DrawRectBorder(Rect rect, float thickness, Color color)
    {
        Utilitys.DrawRect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
        Utilitys.DrawRect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
        Utilitys.DrawRect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
        Utilitys.DrawRect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
    }

    public static Rect GetRectScreen(Vector3 mouseClickedPos, Vector3 positionToDrawto)
    {
        mouseClickedPos.y = Screen.height - mouseClickedPos.y;
        positionToDrawto.y = Screen.height - positionToDrawto.y;

        Vector3 topLeft = Vector3.Min(mouseClickedPos, positionToDrawto);
        Vector3 bottomRight = Vector3.Max(mouseClickedPos, positionToDrawto);

        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    /// <summary>
    /// The will Retrun the Position the Raycast from the Camera to the MouseWorldPosition hits the first Obstacle
    /// </summary>
    /// <returns></returns>
    public static Vector3 GiveMousePositionInWorld()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
            return hit.point;
        else
            return Vector3.zero;
    }

    /// <summary>
    /// This will Return the Position the Raycast from the Camera to the MouseWorldPosition hits the Layer floor, vector Zero if it never hits the Floor
    /// </summary>
    /// <returns></returns>
    public static Vector3 GiveMousePositionInWorldOnFloor()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hit = Physics.RaycastAll(ray);

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.tag == "Floor")
                return GameManager.Manager.BuildingMap.GivePositionNear(hit[i].point);
        }
        return Vector3.zero;
    }

    public static float ClampAngel(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    public static Vector3 GivePositionOnFloor(Vector3 pos)
    {
        Ray ray = new Ray(pos, Vector3.down);
        RaycastHit[] hit = Physics.RaycastAll(ray);

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.tag == "Floor")
                return hit[i].point;
        }
        return Vector3.zero;
    }

    /// <summary>
    /// Return if you hit an UI Element 
    /// </summary>
    /// <returns></returns>
    public static bool RayCastForUIElement()
    {
        PointerEventData pointer = new PointerEventData(EventSystem.current);

        pointer.position = Camera.main.WorldToScreenPoint(Input.mousePosition);

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);
        if (raycastResults.Count > 0)
        {
            if (raycastResults[0].gameObject.layer == LAYERTOIGNOREINPUTINDEX)
                return true;
        }
        return false;
    }

    /// <summary>
    /// This returs an Array of Gameobjects that are selected by a Raycast shot from your Camera to the MouseWorldPosition
    /// </summary>
    /// <returns></returns>
    public static GameObject[] RayCastAllFromMouseToScreen()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hit = Physics.RaycastAll(ray);
        GameObject[] hitGameObjetcs = new GameObject[hit.Length];

        if (hit.Length == 0)
            return null;

        for (int i = 0; i < hit.Length; i++)
            hitGameObjetcs[i] = hit[i].transform.gameObject;

        return hitGameObjetcs;
    }

    public static Bounds GetViewportBounds(Camera camera, Vector3 clickedPosition, Vector3 currentMousePos)
    {
        var v1 = Camera.main.ScreenToViewportPoint(clickedPosition);
        var v2 = Camera.main.ScreenToViewportPoint(currentMousePos);
        var min = Vector3.Min(v1, v2);
        var max = Vector3.Max(v1, v2);
        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;

        var bounds = new Bounds();
        bounds.SetMinMax(min, max);
        return bounds;
    }

    public static GameObject[] CapsuleCastFromRect(Rect rect, Camera camera)
    {
        RaycastHit[] hits;

        hits = Physics.BoxCastAll(camera.ScreenToWorldPoint(rect.center), new Vector3(rect.width / 2, rect.height / 2, 0), camera.ScreenToWorldPoint(rect.center));

        GameObject[] gameObjects = new GameObject[hits.Length];

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag != "Selectable")
                gameObjects[i] = hits[i].transform.gameObject;
        }

        return gameObjects;
    }

    /// <summary>
    /// This will turn the Color of the Given Gameobject to the given Color, if it has an Material attached
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="color"></param>
    public static void ChangeColorOfGameObjectAndChilds(GameObject gameObject, Color color)
    {
        Renderer[] materials = gameObject.GetComponentsInChildren<Renderer>();

        foreach (var renderer in materials)
            renderer.material.color = color;
    }

    /// <summary>
    /// This will turn the Given GameObjects alpha to the given value, if it has an Material attached
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="value"></param>
    public static void MakeObjectTransparent(GameObject gameObject, float value)
    {
        Renderer[] materials = gameObject.GetComponentsInChildren<Renderer>();

        foreach (var renderer in materials)
        {
            Color tempColor = renderer.material.color;
            tempColor.a = value;
            renderer.material.color = tempColor;
        }
    }

    /// <summary>
    /// This will turn the Given Gameobjects alpha to 255
    /// </summary>
    /// <param name="gameObject"></param>
    public static void MakeObjectVisible(GameObject gameObject)
    {
        Renderer[] materials = gameObject.GetComponentsInChildren<Renderer>();

        foreach (var renderer in materials)
        {
            Color tempColor = renderer.material.color;
            tempColor.a = 255;
            renderer.material.color = tempColor;
        }
    }

    /// <summary>
    /// This will Rotate the given Gameobject by the given degrees
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="deg"></param>
    public static void RotateGameObjectByDeg(GameObject gameObject, float deg)
    {
        gameObject.transform.Rotate(0, deg, 0, Space.Self);
    }

    /// <summary>
    /// This Method checks if only one Type of Selectable Object is selected
    /// </summary>
    /// <param name="listOfSelectedObjects"></param>
    /// <returns></returns>
    public static bool CheckSelectionForMultipleTypes(List<SelectableObject> listOfSelectedObjects)
    {
        bool hasBuilding = false;
        bool hasUnit = false;

        for (int i = 0; i < listOfSelectedObjects.Count; i++)
        {
            if (listOfSelectedObjects[i] is Unit)
                hasUnit = true;

            if (listOfSelectedObjects[i] is Building)
                hasBuilding = true;
        }

        if (hasBuilding && hasUnit)
            return true;

        return false;
    }

    /// <summary>
    /// This Method Returns from all the Current Selected Objects only the ones with the Type of the most selected
    /// </summary>
    /// <param name="listOfSelectedObjects"></param>
    /// <returns></returns>
    public static List<SelectableObject> GetThePriorSelectableObjectFromList(List<SelectableObject> listOfSelectedObjects)
    {
        int unitCount = 0;
        int buildingCount = 0;

        for (int i = 0; i < listOfSelectedObjects.Count; i++)
        {
            if (listOfSelectedObjects[i] is Unit)
                unitCount++;
            if (listOfSelectedObjects[i] is Building)
                buildingCount++;
        }

        List<SelectableObject> tempList = new List<SelectableObject>();

        if (unitCount >= buildingCount)
        {
            for (int i = 0; i < listOfSelectedObjects.Count; i++)
            {
                if (listOfSelectedObjects[i] is Unit)
                    tempList.Add(listOfSelectedObjects[i]);
            }
        }
        else
        {
            for (int i = 0; i < listOfSelectedObjects.Count; i++)
            {
                if (listOfSelectedObjects[i] is Building)
                    tempList.Add(listOfSelectedObjects[i]);
            }
        }

        return tempList;
    }

    public static bool CheckForOnlyOneSelectedType()
    {
        SelectableObject a = null;

        foreach (SelectableObject item in GameManager.SelectedObjects)
        {
            if (a == null)
                a = item;

            if (item.GetType() != a.GetType())
                return false;
        }
        return true;
    }

    public static Vector3 GetDirectionOfMouseWithoutHeight(Vector3 currentPos)
    {
        Vector3 mouseClick = GiveMousePositionInWorldOnFloor();
        mouseClick.y = currentPos.y;
        Vector3 direction = (mouseClick - currentPos).normalized; 

        return direction;
    }

    /// <summary>
    /// Scale THe Gameobject by the given Scale
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="scale"></param>
    public static void ScaleGameobject(GameObject gameObject, Vector3 scale)
    {
        gameObject.transform.localScale = scale;
    }

    /// <summary>
    /// Scale The Parent Gameobject without Scaling its Children
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="scale"></param>
    public static void ScaleParentGameObject(Transform parent, Vector3 scale)
    {
        List<Transform> childs = new List<Transform>();

        foreach (Transform child in parent)
        {
            child.parent = null;
            childs.Add(child);
        }

        parent.localScale = scale;

        foreach (Transform child in childs)
            child.parent = parent;
    }

    public static float LeftRightCheck(Vector3 forward, Vector3 targetDirection, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(forward, targetDirection);
        float direction = Vector3.Dot(perp, up);

        if (direction > 0f)
            return 1f;
        else if (direction < 0f)
            return -1f;
        else
            return 0f;
    }

    /// <summary>
    /// Gets the Nearest Wall relative to the given position.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static Building GetNearestWall(Vector3 pos)
    {
        float currentDist = float.MaxValue;
        Building currentWall = null;

        for (int i = 0; i < GameManager.WallsList.Count; i++)
        {
            if (GameManager.WallsList[i] == null) continue;

            if (Vector3.Distance(pos, GameManager.WallsList[i].transform.position) <= currentDist)
            {
                currentDist = Vector3.Distance(pos, GameManager.WallsList[i].transform.position);
                currentWall = GameManager.WallsList[i];
            }
        }

        return currentWall;
    }

    public static TreeInstance[] GetAllTerrainTrees()
    {
        return GameManager.Manager.GetTerrain().terrainData.treeInstances;
    }
}
