﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MainMenuCameraCotroller : MonoBehaviour
{
    [Header("Fields")]
    public CreditsController creditsController;
    public float MinDistanceToTarget;
    public float Speed;
    public float RotationsSpeed;

    [Header("Positions")]
    public Transform StartPosition;
    public Transform StartPositionsLookAt;

    public Transform CreditsTransform;
    public Transform CreditsTransformLookAt;

    [Header("UI")]
    public Button CreditsButton;
    public Button CreditsBackButton;

    public GameObject MainMenuPanel;

    private Coroutine moveCoroutine;
    private CameraShake cameraShake;

    void Awake()
    {
        cameraShake = GetComponent<CameraShake>();
    }

    void Start()
    {
        CreditsButton.onClick.AddListener(delegate { OnCreditsButtonClicked(); });
        //CreditsBackButton.onClick.AddListener(delegate { OnCreditsBackButtonClicked(); });
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnCreditsBackButtonClicked();
    }

    private void OnCreditsBackButtonClicked()
    {
        if (moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        creditsController.StopCredits();

        cameraShake.enabled = false;
        moveCoroutine = StartCoroutine(MoveCoroutine(StartPosition.position, StartPositionsLookAt));
    }

    private void OnCreditsButtonClicked()
    {
        if (moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        creditsController.ResetCredits();

        cameraShake.enabled = false;
        MainMenuPanel.SetActive(false);
        moveCoroutine = StartCoroutine(MoveCoroutine(CreditsTransform.position, CreditsTransformLookAt));
    }

    IEnumerator MoveCoroutine(Vector3 target, Transform lookAt)
    {
        yield return new WaitForFixedUpdate();

        transform.position = Vector3.Lerp(transform.position, target, Speed);
        transform.rotation = Quaternion.Lerp(transform.rotation, lookAt.rotation, RotationsSpeed);

        //transform.LookAt(Vector3.Lerp(transform.forward, lookAt, RotationsSpeed));
        
        if (Vector3.Distance(transform.position, target) <= MinDistanceToTarget)
        {
            transform.position = target;
            transform.LookAt(lookAt);
            moveCoroutine = null;

            if (transform.position == StartPosition.position)
            {
                MainMenuPanel.SetActive(true);
                cameraShake.enabled = true;
            }
            else if (transform.position == CreditsTransform.position)
                creditsController.StartCredits();
        }
        else
        {
            moveCoroutine = StartCoroutine(MoveCoroutine(target, lookAt));
        }
    }
}
