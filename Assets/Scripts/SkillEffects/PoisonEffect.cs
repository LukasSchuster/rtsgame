﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoisonEffect : SkillEffect
{
    private const string EFFECTTEXT = "Poisoned";

    public PoisonEffect(float damage, float duration, float radius, float delay, List<Health> enemys) : base(damage, duration, radius, delay, enemys) { }

    protected override void OnEveryDelaydFrame()
    {
        foreach (Health enemyHealth in Enemys)
        {
            if (enemyHealth != null)
                enemyHealth.DecreaseHealth(Damage);

            Vector3 pos = new Vector3(enemyHealth.transform.position.x, enemyHealth.transform.position.y + 2.5f, enemyHealth.transform.position.z);

            DamageIndicator.Create(pos, EFFECTTEXT, 3.5f, 2, Color.green);
        }

        base.OnEveryDelaydFrame();
    }
}
