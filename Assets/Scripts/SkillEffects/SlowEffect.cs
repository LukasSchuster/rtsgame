﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlowEffect : SkillEffect
{
    private const string EFFECTTEXT = "Slowed";

    public SlowEffect(float damage, float duration, float radius, float delay, List<Health> enemys) : base(damage, duration, radius, delay, enemys) { }

    protected override void OnStartFrame()
    {
        foreach (Health enemyHealth in Enemys)
        {
            if (enemyHealth != null)
                enemyHealth.gameObject.GetComponent<NavMeshAgent>().speed /= 2;

            Vector3 pos = new Vector3(enemyHealth.transform.position.x, enemyHealth.transform.position.y + 2.5f, enemyHealth.transform.position.z);

            DamageIndicator.Create(pos, EFFECTTEXT, 3.5f, 2, Color.blue);
        }


        base.OnStartFrame();
    }

    protected override void OnEndFrame()
    {
        foreach (Health enemyHealth in Enemys)
        {
            if (enemyHealth != null)
                enemyHealth.gameObject.GetComponent<NavMeshAgent>().speed *= 2;
        }

        base.OnEndFrame();
    }
}
