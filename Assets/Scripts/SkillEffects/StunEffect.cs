﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StunEffect : SkillEffect
{
    private const string EFFECTTEXT = "Stunned";

    public StunEffect(float damage, float duration, float radius, float delay, List<Health> enemys) : base(damage, duration, radius, delay, enemys) { }

    protected override void OnStartFrame()
    {
        foreach (Health enemyHealth in Enemys)
        {
            enemyHealth.gameObject.GetComponent<NavMeshAgent>().Stop();

            Vector3 pos = new Vector3(enemyHealth.transform.position.x, enemyHealth.transform.position.y + 2.5f, enemyHealth.transform.position.z);

            DamageIndicator.Create(pos, EFFECTTEXT, 3.5f, 2, Color.yellow);
        }

        base.OnStartFrame();
    }

    protected override void OnEndFrame()
    {
        foreach (Health enemy in Enemys)
            enemy.gameObject.GetComponent<NavMeshAgent>().Resume();

        base.OnEndFrame();
    }
}
