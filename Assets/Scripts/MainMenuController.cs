﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System;

public class MainMenuController : MonoBehaviour
{
    [Header("Config")]
    public int GameSceneIndex;
    public AudioSource MusicSource;
    public AudioSource EffectsSource;

    [Header("UI Controller")]
    public Button OptionsButton;
    public Button CreditsButton;
    public Button ExitButton;

    [Header("Options UI")]
    public Slider MusicSlider;
    public Slider EffectSlider;
    public Toggle AntiAliasingTogle;
    public Toggle ShadowsTogle;
    public Toggle DrawEnvironmentTogle;

    [Header("UI")]
    public GameObject OptionsPanel;

    void Start()
    {
        OptionsButton.onClick.AddListener(delegate { OnOptionsClicked(); });
        CreditsButton.onClick.AddListener(delegate { OnCreditsClicked(); });
        ExitButton.onClick.AddListener(delegate { OnExitClicked(); });

        MusicSlider.onValueChanged.AddListener(delegate { OnMusicSliderChanged(); });
        EffectSlider.onValueChanged.AddListener(delegate { OnEffectsSliderChanged(); });
        AntiAliasingTogle.onValueChanged.AddListener(delegate { OnAntiAliasingClicked(); });
        ShadowsTogle.onValueChanged.AddListener(delegate { OnShadowsClicked(); });
        DrawEnvironmentTogle.onValueChanged.AddListener(delegate { OnDrawEnvironmentClicked(); });

        OnStart();

        OptionsPanel.SetActive(false);

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnDrawEnvironmentClicked()
    {
        GameManager.Manager.GameSettings.DrawEnvironment = DrawEnvironmentTogle.isOn;
    }

    private void OnShadowsClicked()
    {
        GameManager.Manager.GameSettings.Shadows = ShadowsTogle.isOn;
    }

    private void OnAntiAliasingClicked()
    {
        GameManager.Manager.GameSettings.AntiAliasing = AntiAliasingTogle.isOn;
    }

    private void OnEffectsSliderChanged()
    {
        GameManager.Manager.GameSettings.EffectsVolume = EffectSlider.value;
        EffectsSource.volume = EffectSlider.value;
    }

    private void OnMusicSliderChanged()
    {
        GameManager.Manager.GameSettings.MusicVolume = MusicSlider.value;
        MusicSource.volume = MusicSlider.value;
    }

    private void OnExitClicked()
    {
        Application.Quit();
    }

    private void OnCreditsClicked()
    {

    }

    private void OnOptionsClicked()
    {
        if (OptionsPanel.activeSelf)
            OptionsPanel.SetActive(false);
        else
            OptionsPanel.SetActive(true);
    }

    private void OnStart()
    {
        Cursor.visible = true;

        GameManager.Manager.GameSettings.AntiAliasing = AntiAliasingTogle.isOn;
        GameManager.Manager.GameSettings.DrawEnvironment = DrawEnvironmentTogle.isOn;
        GameManager.Manager.GameSettings.EffectsVolume = EffectSlider.value;
        GameManager.Manager.GameSettings.MusicVolume = MusicSlider.value;
        GameManager.Manager.GameSettings.Shadows = ShadowsTogle.isOn;
    }
}
