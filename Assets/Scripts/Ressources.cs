﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Ressources : MonoBehaviour
{
    private const float IMAGE_VALUE_LERP_SPEED = 1.5f;
    private const float MIN_IMAGE_LERP_DIST = 0.01f;

    public int MaxGodPoints;

    public int money;
    public int wood;
    public int food;
    private int godPoints;

    private Coroutine healthLerpCoroutine;
    private Coroutine manaLerpCoroutine;

    #region properties

    public int Money { get { return money; } }
    public int Wood { get { return wood; } }
    public int Food { get { return food; } }
    public int GodPoints { get { return godPoints; } }

    #endregion

    [Header("UI Stuff")]
    public Text MoneyText;
    public Text WoodText;
    public Text FoodText;
    public Image GodPointsImageHeroMode;
    public Image HealthImageHeroMode;
    public Slider GodPointsSliderBuildMode;

    private Text HealthBarTextHeroMode;
    private Text GodPointsTextBuildMode;
    private Text GodPointsTextHeroMode;
    private StrategieCameraController controller;
    private Health heroHealth;

    void Awake()
    {
        controller = GetComponent<StrategieCameraController>();

        if (GodPointsSliderBuildMode != null)
            GodPointsTextBuildMode = GodPointsSliderBuildMode.GetComponentInChildren<Text>();

        if (GodPointsImageHeroMode != null)
            GodPointsTextHeroMode = GodPointsImageHeroMode.GetComponentInChildren<Text>();

        if (HealthImageHeroMode != null)
            HealthBarTextHeroMode = HealthImageHeroMode.GetComponentInChildren<Text>();
    }

    void Start()
    {
        heroHealth = GameManager.Manager.GetHeroController().GetComponent<Health>();
        heroHealth.OnChanged += HeroHealth_OnChanged;

        GodPointsSliderBuildMode.maxValue = MaxGodPoints;

        MoneyText.text = money.ToString();
        FoodText.text = Food.ToString();
        WoodText.text = Wood.ToString();

        UpdateGodPointsSlider();
    }

    public void HeroHealth_OnChanged()
    {
        if (HealthImageHeroMode != null)
        {
            if (healthLerpCoroutine != null) StopCoroutine(healthLerpCoroutine);
            healthLerpCoroutine = StartCoroutine(HealthValueLerper(HealthImageHeroMode));
            HealthBarTextHeroMode.text = string.Format("{0} / {1}", heroHealth.CurrentHealth.ToString(), heroHealth.MaxHealth.ToString());
        }
    }

    public void IncreaseGodPoints(int amount)
    {
        godPoints += amount;

        if (godPoints > MaxGodPoints)
            godPoints = MaxGodPoints;

        UpdateGodPointsSlider();
    }

    public void DecreaseGodPoints(int amount)
    {
        godPoints -= amount;

        if (godPoints < 0)
            godPoints = 0;

        UpdateGodPointsSlider();
    }

    public void IncreaseWood(int amount)
    {
        GameManager.Manager.GameStats.CollectedWood += amount;
        wood += amount;
        WoodText.text = wood.ToString();
    }

    public void DecreaseWood(int amount)
    {
        wood -= amount;
        if (wood < 0)
            wood = 0;
        WoodText.text = wood.ToString();
    }

    public void IncreaseMoney(int amount)
    {
        GameManager.Manager.GameStats.CollectedGold += amount;
        money += amount;
        MoneyText.text = money.ToString();

    }

    public void DecreaseMoney(int amount)
    {
        money -= amount;
        if (money < 0)
            money = 0;
        MoneyText.text = money.ToString();
    }

    public void IncreaseFood(int amount)
    {
        GameManager.Manager.GameStats.CollectedFood += amount;
        food += amount;
        FoodText.text = food.ToString();
    }

    public void DecreaseFood(int amount)
    {
        food -= amount;
        if (food < 0)
            food = 0;
        FoodText.text = food.ToString();
    }

    private void UpdateGodPointsSlider()
    {
        if (GodPointsSliderBuildMode != null)
        {
            GodPointsSliderBuildMode.value = GodPoints;
            GodPointsTextBuildMode.text = string.Format("{0} / {1}", GodPoints.ToString(), MaxGodPoints.ToString());
        }

        if (GodPointsImageHeroMode != null)
        {
            if (manaLerpCoroutine != null) StopCoroutine(manaLerpCoroutine);
            manaLerpCoroutine = StartCoroutine(ManaValueLerper(GodPointsImageHeroMode));
            GodPointsTextHeroMode.text = string.Format("{0} / {1}", GodPoints.ToString(), MaxGodPoints.ToString());
        }
    }

    IEnumerator HealthValueLerper(Image image)
    {
        yield return new WaitForFixedUpdate();

        if (image != null)
        {
            float wantedValue = (heroHealth.CurrentHealth / heroHealth.MaxHealth);
            image.fillAmount = Mathf.Lerp(image.fillAmount, wantedValue, IMAGE_VALUE_LERP_SPEED * Time.deltaTime);

            if (Mathf.Abs(wantedValue - image.fillAmount) <= MIN_IMAGE_LERP_DIST)
                image.fillAmount = wantedValue;
            else
                healthLerpCoroutine = StartCoroutine(HealthValueLerper(image));
        }
    }

    IEnumerator ManaValueLerper(Image image)
    {
        yield return new WaitForFixedUpdate();

        if (image != null)
        {
            float wantedValue = ((float)godPoints / (float)MaxGodPoints);
            image.fillAmount = Mathf.Lerp(image.fillAmount, wantedValue, IMAGE_VALUE_LERP_SPEED * Time.deltaTime);

            if (Mathf.Abs(wantedValue - image.fillAmount) <= MIN_IMAGE_LERP_DIST)
                image.fillAmount = wantedValue;
            else
                manaLerpCoroutine = StartCoroutine(ManaValueLerper(image));
        }
    }
}
