﻿using UnityEngine;
using System.Collections;

public class HeroAttackIndicator : MonoBehaviour
{
    private Projector projector;

    void Start()
    {
        projector = GetComponentInChildren<Projector>();
        projector.enabled = false;
    }

    public void ShowIndicator(Vector3 lookDir)
    {
        //transform.position = GameManager.Manager.GetHeroController().AttackIndicatorDockPosition.position;

        Vector3 lookAt = transform.position + lookDir;
        lookAt.y = transform.position.y;

        transform.LookAt(lookAt);
        projector.enabled = true;
    }

    public void HideIndicator()
    {
        if (projector.enabled == true)
            projector.enabled = false;
    }
}
