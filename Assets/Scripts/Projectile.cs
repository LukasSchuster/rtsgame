﻿using UnityEngine;
using System.Collections;
using System;

public class Projectile : MonoBehaviour, IPoolable
{
    public GameObject ExplosionParticle;

    [HideInInspector]
    public bool hasTarget;
    [HideInInspector]
    public Transform Target;

    private ObjectPool<Projectile> Pool;
    private Rigidbody myRigidbody;
    private Vector3 direction;
    private Vector3 StartPos;
    private float damage;
    private float speed;
    private float maxDistance;
    private float range;

    void Awake()
    {
        StartPos = transform.position;
        myRigidbody = GetComponent<Rigidbody>();
    }

    void LateUpdate()
    {
        if (hasTarget && Target != null)
        {
            transform.LookAt(new Vector3(Target.position.x, Target.position.y + 1, Target.position.z));
            myRigidbody.velocity = transform.forward * speed;
        }

        if (Target != null && hasTarget)
        {
            if (Vector3.Distance(transform.position, Target.position) <= 0.5f)
            {
                if (ExplosionParticle != null)
                    Instantiate(ExplosionParticle, transform.position, Quaternion.identity);
                Explode();

                Pool.StoreObject(gameObject);
                return;
            }
        }
        else if (hasTarget && Target == null)
        {
            if (ExplosionParticle != null)
                Instantiate(ExplosionParticle, transform.position, Quaternion.identity);
            Explode();

            Pool.StoreObject(gameObject);
            return;
        }

        if (Vector3.Distance(StartPos, transform.position) >= maxDistance)
        {
            if (ExplosionParticle != null)
                Instantiate(ExplosionParticle, transform.position, Quaternion.identity);
            Explode();

            Pool.StoreObject(gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Hero") return;

        if (ExplosionParticle != null)
            Instantiate(ExplosionParticle, transform.position, Quaternion.identity);
        Explode();

        Pool.StoreObject(gameObject);
    }

    public void SetValues(Vector3 direction, float damage, float speed, float maxDistance, float range, ObjectPool<Projectile> pool)
    {
        if (Pool != pool || Pool == null)
            Pool = pool;

        StartPos = transform.position;

        this.range = range;
        this.direction = new Vector3(direction.x, 0, direction.z);
        this.damage = damage;
        this.speed = speed;
        this.maxDistance = maxDistance;
    }

    public void SetTarget(Transform target)
    {
        this.Target = target;
        this.hasTarget = true;
        this.myRigidbody.mass = 0.6f;


        transform.LookAt(target);
    }

    public void Fire()
    {
        if (!hasTarget)
            myRigidbody.AddForce(direction * speed, ForceMode.Impulse);
    }

    private void Explode()
    {
        foreach (var enemyHealth in UnitManager.Manager.EnemyUnits)
        {
            if (Vector3.Distance(transform.position, enemyHealth.transform.position) <= range)
            {
                HealthHitInfo hitInfo = enemyHealth.DecreaseHealth(damage);

                if (hitInfo.Type == HealthHitInfo.HitType.Minion)
                {
                    if (hitInfo.IsDead)
                        GameManager.Manager.GameStats.MinionKillCount++;
                }
            }
        }

        for (int i = 0; i < GameManager.Manager.GetEnemyMinionControllerList().Count; i++)
        {
            if (Vector3.Distance(transform.position, GameManager.Manager.GetEnemyMinionControllerList()[i].transform.position) <= range)
            {
                Health enemyMinionSpawnerHealth = GameManager.Manager.GetEnemyMinionControllerList()[i].gameObject.GetComponent<Health>();
                if (enemyMinionSpawnerHealth != null)
                    enemyMinionSpawnerHealth.DecreaseHealth(damage / 2);
            }
        }
    }

    public void ResetObject()
    {
        myRigidbody.mass = 1;
        hasTarget = false;
        myRigidbody.velocity = Vector3.zero;
        myRigidbody.angularVelocity = Vector3.zero;
    }
}
