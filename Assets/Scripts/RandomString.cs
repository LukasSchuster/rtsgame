﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class RandomString : MonoBehaviour
{
    public List<string> Strings = new List<string>();

    public Text TextToEdit;

    void Awake()
    {
        if (TextToEdit != null && Strings.Count > 0)
            TextToEdit.text = Strings[Random.Range(0, Strings.Count)];
    }
}
