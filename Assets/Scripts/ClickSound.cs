﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ClickSound : MonoBehaviour
{
    public enum SoundEffect { Button = 1, Toggle = 2, SoftButton = 3 }
    public SoundEffect Sound = SoundEffect.Button;

    private Button button;
    private Toggle toggle;

    void Awake()
    {
        button = GetComponent<Button>();
        toggle = GetComponent<Toggle>();

        if (button != null) button.onClick.AddListener(delegate { OnButtonClicked(); });
        if (toggle != null) toggle.onValueChanged.AddListener(delegate { OnToggleChanged(); });
    }

    private void OnToggleChanged()
    {
        GameManager.Manager.PlaySound(GameManager.SoundFile.Toggle);
    }

    private void OnButtonClicked()
    {
        if (Sound == SoundEffect.Button)
            GameManager.Manager.PlaySound(GameManager.SoundFile.Button);
        else if (Sound == SoundEffect.SoftButton)
            GameManager.Manager.PlaySound(GameManager.SoundFile.SoftButton);
    }
}
