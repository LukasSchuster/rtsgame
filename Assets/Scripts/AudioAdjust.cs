﻿using UnityEngine;
using System.Collections;

public class AudioAdjust : MonoBehaviour
{
    public enum AudioType { Effect = 1, Music = 2 }
    public AudioType SoundType = AudioType.Effect;

    private AudioSource[] audioSources;

    void Awake()
    {
        audioSources = GetComponents<AudioSource>();
    }

    void Start()
    {
        bool soundTypeEffect = true;

        switch (SoundType)
        {
            case AudioType.Effect:
                soundTypeEffect = true;
                break;
            case AudioType.Music:
                soundTypeEffect = false;
                break;
        }

        for (int i = 0; i < audioSources.Length; i++)
        {
            switch (SoundType)
            {
                case AudioType.Effect:
                    audioSources[i].volume = GameManager.Manager.GameSettings.EffectsVolume;
                    break;
                case AudioType.Music:
                    audioSources[i].volume = GameManager.Manager.GameSettings.MusicVolume;
                    break;
            }
        }

        GameManager.Manager.GetAudioManager().AddAudiosources(audioSources, soundTypeEffect);
    }
}
