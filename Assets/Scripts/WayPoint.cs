﻿using UnityEngine;
using System.Collections;

public class WayPoint : MonoBehaviour
{
    [HideInInspector]
    public Vector3 Position;

    public WayPoint NextWayPoint;
    public WayPoint PreviousWayPoint;

    void Awake()
    {
        WayPointManager.WayPoints.Add(this);
        Position = transform.position;
    }
}
