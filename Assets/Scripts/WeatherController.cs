﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;

public class WeatherController : MonoBehaviour
{
    public bool RandomWeather = false;

    public enum WeatherAppMode { Sunny = 1, Cloudy = 2, CloudyRainy = 3, Night = 4 }
    public WeatherAppMode WeatherMode = WeatherAppMode.Sunny;

    [Header("Light Color")]
    public Color SunnyLightColor;
    public Color NightLightColor;
    public Color CloudyLightColor;
    public Color ClodyRainyLightColor;

    [Header("Skybox")]
    public Material SunnySkyboxMat;
    public Material NightSkyboxMat;
    public Material CloudySkyboxMat;
    public Material CloudyRainySkyboxMat;

    [Header("Fog Color")]
    public Color SunnyFogColor;
    public Color NightFogColor;
    public Color CloudyFogColor;
    public Color CloudyRainyFogColor;

    [Header("Fog Density")]
    public float SunnyFogDensity;
    public float NightFogDensity;
    public float CloudyFogDensity;
    public float CloudyRainyFogDensity;

    private GlobalFog globalFog;
    private Light directionalLight;

    void Awake()
    {
        globalFog = Camera.main.GetComponent<GlobalFog>();
        directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();

        ControllWeather();
    }

    private void ControllWeather()
    {
        if (RandomWeather)
        {

        }
        else
        {
            switch (WeatherMode)
            {
                case WeatherAppMode.Sunny:
                    RenderSettings.skybox = SunnySkyboxMat;
                    RenderSettings.fog = false;
                    globalFog.enabled = false;
                    directionalLight.color = SunnyLightColor;

                    break;
                case WeatherAppMode.Cloudy:
                    RenderSettings.skybox = CloudySkyboxMat;
                    RenderSettings.fog = true;
                    globalFog.enabled = true;
                    directionalLight.color = CloudyLightColor;

                    break;
                case WeatherAppMode.CloudyRainy:
                    RenderSettings.skybox = CloudyRainySkyboxMat;
                    RenderSettings.fog = true;
                    globalFog.enabled = true;
                    directionalLight.color = ClodyRainyLightColor;

                    break;
                case WeatherAppMode.Night:
                    RenderSettings.skybox = NightSkyboxMat;
                    RenderSettings.fog = false;
                    globalFog.enabled = false;
                    directionalLight.color = NightLightColor;

                    break;
            }
        }
    }
}
