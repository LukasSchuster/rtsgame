﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SkillButtonBehavoir : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int ButtonNumber;

    private int skillsIndex;

    void Awake()
    {
        skillsIndex = ButtonNumber - 1;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Manager.UIPressed();

        HeroController tempHero = GameManager.Manager.GetHeroController();
        if (tempHero.Skills.Count >= ButtonNumber)
            GameManager.Manager.ShowToolTip(tempHero.Skills[skillsIndex].Image, tempHero.Skills[skillsIndex].Name, string.Format("{0} GodPoints: {1}", tempHero.Skills[skillsIndex].Description, tempHero.Skills[skillsIndex].GodPointsCost), 0, 0, 0);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Manager.UIDePressed();
        GameManager.Manager.HideToolTip();
    }
}
