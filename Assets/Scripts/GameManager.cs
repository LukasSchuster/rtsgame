﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class GameManager : MonoBehaviour
{
    private const string HERONAME = "Galtrilian_Player";

    public static GameManager Manager;

    public static List<Building> Buildings = new List<Building>();
    public static List<Building> WallsList = new List<Building>();

    public static LinkedList<SelectableObject> SelectableObjectsLinked { get { return selectableObjectsLinked; } }

    private static LinkedList<SelectableObject> selectableObjectsLinked = new LinkedList<SelectableObject>();

    public static List<SelectableObject> SelectedObjects = new List<SelectableObject>();

    private static List<SelectableObject> objectsToRemove = new List<SelectableObject>();
    private static int index = 0;
    private static bool removeThisFrame = false;

    [HideInInspector]
    public Canvas Canvas;

    #region Tutorial

    [Header("Tutorial Mode")]
    public bool TutorialMode = false;
    [HideInInspector]
    public bool TowerBuild = false;
    [HideInInspector]
    public bool CutterBuild = false;
    [HideInInspector]
    public bool BakerBuild = false;
    [HideInInspector]
    public bool TempleBuild = false;

    #endregion

    [Header("GameSettings")]
    public GameSettings GameSettings;

    [Header("BuildingMap")]
    public BuildingMap BuildingMap;

    [Header("GameStats")]
    public GameStats GameStats;

    [Header("Canvas Panels")]
    public GameObject HeroPanelDown;
    public GameObject MainPanelUpper;
    public GameObject MiniMapPanel;

    public CurrentSkillPanelController CurrentSkillPanelController;

    public GameObject MinionUpgradePanel;
    private Animator minionUpgradePanelAnimator;

    [Header("Effects")]
    public GameObject HitEffectPanel;
    public GameObject ClickEffect;
    public GameObject DamageIndicatorGameObject;
    public Color FriendlyDamageColor;
    public GameObject HeroAttackIndicatorGameobject;
    public Material OutlineMaterial;
    public GameObject BuildingRepairParticle;

    [Header("Tutorial")]
    public TutorialHandler TutorialHandler;
    public TutorialPanelController TutorialPanelController;

    [Header("Repair Canvas")]
    public GameObject RepairCanvasPrefab;

    [Header("Skybox")]
    public GameObject SkyBoxTerrain;

    [Header("FogOfWar")]
    public GameObject FogOfWarElementPrefab;
    public GameObject FogOfWarPlane;

    [Header("OverLay Projector")]
    public GameObject OverlayProjectorPrefab;

    [Header("HUD")]
    public GameObject BuildHud;
    public GameObject HeroHud;

    [Header("Audio")]
    private AudioManager audioManager;

    [Header("UI")]
    public GameObject SkillHeroCamera;
    public Text GameTime;
    private bool uIPressedThisFrame = false;

    [Header("Terrain")]
    public Terrain Terrain;
    public GameObject GrassPlane;

    [Header("Upgrade Panel")]
    public GameObject UpgradePanel;
    public GameObject UpgradePanelInnerPanel;
    private Animator upgradePanelAnimator;

    [Header("EndGame Panel")]
    public EndGameController EndGameController;

    [Header("MinionController")]
    public GameObject MinionControllerPanel;
    

    [Header("Villagar")]
    public GameObject VillagerPrefab;

    [Header("UnitCreatePanel")]
    public GameObject UnitCreatePanel;
    private Animator unitCreateAnimator;

    [Header("Tool Tip")]
    public GameObject ToolTip;
    public Image ToolTipImage;
    public Text ToolTipName;
    public Text ToolTipDescription;

    public Text ToolTipGoldCostText;
    public Text ToolTipGoldText;
    public Text ToolTipWoodCostText;
    public Text ToolTipWoodText;
    public Text ToolTipFoodCostText;
    public Text ToolTipFoodText;
    public Color TransParentColor;
    public Color VisibleTextColor;

    [Header("Skill Tool Tip")]
    public GameObject SkillToolTip;

    public Image SkillToolTipImage;
    public Text SkillToolTipNameText;
    public Text SkillToolTipDescriptionText;
    public Text SkillToolTipEffectText;
    public Text SkillToolTipGPCostText;
    public Text SkillToolTipGoldCostText;
    public Text SkillToolTipCoolDownText;

    [Header("Hint")]
    public GameObject Hint;
    public Text HintText;

    [Header("Weather")]
    private WeatherController weatherController;

    [Header("MessageBox")]
    public GameObject MessageBox;
    public Image MessageBoxImage;
    public Text MessageBoxTitle;
    public Text MessageBoxDescription;
    public Sprite WarningSprite;
    public Sprite ErrorSprite;
    public Sprite HintSprite;
    [HideInInspector]
    public enum MessageBoxMode { Error = 0, Warning = 1, Hint = 2 }
    [HideInInspector]
    public MessageBoxMode MessageMode;

    [Header("PauseMenu")]
    public GameObject PauseMenuPanel;
    private PauseMenuManager pauseMenuManager;
    private bool gamePaused = false;

    [Header("SkillPanelController")]
    public SkillPanelController SkillPanelController;

    [Header("Logic")]
    public SelectableObjectPanel CurrentPanel;
    public GameObject UpgradePrefab;

    [Header("Buildings")]
    public List<GameObject> BuildingPrefabs = new List<GameObject>();
    public MainBuildingController MainBuildingController;
    public Color BuildingHitColor;

    private BuildingCanvasController buildingCanvasController;

    private float timer;
    private StrategieCameraController controller;
    private HeroController hero;
    private Ressources ressources;
    private List<EnemyMinionSpawnerController> enemyMinionsSpawnerControllers = new List<EnemyMinionSpawnerController>();

    [Header("HeroSkills")]
    public List<Skill> HeroSkills = new List<Skill>();
    public HeroSkillsPanelController HeroSkillsPanelController;

    [Header("MiniMap")]
    public Camera MiniMapCamera;

    [Header("HealthBar")]
    public GameObject HealthBar_Outline;

    [Header("Sounds")]
    public AudioSource Audiosource;
    [HideInInspector]
    public enum SoundFile { Error = 1, Upgrade = 2 , Button = 3, Toggle = 4, SoftButton = 5}
    [HideInInspector]
    public SoundFile SoundType;
    public AudioClip UpgradeClip;
    public AudioClip ErrorClip;
    public AudioClip ButtonClip;
    public AudioClip ToggleClip;
    public AudioClip SoftButtonClip;

    [HideInInspector]
    public bool TextInputMode = false;
    private bool upgradeBaught;

    void Awake()
    {
        minionUpgradePanelAnimator = MinionUpgradePanel.GetComponent<Animator>();
        weatherController = GetComponent<WeatherController>();
        audioManager = GetComponent<AudioManager>();
        pauseMenuManager = PauseMenuPanel.GetComponent<PauseMenuManager>();
        buildingCanvasController = GetComponent<BuildingCanvasController>();
        Canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        controller = Camera.main.GetComponent<StrategieCameraController>();
        ressources = controller.GetComponent<Ressources>();
        upgradePanelAnimator = UpgradePanel.GetComponent<Animator>();
        hero = GameObject.Find(HERONAME).GetComponent<HeroController>();
        unitCreateAnimator = UnitCreatePanel.GetComponent<Animator>();
        Manager = this;
    }

    void Start()
    {
        HidePanel();

        GameSettings.SetSettingsToUserPrefs();
    }

    void LateUpdate()
    {
        if (removeThisFrame)
        {
            RemoveSelectableObjectsReferences();
            removeThisFrame = false;
        }
        CalculateGameTime();
        MoveToolTipToMouse();
        MoveSkillToolTipToMouse();
        MoveHintToMouse();
    }

    private static void RemoveSelectableObjectsReferences()
    {
        Manager.HideUpgradePanel();
        Manager.HideToolTip();

        foreach (var item in objectsToRemove)
        {
            if (selectableObjectsLinked.Contains(item))
                selectableObjectsLinked.Remove(item);
            

            if (SelectedObjects.Contains(item))
                SelectedObjects.Remove(item);

            if (item != null)
                Destroy(item.gameObject, 0.1f);
        }
    }

    public static void AddBuilding(Building building)
    {
        foreach (Building item in Buildings)
        {
            if (item != building)
            {
                Buildings.Add(building);
                return;
            }
        }

        if (Buildings.Count == 0)
            Buildings.Add(building);
    }

    public static void AddWall(Building wall)
    {
        if (!WallsList.Contains(wall))
            WallsList.Add(wall);
    }

    public static void RemoveWall(Building wall)
    {
        if (WallsList.Contains(wall))
            WallsList.Remove(wall);
    }

    public static void AddSelectableObject(SelectableObject selectableObject)
    {
        if (selectableObjectsLinked.Count <= 0 && selectableObjectsLinked.Contains(selectableObject) == false)
            selectableObjectsLinked.AddFirst(selectableObject);
        else if (selectableObjectsLinked.Contains(selectableObject) == false)
            selectableObjectsLinked.AddLast(selectableObject);
    }

    public static void RemoveSelectableObject(SelectableObject selectableObject)
    {
        objectsToRemove.Add(selectableObject);
        removeThisFrame = true;
    }

    private void CalculateGameTime()
    {
        timer += Time.deltaTime;

        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        GameTime.text = niceTime;
    }

    public void HeroButtonPressed()
    {
        if (controller.CamMode == StrategieCameraController.CameraMode.HeroMode)
        {
            hero.DeActivateHeroMode();
        }
        else
            controller.ChangeModeTo("HeroMode");
    }

    public void ShowPanel(Sprite image, string name, string description, List<Upgrade> upgrades)
    {
        CurrentPanel.Image.sprite = image;
        CurrentPanel.Name.text = name;
        CurrentPanel.Description.text = description;
        CurrentPanel.Count.text = (SelectedObjects.Count + 1).ToString();

        CurrentPanel.gameObject.SetActive(true);
    }

    public void HidePanel()
    {
        CurrentPanel.gameObject.SetActive(false);
    }

    public void ShowToolTip(Sprite image, string name, string description, int goldCost, int woodCost, int foodCost)
    {
        controller.FreezeMovement = true;

        if (ToolTip != null)
        {
            ToolTip.SetActive(true);

            ToolTipGoldCostText.text = goldCost.ToString();
            ToolTipGoldCostText.color = VisibleTextColor;
            ToolTipGoldText.color = VisibleTextColor;

            ToolTipWoodCostText.text = woodCost.ToString();
            ToolTipWoodCostText.color = VisibleTextColor;
            ToolTipWoodText.color = VisibleTextColor;

            ToolTipFoodCostText.text = foodCost.ToString();
            ToolTipFoodCostText.color = VisibleTextColor;
            ToolTipFoodText.color = VisibleTextColor;

            ToolTipName.text = name;
            ToolTipDescription.text = description;

            if (image != null)
                ToolTipImage.sprite = image;

            Vector2 pos;
            Vector2 posXVector = Vector2.zero;
            Vector2 posYVector = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, Input.mousePosition, Canvas.worldCamera, out pos);

            RectTransform rectTransform = ToolTip.GetComponent<RectTransform>();

            if (Input.mousePosition.y - (rectTransform.rect.height * Canvas.scaleFactor) < 0)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Input.mousePosition.x, rectTransform.rect.height * Canvas.scaleFactor), Canvas.worldCamera, out posYVector);

            if (Input.mousePosition.x + (rectTransform.rect.width * Canvas.scaleFactor) > Screen.width)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Screen.width - rectTransform.rect.width * Canvas.scaleFactor, Input.mousePosition.y), Canvas.worldCamera, out posXVector);

            ToolTip.transform.position = Canvas.transform.TransformPoint(new Vector3(
                posXVector == Vector2.zero ? pos.x : posXVector.x,
                posYVector == Vector2.zero ? pos.y : posYVector.y,
                0
                ));
        }
    }

    public void HideToolTip()
    {
        controller.FreezeMovement = false;

        if (ToolTip != null)
            ToolTip.SetActive(false);
    }

    public void ShowSkillToolTip(Sprite image, string name, string description, string effect, int goldCost, int GpCost, float coolDownTime)
    {
        controller.FreezeMovement = true;

        if (SkillToolTip != null)
        {
            SkillToolTip.SetActive(true);

            SkillToolTipImage.sprite = image;
            SkillToolTipNameText.text = name;
            SkillToolTipDescriptionText.text = description;
            SkillToolTipEffectText.text = string.Format("Effect: {0}", effect);
            SkillToolTipGPCostText.text = string.Format("GP: {0}", GpCost);
            SkillToolTipCoolDownText.text = string.Format("Cooldown: {0} sec", coolDownTime);

            if (goldCost != 0)
                SkillToolTipGoldCostText.text = string.Format("Gold: {0}", goldCost);
            else
                SkillToolTipGoldCostText.text = string.Empty;

            Vector2 pos;
            Vector2 posXVector = Vector2.zero;
            Vector2 posYVector = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, Input.mousePosition, Canvas.worldCamera, out pos);

            RectTransform rectTransform = SkillToolTip.GetComponent<RectTransform>();

            if (Input.mousePosition.y - (rectTransform.rect.height * Canvas.scaleFactor) < 0)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Input.mousePosition.x, rectTransform.rect.height * Canvas.scaleFactor), Canvas.worldCamera, out posYVector);

            if (Input.mousePosition.x + (rectTransform.rect.width * Canvas.scaleFactor) > Screen.width)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Screen.width - rectTransform.rect.width * Canvas.scaleFactor, Input.mousePosition.y), Canvas.worldCamera, out posXVector);

            SkillToolTip.transform.position = Canvas.transform.TransformPoint(new Vector3(
                posXVector == Vector2.zero ? pos.x : posXVector.x,
                posYVector == Vector2.zero ? pos.y : posYVector.y,
                0
                ));
        }
    }

    public void HideSkillToolTip()
    {
        controller.FreezeMovement = false;

        if (SkillToolTip != null)
            SkillToolTip.SetActive(false);
    }

    public void ShowMessageBox(MessageBoxMode mode, string title, string description, float timeToShow)
    {
        MessageBoxTitle.text = title;
        MessageBoxDescription.text = description;

        switch (mode)
        {
            case MessageBoxMode.Error:
                MessageBoxImage.sprite = ErrorSprite;
                PlaySound(SoundFile.Error);
                break;
            case MessageBoxMode.Warning:
                MessageBoxImage.sprite = WarningSprite;
                PlaySound(SoundFile.Error);
                break;
            case MessageBoxMode.Hint:
                MessageBoxImage.sprite = HintSprite;
                break;
        }

        MessageBox.SetActive(true);
        StartCoroutine(MessageBoxTimer(timeToShow));
    }

    public void HideMessageBox()
    {
        MessageBox.SetActive(false);
    }

    public void ShowHint(string text)
    {
        if (Hint.gameObject != null)
        {
            HintText.text = text;
            Hint.gameObject.SetActive(true);
        }
    }

    public void HideHint()
    {
        if (Hint.gameObject != null)
            Hint.SetActive(false);
    }

    public void ShowCurrentSkillPanel(Sprite image, string name, string describtion, string cost, string effect)
    {
        CurrentSkillPanelController.SetCurrentSkillPanel(image, name, describtion, cost, effect);
    }

    public void HideCurrentSkillPanel()
    {
        CurrentSkillPanelController.Hide();
    }

    public void ShowUpgradePanel()
    {
        foreach (Transform child in UpgradePanelInnerPanel.transform)
            Destroy(child.gameObject);

        if (SelectedObjects.Count == 0)
            return;

        if (SelectedObjects[0].Upgrades.Count == 0)
            return;

        upgradePanelAnimator.SetBool("show", true);

        foreach (Upgrade upgrade in SelectedObjects[0].Upgrades)
        {
            Upgrade newUpgrade = Instantiate(upgrade) as Upgrade;
            newUpgrade.transform.SetParent(UpgradePanelInnerPanel.transform, false);
        }
    }

    public void HideUpgradePanel()
    {
        upgradePanelAnimator.SetBool("show", false);
    }

    public void ShowMinionUpgradePanel()
    {
        minionUpgradePanelAnimator.SetBool("show", true);
    }

    public void HideMinionUpgradePanel()
    {
        minionUpgradePanelAnimator.SetBool("show", false);
    }

    public void ShowMinionControllerPanel()
    {
        MinionControllerPanel.SetActive(true);
    }

    public void HideMinionControllerPanel()
    {
        MinionControllerPanel.SetActive(false);
    }

    public void ShowUnitCreatePanel()
    {
        unitCreateAnimator.SetBool("show", true);
    }

    public void HideUnitCreatePanel()
    {
        unitCreateAnimator.SetBool("show", false);
    }

    public void HideBuildingPanel()
    {
        buildingCanvasController.HideCanvas();
    }

    public void PauseGame()
    {
        gamePaused = true;

        Time.timeScale = 0;

        if (PauseMenuPanel != null)
            PauseMenuPanel.gameObject.SetActive(true);
    }

    public void ResumeGame()
    {
        gamePaused = false;

        Time.timeScale = 1;

        if (PauseMenuPanel != null)
        {
            PauseMenuPanel.gameObject.SetActive(false);
            pauseMenuManager.SetOptionsPanelActive(false);
        }
    }

    public bool IsGamePaused()
    {
        return gamePaused;
    }

    public bool IsUpgradeBought()
    {
        return upgradeBaught;
    }

    public void UpgradeBought()
    {
        upgradeBaught = true;
        StartCoroutine(UpgradeBoughtTimer());
    }

    public bool CheckIfUIPressedThisFrame()
    {
        return uIPressedThisFrame;
    }

    public void UIPressedOnce()
    {
        uIPressedThisFrame = true;
        StartCoroutine(UIPressedTimer());
    }

    public void UIPressed()
    {
        uIPressedThisFrame = true;
    }

    public void UIDePressed()
    {
        uIPressedThisFrame = false;
    }

    public void PlaySound(SoundFile sound)
    {
        switch (sound)
        {
            case SoundFile.Error:
                Audiosource.clip = ErrorClip;
                break;
            case SoundFile.Upgrade:
                Audiosource.clip = UpgradeClip;
                break;
            case SoundFile.Button:
                Audiosource.clip = ButtonClip;
                break;
            case SoundFile.Toggle:
                Audiosource.clip = ToggleClip;
                break;
            case SoundFile.SoftButton:
                Audiosource.clip = SoftButtonClip;
                break;
            default:
                break;
        }

        Audiosource.Play();
    }

    public void ActivateHeroSkillsPanel(bool value)
    {
        GetHeroSkillsPanelController().transform.parent.gameObject.SetActive(value);
        Time.timeScale = value ? 0 : 1;
    }

    public void SetTextInputMode(bool value)
    {
        TextInputMode = value;
    }

    public void DestroyAllRepairCanvases()
    {
        for (int i = 0; i < Buildings.Count; i++)
        {
            if (Buildings[i] is IRepeirable)
            {
                if ((Buildings[i] as IRepeirable).GetRepairCanvas() != null)
                    Destroy((Buildings[i] as IRepeirable).GetRepairCanvas());
            }
        }
    }

    public int GetGameTime()
    {
        return (int)timer;
    }

    public void AddEnemyMinionController(EnemyMinionSpawnerController controller)
    {
        enemyMinionsSpawnerControllers.Add(controller);
    }

    public void RemoveEnemyMinionController(EnemyMinionSpawnerController controller)
    {
        if (enemyMinionsSpawnerControllers.Contains(controller))
            enemyMinionsSpawnerControllers.Remove(controller);
        else
            Debug.Log("Enemy Minion Controller is not in the list.");
    }

    public void ShowTutorialPanel(Sprite image, string name, string description, string hint, int count, int maxCount)
    {
        TutorialPanelController.gameObject.SetActive(true);

        TutorialPanelController.Image.sprite = image;
        TutorialPanelController.Name.text = name;
        TutorialPanelController.Description.text = description;
        TutorialPanelController.Hint.text = hint;
        TutorialPanelController.TutorialText.text = string.Format("Tutorial {0} / {1}", count, maxCount);
    }

    public void HideTurorialPanel()
    {
        TutorialPanelController.gameObject.SetActive(false);
    }

    public List<EnemyMinionSpawnerController> GetEnemyMinionControllerList()
    {
        return enemyMinionsSpawnerControllers;
    }

    public GameObject[] GetHeroHudElements()
    {
        if (HeroPanelDown != null && MainPanelUpper != null && MiniMapPanel != null)
            return new GameObject[] { HeroPanelDown, MainPanelUpper, MiniMapPanel };
        else
            throw new System.Exception("HeroPanelDown, MainPanelUpper or MiniMapPanel in Gamemanager are/is Null");
    }

    public GameObject GetGrassPlane()
    {
        if (GrassPlane != null)
            return GrassPlane;
        else
            throw new System.Exception("There is no Grass Plane attached to the Gamemanager!");
    }

    public GameObject GetRepairCanvasPrefab()
    {
        if (RepairCanvasPrefab != null)
            return RepairCanvasPrefab;
        else
            throw new System.Exception("There is no RepairCanvasPrefab attached to the gameManger!");
    }

    public GameObject GetBuildingRepairParticle()
    {
        if (BuildingRepairParticle != null)
            return BuildingRepairParticle;
        else
            throw new System.Exception("There is no BuildingRepairParticle attached to the Gamemanager!");
    }

    public GameObject GetVillagar()
    {
        if (VillagerPrefab != null)
            return VillagerPrefab;
        else
            throw new System.Exception("There is no VillagarPrefab attached to the GameManager!");
    }

    public GameObject GetFogOfWarPlane()
    {
        if (FogOfWarPlane != null)
            return FogOfWarPlane;
        else
            throw new System.Exception("There is no FogOfWarPlane attached to the GameManager!");
    }

    public GameObject GetSkyBoxTerrain()
    {
        if (SkyBoxTerrain != null)
            return SkyBoxTerrain;
        else
            throw new System.Exception("There is no SkyBoxTerrain attached to the Gamemanager!");
    }

    public GameObject GetFogOfWarElementPrefab()
    {
        if (FogOfWarElementPrefab != null)
            return FogOfWarElementPrefab;
        else
            throw new System.Exception("There is no FogOfWarElementPrefab attached to the GameManager!");
    }

    public GameObject GetHealthBarOutline()
    {
        if (HealthBar_Outline != null)
            return HealthBar_Outline;
        else
            throw new System.Exception("There is no HealthBar_Outline attached to the Gamemanager");
    }

    public GameObject GetDemageIndicator()
    {
        if (DamageIndicatorGameObject != null)
            return DamageIndicatorGameObject;
        else
            throw new System.Exception("There is no DamageIndicatorGameobject attached to the Gamemanager");
    }

    public GameObject GetSkillHeroCamera()
    {
        if (SkillHeroCamera != null)
            return SkillHeroCamera;
        else
            throw new System.Exception("there is no SkillHeroCamera attached to the GameManager!");
    }

    public BuildingCanvasController GetBuildingCanvasController()
    {
        if (buildingCanvasController != null)
            return buildingCanvasController;
        else
            throw new System.Exception("There is now buildingCanvasController attached to the Gamemanager!");
    }

    public MainBuildingController GetMainBuildingController()
    {
        if (MainBuildingController != null)
            return MainBuildingController;
        else
            throw new System.Exception("There is no MainBuildingController attached to the Gamemanager");
    }

    public EndGameController GetEndGameController()
    {
        if (EndGameController != null)
            return EndGameController;
        else
            throw new System.Exception("There is no EndGameController attached to the Gamemanager");
    }

    public Terrain GetTerrain()
    {
        if (Terrain != null)
            return Terrain;
        else
            throw new System.Exception("Terrain in Gamemanager is Null");
    }

    public PauseMenuManager GetPauseMenuManager()
    {
        if (pauseMenuManager != null)
            return pauseMenuManager;
        else
            throw new System.Exception("pauseMenuManager in Gamemanager is Null");
    }

    public HeroSkillsPanelController GetHeroSkillsPanelController()
    {
        return HeroSkillsPanelController;
    }

    public List<Skill> GetHeroSkills()
    {
        return HeroSkills;
    }

    public Building GetBuilding(string name, bool forToolTip)
    {
        foreach (GameObject building in BuildingPrefabs)
        {
            if (building.name == name)
                return building.GetComponent<Building>();
        }
        return null;
    }

    public GameObject GetBuildingGameObject(string name)
    {
        foreach (GameObject gameObject in BuildingPrefabs)
        {
            if (gameObject.name == name)
                return gameObject;
        }
        return null;
    }

    public HeroController GetHeroController()
    {
        if (hero != null)
            return hero;
        else
            throw new System.Exception("There is no GameObject called Hero in the Scene!");
    }

    public WeatherController GetWeatherController()
    {
        if (weatherController != null)
            return weatherController;
        else
            throw new System.Exception("There is no WeatherController attached To the GameManager!");
    }

    public StrategieCameraController GetGameController()
    {
        if (controller != null)
            return controller;
        else
            throw new System.Exception("There is no Controller attached!");
    }

    public Ressources GetRessources()
    {
        if (ressources != null)
            return ressources;
        else
            throw new System.Exception("Ressources in Gamemanager is Null");
    }

    public SkillPanelController GetSkillPanelController()
    {
        if (SkillPanelController != null)
            return SkillPanelController;
        else
            throw new System.Exception("SkillPanelController in Gamemanager is Null");
    }

    public AudioManager GetAudioManager()
    {
        if (audioManager != null)
            return audioManager;
        else
            throw new System.Exception("There is no AudioManager on the GameManager gameobject");
    }

    public Camera GetMiniMapCamera()
    {
        if (MiniMapCamera != null)
            return MiniMapCamera;
        else
            throw new System.Exception("There is no MiniMapCamera attached to the Gamemanger");
    }

    public Image GetHitEffectImage()
    {
        if (HitEffectPanel != null)
            return HitEffectPanel.GetComponent<Image>();
        else
            throw new System.Exception("There is no HitEffectPanel Attached to the Gamemanager");
    }

    public GameObject[] GetAllGameobjects()
    {
        GameObject[] gameObjects = FindObjectsOfType<GameObject>();
        return gameObjects;
    }

    public void CreateClickEffectOnClickedPos()
    {
        if (ClickEffect != null)
            Instantiate(ClickEffect, Utilitys.GiveMousePositionInWorldOnFloor(), Quaternion.identity);
    }

    public void ChangeHud(bool heroMode)
    {
        if (heroMode)
        {
            HeroHud.SetActive(true);
            BuildHud.SetActive(false);
        }
        else
        {
            HeroHud.SetActive(false);
            BuildHud.SetActive(true);
        }
    }

    private void MoveToolTipToMouse()
    {
        if (ToolTip.activeSelf)
        {
            Vector2 pos;
            Vector2 posXVector = Vector2.zero;
            Vector2 posYVector = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, Input.mousePosition, Canvas.worldCamera, out pos);

            RectTransform rectTransform = ToolTip.GetComponent<RectTransform>();

            if (Input.mousePosition.y - (rectTransform.rect.height * Canvas.scaleFactor) < 0)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Input.mousePosition.x, rectTransform.rect.height * Canvas.scaleFactor), Canvas.worldCamera, out posYVector);

            if (Input.mousePosition.x + (rectTransform.rect.width * Canvas.scaleFactor) > Screen.width)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Screen.width - rectTransform.rect.width * Canvas.scaleFactor, Input.mousePosition.y), Canvas.worldCamera, out posXVector);

            ToolTip.transform.position = Canvas.transform.TransformPoint(new Vector3(
                posXVector == Vector2.zero ? pos.x : posXVector.x,
                posYVector == Vector2.zero ? pos.y : posYVector.y,
                0
            ));
        }
    }

    private void MoveSkillToolTipToMouse()
    {
        if (SkillToolTip.activeSelf)
        {
            Vector2 pos;
            Vector2 posXVector = Vector2.zero;
            Vector2 posYVector = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, Input.mousePosition, Canvas.worldCamera, out pos);

            RectTransform rectTransform = SkillToolTip.GetComponent<RectTransform>();

            if (Input.mousePosition.y - (rectTransform.rect.height * Canvas.scaleFactor) < 0)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Input.mousePosition.x, rectTransform.rect.height * Canvas.scaleFactor), Canvas.worldCamera, out posYVector);

            if (Input.mousePosition.x + (rectTransform.rect.width * Canvas.scaleFactor) > Screen.width)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Screen.width - rectTransform.rect.width * Canvas.scaleFactor, Input.mousePosition.y), Canvas.worldCamera, out posXVector);

            SkillToolTip.transform.position = Canvas.transform.TransformPoint(new Vector3(
                posXVector == Vector2.zero ? pos.x : posXVector.x,
                posYVector == Vector2.zero ? pos.y : posYVector.y,
                0
            ));
        }
    }

    private void MoveHintToMouse()
    {
        if (Hint.activeSelf)
        {
            Vector2 pos;
            Vector2 posXVector = Vector2.zero;
            Vector2 posYVector = Vector2.zero;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, Input.mousePosition, Canvas.worldCamera, out pos);

            RectTransform rectTransform = Hint.GetComponent<RectTransform>();

            if (Input.mousePosition.y - (rectTransform.rect.height * Canvas.scaleFactor) < 0)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Input.mousePosition.x, rectTransform.rect.height * Canvas.scaleFactor), Canvas.worldCamera, out posYVector);

            if (Input.mousePosition.x + (rectTransform.rect.width * Canvas.scaleFactor) > Screen.width)
                RectTransformUtility.ScreenPointToLocalPointInRectangle(Canvas.transform as RectTransform, new Vector2(Screen.width - rectTransform.rect.width * Canvas.scaleFactor, Input.mousePosition.y), Canvas.worldCamera, out posXVector);

            Hint.transform.position = Canvas.transform.TransformPoint(new Vector3(
                posXVector == Vector2.zero ? pos.x : posXVector.x,
                posYVector == Vector2.zero ? pos.y : posYVector.y,
                0
            ));
        }
    }

    IEnumerator MessageBoxTimer(float time)
    {
        yield return new WaitForSeconds(time);
        HideMessageBox();
    }

    IEnumerator UIPressedTimer()
    {
        yield return new WaitForFixedUpdate();
        uIPressedThisFrame = false;
    }

    IEnumerator UpgradeBoughtTimer()
    {
        yield return new WaitForSeconds(0.2f);
        upgradeBaught = false;
    }
}
