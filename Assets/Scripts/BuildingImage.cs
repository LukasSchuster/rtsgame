﻿using UnityEngine;
using UnityEngine.UI;

public class BuildingImage : MonoBehaviour
{
    public Image Image;
    public Text Name;
    public Text Price;
}
