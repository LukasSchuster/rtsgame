﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class MiniMapController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private const int HUNDRED_PERCENT = 100;

    private RectTransform rectTransform;
    private Vector2 rectBottemLeft;
    private float terrainLenghtX;
    private float terrainLenghtZ;
    private bool active;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void Start()
    {
        rectBottemLeft = new Vector2(Screen.width - rectTransform.rect.xMax * GameManager.Manager.Canvas.scaleFactor, Screen.height - rectTransform.rect.yMax * GameManager.Manager.Canvas.scaleFactor);

        terrainLenghtX = GameManager.Manager.GetTerrain().terrainData.size.x;
        terrainLenghtZ = GameManager.Manager.GetTerrain().terrainData.size.z;
    }

    void Update()
    {
        if (active && Input.GetMouseButton(0))
        {
            if (GameManager.Manager.GetGameController().CamMode == StrategieCameraController.CameraMode.SelectMode)
            {
                GameManager.Manager.GetGameController().transform.position = GetRelativeTerrainPositionofMapPoint(GameManager.Manager.GetGameController().transform.position);
                return;
            }
        }
    }

    private Vector3 GetRelativeTerrainPositionofMapPoint(Vector3 camPosition)
    {
        Vector2 clickedPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        float xBase = Screen.width - rectBottemLeft.x;
        float xValue = clickedPos.x - rectBottemLeft.x;
        float xPercentage = xValue / xBase * HUNDRED_PERCENT;

        float yBase = Screen.height - rectBottemLeft.y;
        float yValue = clickedPos.y - rectBottemLeft.y;
        float yPercentage = yValue / yBase * HUNDRED_PERCENT;

        float newCamPosX = xPercentage / HUNDRED_PERCENT * terrainLenghtX;
        float newCamPosZ = yPercentage / HUNDRED_PERCENT * terrainLenghtZ;

        return new Vector3(newCamPosX, camPosition.y, newCamPosZ);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        active = true;
        GameManager.Manager.UIPressed();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        active = false;
        GameManager.Manager.UIDePressed();
    }
}
