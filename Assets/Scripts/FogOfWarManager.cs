﻿using UnityEngine;
using System.Collections.Generic;

public class FogOfWarManager : MonoBehaviour
{
    private static List<FogOfWarElement> fogOfWarElements = new List<FogOfWarElement>();

    public static FogOfWarElement AddElement(Health parentHealth, float scale)
    {
        GameObject elementGO = Instantiate(GameManager.Manager.GetFogOfWarElementPrefab(), Vector3.zero, Quaternion.identity) as GameObject;
        FogOfWarElement element = elementGO.GetComponent<FogOfWarElement>();

        elementGO.transform.SetParent(GameManager.Manager.GetFogOfWarPlane().transform);
        fogOfWarElements.Add(element);
        element.SetValues(parentHealth, scale);

        return element;
    }

    public static void RemoveElement(FogOfWarElement element)
    {
        fogOfWarElements.Remove(element);

        if (element != null)
            Destroy(element.gameObject);
    }
}
