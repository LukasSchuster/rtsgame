﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(NavMeshAgent))]
public class Unit : SelectableObject, IMoveable<Vector3>
{
    private bool move;
    private Vector3 moveToPosition;

    public override void CallOnSelectedRightClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            GameManager.Manager.CreateClickEffectOnClickedPos();
            moveToPosition = hit.point;
            move = true;
        }

        MoveTo(hit.point);
    }

    public void MoveTo(Vector3 position)
    {
        moveToPosition = position;
        Agent.SetDestination(position);
    }

    public override void OnDeSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnDeSelected(ref selectedObjects);
    }

    public override void OnSelected(ref List<SelectableObject> selectedObjects)
    {
        base.OnSelected(ref selectedObjects);
    }
}
