﻿using UnityEngine;
using System.Collections;

public class MapProp : MonoBehaviour
{
    public GameObject IconPrefab;
    public Sprite IconTexture;
    public float IconSize;
    public float HeightOffset = 50;
    public bool RotateYAxis = false;

    [HideInInspector]
    public GameObject MapPropGameObject;

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        MapPropGameObject = Instantiate(IconPrefab, new Vector3(transform.position.x, transform.position.y + HeightOffset, transform.position.z), IconPrefab.transform.rotation) as GameObject;

        spriteRenderer = MapPropGameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = IconTexture;

        MapPropGameObject.transform.localScale = new Vector3(IconSize, IconSize, IconSize);

        MapPropGameObject.transform.SetParent(transform);
    }

    public GameObject AddMapProp(Sprite iconTexture, Transform parent)
    {
        MapPropGameObject = Instantiate(IconPrefab, new Vector3(transform.position.x, transform.position.y + HeightOffset, transform.position.z), IconPrefab.transform.rotation) as GameObject;

        spriteRenderer = MapPropGameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = iconTexture;

        MapPropGameObject.transform.localScale = new Vector3(IconSize, IconSize, IconSize);
        MapPropGameObject.transform.SetParent(parent);

        return MapPropGameObject;
    }

    public void DestroyIcon()
    {
        if (MapPropGameObject != null)
            Destroy(MapPropGameObject);
    }

    public void DisableIcon()
    {
        if (MapPropGameObject != null)
            MapPropGameObject.SetActive(false);
    }

    public void EnableIcon()
    {
        if (MapPropGameObject != null)
            MapPropGameObject.SetActive(true);
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {
        if (MapPropGameObject != null)
        {
            MapPropGameObject.transform.position = new Vector3(transform.position.x, transform.position.y + HeightOffset, transform.position.z);

            if (RotateYAxis)
            {
                MapPropGameObject.transform.rotation = transform.rotation;
                MapPropGameObject.transform.Rotate(90, 0, 0);
            }
        }
    }
}
